//
//  UIButtonForMenu.h
//  CenturyLink
//
//  Created by Tan Duong Nhat on 1/20/15.
//  Copyright (c) 2015 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>

#define k_BackgroundColor_Normal [UIColor colorWithRed:238/255.0f green:238/255.0f blue:238/255.0f alpha:1.0f]

#define k_BackgroundColor_Selected [UIColor colorWithRed:0.0f green:133/255.0f blue:63/255.0f alpha:1.0f]

#define k_BackgroundColor_Highlighted [UIColor colorWithRed:141/255.0f green:198/255.0f blue:63/255.0f alpha:1.0f]



@interface UIButtonForMenu : UIButton

@property (nonatomic, strong) UIColor *original_BackgroundColor;


@end
