//
//  UIButtonForMenu.m
//  CenturyLink
//
//  Created by Tan Duong Nhat on 1/20/15.
//  Copyright (c) 2015 CO2. All rights reserved.
//

#import "UIButtonForMenu.h"

@implementation UIButtonForMenu


@synthesize original_BackgroundColor;


//-------------------------------------------------------
- (void)drawRect:(CGRect)rect {
    
    [self addTarget:self action:@selector(didTapButtonForHighlight:) forControlEvents:UIControlEventTouchDown];
    
    [self addTarget:self action:@selector(didTapButtonForHighlight:) forControlEvents:UIControlEventTouchDragOutside];
    
    [self addTarget:self action:@selector(didTapButtonForHighlight:) forControlEvents:UIControlEventTouchDragInside];
    
    [self addTarget:self action:@selector(didUnTapButtonForHighlight:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addTarget:self action:@selector(didUnTapButtonForHighlight:) forControlEvents:UIControlEventTouchUpOutside];
    
    
}


//-------------------------------------------------------
- (void)didTapButtonForHighlight:(UIButton *)sender
{
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self setBackgroundColor:k_BackgroundColor_Highlighted];
}


//-------------------------------------------------------
- (void)didUnTapButtonForHighlight:(UIButton *)sender {
    
    [self setBackgroundColor:original_BackgroundColor];
}


@end
