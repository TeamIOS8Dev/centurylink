//
//  Common.m
//  CenturyLink
//
//  Created by MacMini  on 11/5/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "Common.h"
#import "Reachability.h"


@implementation Common


#pragma mark -
#pragma mark Ultilities

/***************************************************/


//------------------------------------------------
+(AppDelegate *) getAppDelegate
{
    return (AppDelegate *) [[UIApplication sharedApplication] delegate];
}


//------------------------------------------------
+(UIColor *)resultContentColor
{
    return [UIColor colorWithRed:97 / 255.0f green:99 / 255.0f blue:101 / 255.0f alpha:1.0f];
}


//------------------------------------------------
+(void) setLineHeight:(float) lineHeight for_UILabel:(UILabel *) label
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineHeight];
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, label.text.length)];
    
    label.attributedText = attributedString;
    
    attributedString = nil;
    paragraphStyle = nil;
}



//------------------------------------------------
+(BOOL) validateEmail:(NSString *)originalString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:originalString];
}


//------------------------------------------------
+(BOOL) validateNumber: (NSString *)number
{
    if ([number rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        // BasicAlert(@"", @"This field accepts only numeric entries.");
        return NO;
    }
    return YES;
}

//------------------------------------------------------------------------------------------
/**   */
+(BOOL)isContainEnglishNotCharacter: (NSString *)checkedString
{
    BOOL isNotContain =  [checkedString canBeConvertedToEncoding:NSASCIIStringEncoding];
    if (isNotContain) {
//            NSLog(@"YES");
            return NO;
    }
    else
    {
//        NSLog(@"NO");
            return YES;
    }
    return isNotContain;
}


//------------------------------------------------------------------------------------------
/**   */
+ (NSString *)languageForString:(NSString *) text{
    
    
    if (text.length < 100) {
        
        return (NSString *)CFBridgingRelease(CFStringTokenizerCopyBestStringLanguage((CFStringRef)text, CFRangeMake(0, text.length)));
    } else {
        
        return (NSString *)CFBridgingRelease(CFStringTokenizerCopyBestStringLanguage((CFStringRef)text, CFRangeMake(0, 100)));
    }
    
}

/***************************************************/


//------------------------------------------------------------------------------------------
/** Check network is available  */
+(BOOL) checkNetwork
{
    Reachability *r = [Reachability reachabilityForInternetConnection];
    NetworkStatus intenetStatus = [r currentReachabilityStatus];
    
    if ((intenetStatus != ReachableViaWiFi) && (intenetStatus != ReachableViaWWAN))
    {
        return NO;
    }
    return YES;
}


@end
