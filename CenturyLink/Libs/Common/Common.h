//
//  Common.h
//  CenturyLink
//
//  Created by MacMini  on 11/5/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "AppDelegate.h"



#define kFont_UniversLTStd(theFontSize) [UIFont fontWithName:@"UniversLTStd" size:theFontSize]

#define kFont_UniversLTStd_Italic(theFontSize) [UIFont fontWithName:@"UniversLTStd-LightObl" size:theFontSize]

#define kFont_UniversLTStd_Light_Bold(theFontSize) [UIFont fontWithName:@"Univers-Light-Bold" size:theFontSize]
#define kFont_UniversLTStd_Light(theFontSize) [UIFont fontWithName:@"UniversLTStd-Light" size:theFontSize]
#define kFont_UniversLTStd_Bold(theFontSize) [UIFont fontWithName:@"UniversLTStd-Bold" size:theFontSize]

#define kFont_UniversLTStd_BoldCn(theFontSize) [UIFont fontWithName:@"UniversLTStd-BoldCn" size:theFontSize]

#define kFont_CaeciliaLTStd_Bold(theFontSize) [UIFont fontWithName:@"CaeciliaLTStd-Bold" size:theFontSize]

#define kLineHeightOf_Lb_Footer 8.0f


@interface Common : NSObject


#pragma mark -
#pragma mark Ultilities

/***************************************************/


+(AppDelegate *) getAppDelegate;

+(UIColor *)resultContentColor;

+(void) setLineHeight:(float) lineHeight for_UILabel:(UILabel *) label;

+(BOOL) validateEmail:(NSString *)originalString;

+(BOOL) validateNumber: (NSString *)number;

+(BOOL) checkNetwork;

+(BOOL)isContainEnglishNotCharacter: (NSString *)checkedString;


/***************************************************/


/********************************************************************************/
#pragma mark - Server API


//  url
#define kLinkToServer @"https://secure.eloqua.com/API/REST/1.0/"
#define kSubmitFormData @"data/form/1073"


//  Tag Service
#define kTagPostData 1
#define kTagPostBulkData 2

//  Authorization
#define kUserName @"Savvis\\Admin.Web"
#define kPassword @"CTL-y2014"


#define kNotificationFailed @"NotificationSubmitFailed"
#define kNotificationSuccess @"NotificationSubmitSuccessfull"

/********************************************************************************/


@end
