//
//  LoadAllRequest.m
//  TurnBaseHydra
//
//  Created by Thien Thanh on 7/28/14.
//  Copyright (c) 2014 Thien Thanh. All rights reserved.
//

#import "LoadAllRequest.h"
#import "Common.h"
#import "MBProgressHUD.h"
#import "MasterViewController.h"

@interface LoadAllRequest ()


@end

@implementation LoadAllRequest


+ (LoadAllRequest *)sharedLoadAllRequest
{
    static LoadAllRequest *_sharedLoadAllRequest = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedLoadAllRequest = [[self alloc] initWithBaseURL:[NSURL URLWithString:kLinkToServer]];
    });
    
    return _sharedLoadAllRequest;
}


- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        //  request timeout
        self.requestSerializer.timeoutInterval = 30;
 
    }
    
    return self;
}



-(void)requestPostDataFromServer:(NSString *)url andParameters:(NSDictionary *)params andTag:(int)kTag
{
    // Request: My API (https://secure.eloqua.com/API/REST/1.0)
    
    // Headers
//    [self.requestSerializer setAuthorizationHeaderFieldWithUsername:kUserName password:kPassword];
    [self.requestSerializer setValue:@"Basic U2F2dmlzXEFkbWluLldlYjpDVEwteTIwMTQ=" forHTTPHeaderField:@"Authorization"];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    
    // Request Operation
    switch (kTag) {
        case kTagPostData:
            [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] delegate] window] animated:YES];
            break;
        case kTagPostBulkData:
        {
            //  progress bar
            if (!_progressBarViewController) {
                UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                _progressBarViewController = [mystoryboard instantiateViewControllerWithIdentifier:@"progressBar"];
                
                CGSize screenSize = [UIScreen mainScreen].bounds.size;
                if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                    [_progressBarViewController.view setFrame:CGRectMake(0, 0, screenSize.height, screenSize.width)];
                } else {
                    [_progressBarViewController.view setFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
                }
                
                //  FIXME: GET VIEWCONTROLLER ON TOP
                [[[[[UIApplication sharedApplication] keyWindow] subviews] lastObject] addSubview:_progressBarViewController.view];
                

            }
            _progressBarViewController.view.hidden = NO;
        }
            break;
            
        default:
            break;
    }

    
    [self POST:[NSString stringWithFormat:@"%@%@",kLinkToServer,url] parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [self requestFinished:responseObject andTag:kTag];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self requestFailed:error andTag:kTag];
    }];
}






/********************************************************************************/
#pragma mark - REQUEST FAILED



/**
 * Request failed
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)requestFailed:(NSError *)error andTag:(int)tag
{
    [MBProgressHUD hideAllHUDsForView:[[[UIApplication sharedApplication] delegate] window] animated:YES];
    switch (tag)
    {
        //  Post Data
        case kTagPostData:
        {
            if ([self.delegate respondsToSelector:@selector(didFaildRequestPostData:)]) {
                [self.delegate didFaildRequestPostData:error];
            }
        }
            break;
            
        //  Bulk Data
        case kTagPostBulkData:
        {
            if ([self.delegate respondsToSelector:@selector(didFaildRequestPostBulkData:)]) {
                [self.delegate didFaildRequestPostBulkData:error];
            }
        }
            break;
            
        default:
            break;
    }
}



/********************************************************************************/




/********************************************************************************/
#pragma mark - REQUEST SUCCESSFUL



/**
 * Request successful
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (void)requestFinished:(id)responseObject andTag:(int)tag
{
    [MBProgressHUD hideAllHUDsForView:[[[UIApplication sharedApplication] delegate] window] animated:YES];
    switch (tag)
    {
        // Post Data
        case kTagPostData:
        {
            if ([self.delegate respondsToSelector:@selector(didSucessfullRequestPostData:)]) {
                [self.delegate didSucessfullRequestPostData:responseObject];
            }
        }
            break;
            
            
        //  Bulk Data
        case kTagPostBulkData:
        {
            if ([self.delegate respondsToSelector:@selector(didSucessfullRequestPostBulkData:)]) {
                [self.delegate didSucessfullRequestPostBulkData:responseObject];
            }
        }
            break;

            
            
        default:
            break;
    }
}


/********************************************************************************/




@end
