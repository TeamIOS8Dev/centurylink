//
//  LoadAllRequest.h
//  TurnBaseHydra
//
//  Created by Thien Thanh on 7/28/14.
//  Copyright (c) 2014 Thien Thanh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "ProgressBarViewController.h"


@protocol LoadAllRequestDelegate;

@interface LoadAllRequest : AFHTTPSessionManager

@property (nonatomic, weak) id<LoadAllRequestDelegate> delegate;
@property (nonatomic , strong) ProgressBarViewController *progressBarViewController;

-(void)requestPostDataFromServer:(NSString *)url andParameters:(NSDictionary *)params andTag:(int)kTag;


+ (LoadAllRequest *)sharedLoadAllRequest;

@end

@protocol LoadAllRequestDelegate <NSObject>
@optional

//  Post Data
-(void)didSucessfullRequestPostData:(id)responseObject;
-(void)didFaildRequestPostData:(NSError *)error;

//  Bulk Data
-(void)didSucessfullRequestPostBulkData:(id)responseObject;
-(void)didFaildRequestPostBulkData:(NSError *)error;


@end
