//
//  MasterViewController.m
//  CenturyLink
//
//  Created by MacMini  on 11/6/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "MasterViewController.h"
#import "Common.h"



#import "QueueResultField.h"


#import "StartViewController.h"
#import "NoPlan_Step2ViewController.h"
#import "HasPlan_Step2ViewController.h"

#import "CurrentViewController.h"

#import "Step3ViewController.h"
#import "Step4ViewController.h"

#import "NoPlan_Step5ViewController.h"
#import "HasPlan_Step5ViewController.h"

#import "ResultPart1ViewController.h"
#import "ResultPart2ViewController.h"
#import "ResultPopUpViewController.h"
#import "ProgressBarViewController.h"


#define kTag_AlertView_BackTo_StartMenu 0


// Menu Buttons
#define kMarginHorizontal_SubViewIn_IOS_7 12


@interface MasterViewController ()<UIAlertViewDelegate, StartViewControllerDelegate, NoPlan_Step2ViewControllerDelegate, HasPlan_Step2ViewControllerDelegate, CurrentViewControllerDelegate, Step3ViewControllerDelegate, Step4ViewControllerDelegate, NoPlan_Step5ViewControllerDelegate, HasPlan_Step5ViewControllerDelegate, ResultPart1ViewControllerDelegate, ResultPart2ViewControllerDelegate, ResultPopUpViewControllerDelegate>
{
    
    // Properties
    BOOL isChoose_HasPlan; // NO
    
    // Controllers
    StartViewController *startViewController;
    NoPlan_Step2ViewController *noPlan_Step2ViewController;
    HasPlan_Step2ViewController *hasPlan_Step2ViewController;
    
    CurrentViewController *currentViewController;
    
    Step3ViewController *step3ViewController;
    Step4ViewController *step4ViewController;
    
    NoPlan_Step5ViewController *noPlan_Step5ViewController;
    HasPlan_Step5ViewController *hasPlan_Step5ViewController;
    
    ResultPart1ViewController *resultPart1ViewController;
    ResultPart2ViewController *resultPart2ViewController;
    ResultPopUpViewController *resultPopUpViewController;

    
    AppDelegate *appDelegate;
}

@end

@implementation MasterViewController


#pragma mark - Load Data

/***************************************************************/

//-------------------------------------------------------
-(void) addSubViewToMasterView_StartViewController
{
    // StartViewController
    if (!startViewController)
    {
        startViewController = [StartViewController new];
    }
    
    startViewController.delegate = self;

    CGRect frame = startViewController.view.frame;
    frame.origin.y = 0;

    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x += kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    startViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:startViewController.view];
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_CurrentViewController
{
    // Step3ViewController
    if (!currentViewController)
    {
        currentViewController = [CurrentViewController new];
    }
    
    currentViewController.delegate = self;
    
    CGRect frame = currentViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 1024;
    
    currentViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:currentViewController.view];
    
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_NoPlan_Step2ViewController
{
    // NoPlan_Step2ViewController
    if (!noPlan_Step2ViewController)
    {
        noPlan_Step2ViewController = [NoPlan_Step2ViewController new];
    }
    
    noPlan_Step2ViewController.delegate = self;
    
    
    CGRect frame = noPlan_Step2ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 2048;
    noPlan_Step2ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:noPlan_Step2ViewController.view];
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_HasPlan_Step2ViewController
{
    // HasPlan_Step2ViewController
    if (!hasPlan_Step2ViewController)
    {
        hasPlan_Step2ViewController = [HasPlan_Step2ViewController new];
    }
    
    hasPlan_Step2ViewController.delegate = self;
    
    CGRect frame = hasPlan_Step2ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 2048;
    
    hasPlan_Step2ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:hasPlan_Step2ViewController.view];
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_Step3ViewController
{
    // Step3ViewController
    if (!step3ViewController)
    {
        step3ViewController = [Step3ViewController new];
    }
    
    step3ViewController.isHasPlan = isChoose_HasPlan;
    step3ViewController.delegate = self;
    
    CGRect frame = step3ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 3072;
    
    step3ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:step3ViewController.view];
    
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_Step4ViewController
{
    // Step4ViewController
    if (!step4ViewController)
    {
        step4ViewController = [Step4ViewController new];
    }
    
    step4ViewController.isHasPlan = isChoose_HasPlan;
    step4ViewController.delegate = self;
    
    CGRect frame = step4ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 4096;
    
    step4ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:step4ViewController.view];
   
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_NoPlan_Step5ViewController
{
    // NoPlan_Step5ViewController
    if (!noPlan_Step5ViewController)
    {
        noPlan_Step5ViewController = [NoPlan_Step5ViewController new];
    }
    
    noPlan_Step5ViewController.delegate = self;
    
    CGRect frame = noPlan_Step5ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 5120;
    
    noPlan_Step5ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:noPlan_Step5ViewController.view];
    
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_HasPlan_Step5ViewController
{
    // HasPlan_Step5ViewController
    if (!hasPlan_Step5ViewController)
    {
        hasPlan_Step5ViewController = [HasPlan_Step5ViewController new];
    }
    
    hasPlan_Step5ViewController.delegate = self;
    
    CGRect frame = hasPlan_Step5ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 5120;
    
    hasPlan_Step5ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:hasPlan_Step5ViewController.view];
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_ResultPart1ViewController
{
    // ResultPart1ViewController
    if (resultPart1ViewController)
    {
        [resultPart1ViewController.view removeFromSuperview];
        resultPart1ViewController = nil;
    }
    
    // Add Subview "ResultPart1ViewController"
    resultPart1ViewController = [ResultPart1ViewController new];
    resultPart1ViewController.delegate = self;
    
    // Pass
    resultPart1ViewController.isHasPlan = isChoose_HasPlan;
    
    CGRect frame = resultPart1ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 6144;
    
    resultPart1ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:resultPart1ViewController.view];
    
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_ResultPart2ViewController
{
    // ResultPart2ViewController
    if (resultPart2ViewController)
    {
        [resultPart2ViewController.view removeFromSuperview];
        resultPart2ViewController = nil;
    }
    
    // Add Subview "ResultPart2ViewController"
    resultPart2ViewController = [ResultPart2ViewController new];
    resultPart2ViewController.delegate = self;
    
    CGRect frame = resultPart2ViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 7168;
    
    resultPart2ViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:resultPart2ViewController.view];
}


//-------------------------------------------------------
-(void) addSubViewToMasterView_ResultPopUpViewController
{
    // Remove result view
    if (resultPopUpViewController)
    {
        [resultPopUpViewController.view removeFromSuperview];
        resultPopUpViewController = nil;
    }
    
    // Add Subview "ResultPopUpViewController"
    resultPopUpViewController = [ResultPopUpViewController new];
    resultPopUpViewController.delegate = self;
    
    CGRect frame = resultPopUpViewController.view.frame;
    frame.origin.y = 0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        frame.origin.x = kMarginHorizontal_SubViewIn_IOS_7;
    }
    
    frame.origin.x += 8192;
    
    resultPopUpViewController.view.frame = frame;
    
    [scrollerView_Content addSubview:resultPopUpViewController.view];
}


//-------------------------------------------------------
-(void) addSubviewFor_NoPlan_Except_Step2ViewController
{
    // Step3ViewController
    [self addSubViewToMasterView_Step3ViewController];
    
    // Step4ViewController
    [self addSubViewToMasterView_Step4ViewController];
    
    // NoPlan_Step5ViewController
    [self addSubViewToMasterView_NoPlan_Step5ViewController];
    
    // ResultPart1ViewController
    [self addSubViewToMasterView_ResultPart1ViewController];
    
    // ResultPart2ViewController
    [self addSubViewToMasterView_ResultPart2ViewController];
}


//-------------------------------------------------------
-(void) addSubviewFor_HasPlan_Except_Step2ViewController
{
    // Step3ViewController
    [self addSubViewToMasterView_Step3ViewController];
    
    // Step4ViewController
    [self addSubViewToMasterView_Step4ViewController];
    
    // HasPlan_Step5ViewController
    [self addSubViewToMasterView_HasPlan_Step5ViewController];
    
    // ResultPart1ViewController
    [self addSubViewToMasterView_ResultPart1ViewController];
    
    // ResultPart2ViewController
    [self addSubViewToMasterView_ResultPart2ViewController];

}


//-------------------------------------------------------
-(void) addSubViewsToMasterView
{
    // Remove all Subviews
    [self removeAllSubViewsIn_scrollerView_Content];
    
    // Add Result View
    [self addSubViewToMasterView_ResultPart1ViewController];
    
//    [self performSelectorInBackground:@selector(addSubViewToMasterView_ResultPart1ViewController) withObject:nil];
}

//------------------------------------------------------------------------------------------
/**   */
-(void)addHasPlanView
{
    [self addSubViewToMasterView_HasPlan_Step2ViewController];
    
    [self addSubviewFor_HasPlan_Except_Step2ViewController];
}

//------------------------------------------------------------------------------------------
/**   */
-(void)addNoPlanView
{
    [self addSubViewToMasterView_NoPlan_Step2ViewController];
    
    [self addSubviewFor_NoPlan_Except_Step2ViewController];
}


//-------------------------------------------------------
-(void) removeAllSubViewsIn_scrollerView_Content
{
//    [scrollerView_Content.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // Step2
    if (noPlan_Step2ViewController)
    {
        [noPlan_Step2ViewController.view removeFromSuperview];
        noPlan_Step2ViewController = nil;
    }
    
    if (hasPlan_Step2ViewController)
    {
        [hasPlan_Step2ViewController.view removeFromSuperview];
        hasPlan_Step2ViewController = nil;
    }
    
    
    // step3ViewController
    if (step3ViewController)
    {
        [step3ViewController.view removeFromSuperview];
        step3ViewController = nil;
    }
    
    
    // step4ViewController
    if (step4ViewController)
    {
        [step4ViewController.view removeFromSuperview];
        step4ViewController = nil;
    }
    
    
    // Step5
    if (noPlan_Step5ViewController)
    {
        [noPlan_Step5ViewController.view removeFromSuperview];
        noPlan_Step5ViewController = nil;
    }
    
    
    if (hasPlan_Step5ViewController)
    {
        [hasPlan_Step5ViewController.view removeFromSuperview];
        hasPlan_Step5ViewController = nil;
    }
    
    
    // ResultViewController
    if (resultPart1ViewController)
    {
        [resultPart1ViewController.view removeFromSuperview];
        resultPart1ViewController = nil;
    }
    
    if (resultPart2ViewController)
    {
        [resultPart2ViewController.view removeFromSuperview];
        resultPart2ViewController = nil;
    }
    
    if (resultPopUpViewController)
    {
        [resultPopUpViewController.view removeFromSuperview];
        resultPopUpViewController = nil;
    }
}


/***************************************************************/




#pragma mark - Init View

/***************************************************************/
//-------------------------------------------------------
+ (MasterViewController *)shared
{
    static dispatch_once_t once = 0;
    static MasterViewController *masterViewController;
    
    dispatch_once(&once, ^{ masterViewController = [[MasterViewController alloc] init]; });
    
    return masterViewController;
}


//-------------------------------------------------------
-(void) setUp_For_MenuButton:(UIButton *) button
{
    UIFont *fontMenu = kFont_UniversLTStd_Light(18);

    UIColor *textColor = [UIColor whiteColor];
    
    button.exclusiveTouch = YES;
    button.enabled = NO;
    [button.titleLabel setFont:fontMenu];

    [button setTitleColor:textColor forState:UIControlStateNormal];
    [button setTitleColor:textColor forState:UIControlStateHighlighted];
    [button setTitleColor:textColor forState:UIControlStateSelected];
    
    [button setTitleEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
}


//-------------------------------------------------------
-(void) prepareForLayout
{
    lb_Logo.font = kFont_UniversLTStd_Light(30);
    lb_Logo_SmallText.font = kFont_UniversLTStd(15);
    
    btn_Start.exclusiveTouch = YES;
    
    [self setUp_For_MenuButton:btn_BussinessDrivers];
    [self setUp_For_MenuButton:btn_Current];
    [self setUp_For_MenuButton:btn_Management];
    [self setUp_For_MenuButton:btn_IT_Infrastructure];
    [self setUp_For_MenuButton:btn_ComputeModels];
    [self setUp_For_MenuButton:btn_Results];
    
    
    // DeselectButton
    [self deselect_ButtonMenu:btn_BussinessDrivers];
    [self deselect_ButtonMenu:btn_Current];
    [self deselect_ButtonMenu:btn_Management];
    [self deselect_ButtonMenu:btn_IT_Infrastructure];
    [self deselect_ButtonMenu:btn_ComputeModels];
    [self deselect_ButtonMenu:btn_Results];
    
    // Select button Start
    [self select_ButtonMenu:btn_Start];
}


//-------------------------------------------------------
- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Layout
    [self prepareForLayout];
    
    
    appDelegate = [Common getAppDelegate];
    appDelegate.queueResultField = [QueueResultField new];
   
    
    // Add subview StartViewController
    [self addSubViewToMasterView_StartViewController];
}


//-------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/***************************************************************/




#pragma mark - Ulities For Menu

/***************************************************************/

//-------------------------------------------------------
-(void) deselect_ButtonMenu:(UIButtonForMenu *) button
{
    button.selected = NO;
    button.highlighted = NO;
    UIColor *textColor = [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f];
    
    [button setTitleColor:textColor forState:UIControlStateNormal];
    [button setImage:nil forState:UIControlStateNormal];
    
    button.original_BackgroundColor = k_BackgroundColor_Normal;
    [button setBackgroundColor:button.original_BackgroundColor];
    
    UIImage *image;
    
    if (button == btn_Start)
    {
        image = [UIImage imageNamed:@"btn_Start_Default.png"];
        [button setImage:image forState:UIControlStateNormal];
        
        image = [UIImage imageNamed:@"btn_Start_Highlighted.png"];
        [button setImage:image forState:UIControlStateHighlighted];
        
        return;
    }

    
    
}


//-------------------------------------------------------
-(void) passed_ButtonMenu:(UIButtonForMenu *) button
{
    button.selected = NO;
    button.highlighted = NO;
    
    button.original_BackgroundColor = k_BackgroundColor_Highlighted;
    [button setBackgroundColor:button.original_BackgroundColor];
    
    [button setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIImage *image;
    
    if (button == btn_Start)
    {
        image = [UIImage imageNamed:@"btn_Start_Passed.png"];
        [button setImage:image forState:UIControlStateNormal];
        
        image = [UIImage imageNamed:@"btn_Start_Passed.png"];
        [button setImage:image forState:UIControlStateHighlighted];
        
        return;
    }
    
    
    if (button == btn_BussinessDrivers)
    {
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(4, 4, 0, 0)];
        
        return;
    }
    
    
    if (button == btn_Current)
    {
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(4, 4, 0, 0)];
        
        return;
    }
    
    
    if (button == btn_Management)
    {
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(4, 4, 0, 0)];
        
        return;
    }
    
    
    if (button == btn_IT_Infrastructure)
    {
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(4, 4, 0, 0)];
        
        return;
    }
    
    
    if (button == btn_ComputeModels)
    {
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(4, 4, 0, 0)];
        
        return;
    }
  
}


//-------------------------------------------------------
-(void) select_ButtonMenu:(UIButtonForMenu *) button
{
    button.selected = YES;
    button.highlighted = NO;
    
    button.original_BackgroundColor = k_BackgroundColor_Selected;
    
    [button setImage:nil forState:UIControlStateNormal];
    
    [button setBackgroundColor:button.original_BackgroundColor];
    
    UIImage *image;
    
    if (button == btn_Start)
    {
        image = [UIImage imageNamed:@"btn_Start_Default.png"];
        [button setImage:image forState:UIControlStateNormal];
        
        image = [UIImage imageNamed:@"btn_Start_Passed.png"];
        [button setImage:image forState:UIControlStateHighlighted];
        
        return;
    }
}


//-------------------------------------------------------
-(void) backToStartMenu_StartViewController
{
    // Selected Button
    [self select_ButtonMenu:btn_Start];
    
    // DeselectButton
    [self deselect_ButtonMenu:btn_Start];
    [self deselect_ButtonMenu:btn_BussinessDrivers];
    [self deselect_ButtonMenu:btn_Current];
    [self deselect_ButtonMenu:btn_Management];
    [self deselect_ButtonMenu:btn_IT_Infrastructure];
    [self deselect_ButtonMenu:btn_ComputeModels];
    [self deselect_ButtonMenu:btn_Results];
   
    
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointZero animated:YES];
    
    // Remove all SubViews of scrollerView_Content
    [self removeAllSubViewsIn_scrollerView_Content];
    
    appDelegate.queueResultField = nil;
    appDelegate.queueResultField = [QueueResultField new];
    
    
    // AllButtonsMenu_Without_StartButton
    btn_BussinessDrivers.enabled = NO;
    btn_Current.enabled = NO;
    btn_Management.enabled = NO;
    btn_IT_Infrastructure.enabled = NO;
    btn_ComputeModels.enabled = NO;
    btn_Results.enabled = NO;
}


//-------------------------------------------------------
-(void) checkEnableForMenuButtons
{
    if (isChoose_HasPlan)
    {
        // Step2ViewController
        if (hasPlan_Step2ViewController)
        {
            if (!hasPlan_Step2ViewController.selectedAnswer
                || hasPlan_Step2ViewController.selectedAnswer.count == 0)
            {
                // Invalid
                btn_Management.enabled = NO;
                btn_IT_Infrastructure.enabled = NO;
                btn_ComputeModels.enabled = NO;
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_Management.enabled = YES;
            }
        }
        
        
        // Step3ViewController
        if (step3ViewController)
        {
            if (!step3ViewController.isDidAppear
                || !step3ViewController.selectedAnswer
                || step3ViewController.selectedAnswer.length == 0)
            {
                // Invalid
                btn_IT_Infrastructure.enabled = NO;
                btn_ComputeModels.enabled = NO;
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_IT_Infrastructure.enabled = YES;
            }
        }
        
        
        // Step4ViewController
        if (step4ViewController)
        {
            if (!step4ViewController.isDidAppear
                || !step4ViewController.selectedAnswer
                || step4ViewController.selectedAnswer.length == 0)
            {
                // Invalid
                btn_ComputeModels.enabled = NO;
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_ComputeModels.enabled = YES;
            }
        }
        
        
        // Step5ViewController
        if (hasPlan_Step5ViewController)
        {
            if (!hasPlan_Step5ViewController.isDidAppear
                || !hasPlan_Step5ViewController.selectedAnswer
                || hasPlan_Step5ViewController.selectedAnswer.count == 0)
            {
                // Invalid
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_Results.enabled = YES;
            }
        }
    }
    else // No Plan
    {
        // Step2ViewController
        if (noPlan_Step2ViewController)
        {
            if (!noPlan_Step2ViewController.selectedAnswer
                || noPlan_Step2ViewController.selectedAnswer.count == 0)
            {
                // Invalid
                btn_Management.enabled = NO;
                btn_IT_Infrastructure.enabled = NO;
                btn_ComputeModels.enabled = NO;
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_Management.enabled = YES;
            }
        }
        
        
        // Step3ViewController
        if (step3ViewController)
        {
            if (!step3ViewController.isDidAppear
                || !step3ViewController.selectedAnswer
                || step3ViewController.selectedAnswer.length == 0)
            {
                // Invalid
                btn_IT_Infrastructure.enabled = NO;
                btn_ComputeModels.enabled = NO;
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_IT_Infrastructure.enabled = YES;
            }
        }
        
        
        // Step4ViewController
        if (step4ViewController)
        {
            if (!step4ViewController.isDidAppear
                || !step4ViewController.selectedAnswer
                || step4ViewController.selectedAnswer.length == 0)
            {
                // Invalid
                btn_ComputeModels.enabled = NO;
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_ComputeModels.enabled = YES;
            }
        }
        
        
        // Step5ViewController
        if (noPlan_Step5ViewController)
        {
            if (!noPlan_Step5ViewController.isDidAppear
                || !noPlan_Step5ViewController.selectedAnswer
                || noPlan_Step5ViewController.selectedAnswer.count == 0)
            {
                // Invalid
                btn_Results.enabled = NO;
                
                return;
            }
            else // Valid
            {
                btn_Results.enabled = YES;
            }
        }
    }
    
}

/***************************************************************/



#pragma mark - UIAlertViewDelegate

/***************************************************************/

//-------------------------------------------------------
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case kTag_AlertView_BackTo_StartMenu:
        {
            if (buttonIndex == 1) // Yes
            {
                // Back To StartViewController
                [self backToStartMenu_StartViewController];
            }
        }
            break;
            
        default:
            break;
    }
}

/***************************************************************/




#pragma mark - IBActions Menu's Buttons

/***************************************************************/

//-------------------------------------------------------
- (IBAction)btn_Start_Tapped:(id)sender {
    
    if (scrollerView_Content.contentOffset.x > 0)
    {
        // CurrentViewController
        if (scrollerView_Content.contentOffset.x == 1024)
        {
            // Back To StartViewController
            [self backToStartMenu_StartViewController];
        }
        else // Next ViewControllers
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Restart?" message:@"Your current selections will be lost. Do you want to restart?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            
            alertView.tag = kTag_AlertView_BackTo_StartMenu;
            [alertView show];
            
            alertView = nil;
            
            return;
        }
    }
}


//-------------------------------------------------------
- (IBAction)btn_Current_Tapped:(id)sender {
    
    // Add subview "Step3ViewController" if need
    if (!currentViewController)
    {
        [self addSubViewToMasterView_CurrentViewController];
    }
    
    // Passed Buttons
    [self passed_ButtonMenu:btn_Start];
   
    // Selected Button
    [self select_ButtonMenu:btn_Current];
    
    // DeselectButton
    [self deselect_ButtonMenu:btn_BussinessDrivers];
    [self deselect_ButtonMenu:btn_Management];
    [self deselect_ButtonMenu:btn_IT_Infrastructure];
    [self deselect_ButtonMenu:btn_ComputeModels];
    [self deselect_ButtonMenu:btn_Results];
    
    
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(1024, 0) animated:YES];
    
    // Check Enable "Menu Buttons" by Logic
    //    [self checkEnableForMenuButtons];
    
    
    // Set By Hand
    btn_BussinessDrivers.enabled = YES;
    btn_Current.enabled = NO;
    btn_Management.enabled = NO;
    btn_IT_Infrastructure.enabled = NO;
    btn_ComputeModels.enabled = NO;
    btn_Results.enabled = NO;
    
    
    // Set Default Value for this Step
    [currentViewController setStatusDefault_And_ClearCurrentAnswer];
    
    // Set default value for "Next Steps"
    [step3ViewController setStatusDefault_And_ClearCurrentAnswer];
    [step4ViewController setStatusDefault_And_ClearCurrentAnswer];
    
    if (isChoose_HasPlan)
    {
        [hasPlan_Step2ViewController setStatusDefault_And_ClearCurrentAnswer];
        [hasPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    else
    {
        [noPlan_Step2ViewController setStatusDefault_And_ClearCurrentAnswer];
        [noPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
}


//-------------------------------------------------------
- (IBAction)btn_BusinessDrivers_Tapped:(id)sender {
    
//    NoPlan_Step2ViewController || HasPlan_Step2ViewController
    
    // Add SubView
    if (isChoose_HasPlan && !hasPlan_Step2ViewController)
    {
        // HasPlan_Step2ViewController
        [self addSubViewToMasterView_HasPlan_Step2ViewController];
    }
    else // No Plan
        if (!isChoose_HasPlan && !noPlan_Step2ViewController)
        {
            // NoPlan_Step2ViewController
            [self addSubViewToMasterView_NoPlan_Step2ViewController];
        }
    
    // Passed Buttons
    [self passed_ButtonMenu:btn_Start];
    [self passed_ButtonMenu:btn_Current];
    
    // Selected Button
    [self select_ButtonMenu:btn_BussinessDrivers];
    
    // DeselectButton
    [self deselect_ButtonMenu:btn_Management];
    [self deselect_ButtonMenu:btn_IT_Infrastructure];
    [self deselect_ButtonMenu:btn_ComputeModels];
    [self deselect_ButtonMenu:btn_Results];
   
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(2048, 0) animated:YES];
    
    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    // Set By Hand
    btn_BussinessDrivers.enabled = YES;
    btn_Current.enabled = YES;
    btn_Management.enabled = NO;
    btn_IT_Infrastructure.enabled = NO;
    btn_ComputeModels.enabled = NO;
    btn_Results.enabled = NO;
    
    
    // Set Default Value for this Step
    if (isChoose_HasPlan)
    {
        [hasPlan_Step2ViewController setStatusDefault_And_ClearCurrentAnswer];
        [hasPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    else
    {
        [noPlan_Step2ViewController setStatusDefault_And_ClearCurrentAnswer];
        [noPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    
    // Set default value for "Next Steps"
    [step3ViewController setStatusDefault_And_ClearCurrentAnswer];
    [step4ViewController setStatusDefault_And_ClearCurrentAnswer];
}


//-------------------------------------------------------
- (IBAction)btn_Management_Tapped:(id)sender {
    
    // Add subview "Step3ViewController" if need
    if (!step3ViewController)
    {
        [self addSubViewToMasterView_Step3ViewController];
    }
    
    step3ViewController.isDidAppear = YES;
    
    // Passed Buttons
    [self passed_ButtonMenu:btn_Start];
    [self passed_ButtonMenu:btn_BussinessDrivers];
    [self passed_ButtonMenu:btn_Current];
    
    // Selected Button
    [self select_ButtonMenu:btn_Management];
    
    // DeselectButton
    [self deselect_ButtonMenu:btn_IT_Infrastructure];
    [self deselect_ButtonMenu:btn_ComputeModels];
    [self deselect_ButtonMenu:btn_Results];
    
    
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(3072, 0) animated:YES];

    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    
    // Set By Hand
    btn_BussinessDrivers.enabled = YES;
    btn_Current.enabled = YES;
    btn_Management.enabled = YES;
    btn_IT_Infrastructure.enabled = NO;
    btn_ComputeModels.enabled = NO;
    btn_Results.enabled = NO;
    
    
    // Set Default Value for this Step
    [step3ViewController setStatusDefault_And_ClearCurrentAnswer];
    
    // Set default value for "Next Steps"
    [step4ViewController setStatusDefault_And_ClearCurrentAnswer];
    
    if (isChoose_HasPlan)
    {
        [hasPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    else
    {
        [noPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
}


//-------------------------------------------------------
- (IBAction)btn_IT_Infrastructure:(id)sender {
    
    // Add subview "Step4ViewController" if need
    if (!step4ViewController)
    {
        [self addSubViewToMasterView_Step4ViewController];
    }
    
    step4ViewController.isDidAppear = YES;
    
    // Passed Buttons
    [self passed_ButtonMenu:btn_Start];
    [self passed_ButtonMenu:btn_BussinessDrivers];
    [self passed_ButtonMenu:btn_Current];
    [self passed_ButtonMenu:btn_Management];
    
    // Selected Button
    [self select_ButtonMenu:btn_IT_Infrastructure];
    
    // DeselectButton
    [self deselect_ButtonMenu:btn_ComputeModels];
    [self deselect_ButtonMenu:btn_Results];
    
    
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(4096, 0) animated:YES];
    
    
    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    
    // Set By Hand
    btn_BussinessDrivers.enabled = YES;
    btn_Current.enabled = YES;
    btn_Management.enabled = YES;
    btn_IT_Infrastructure.enabled = YES;
    btn_ComputeModels.enabled = NO;
    btn_Results.enabled = NO;
    
    
    // Set Default Value for this Step
    [step4ViewController setStatusDefault_And_ClearCurrentAnswer];
    
    [step4ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer];
    
    if (isChoose_HasPlan)
    {
        [hasPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    else
    {
        [noPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    
    
}


//-------------------------------------------------------
- (IBAction)btn_ComputeModels_Tapped:(id)sender {
    
    if (isChoose_HasPlan)
    {
        // Add subview "HasPlan_Step5ViewController" if need
        if (!hasPlan_Step5ViewController)
        {
            [self addSubViewToMasterView_HasPlan_Step5ViewController];
        }
        
        hasPlan_Step5ViewController.isDidAppear = YES;
        
        // Set Default Value for this Step
        [hasPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
        
        [hasPlan_Step5ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer andelectedAnswerOfStep4:step4ViewController.selectedAnswer];
    }
    else
    {
        // Add subview "NoPlan_Step5ViewController" if need
        if (!noPlan_Step5ViewController)
        {
            [self addSubViewToMasterView_NoPlan_Step5ViewController];
        }
        
        noPlan_Step5ViewController.isDidAppear = YES;
        
        // Set Default Value for this Step
        [noPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
        
        [noPlan_Step5ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer andelectedAnswerOfStep4:step4ViewController.selectedAnswer];
    }
    
    
    // NoPlan_Step5ViewController || HasPlan_Step5ViewControllerDelegate
    
    // Passed Buttons
    [self passed_ButtonMenu:btn_Start];
    [self passed_ButtonMenu:btn_BussinessDrivers];
    [self passed_ButtonMenu:btn_Current];
    [self passed_ButtonMenu:btn_Management];
    [self passed_ButtonMenu:btn_IT_Infrastructure];
    
    // Selected Button
    [self select_ButtonMenu:btn_ComputeModels];
    
    // DeselectButton
    [self deselect_ButtonMenu:btn_Results];
    
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(5120, 0) animated:YES];
    
    
    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    
    // Set By Hand
    btn_BussinessDrivers.enabled = YES;
    btn_Current.enabled = YES;
    btn_Management.enabled = YES;
    btn_IT_Infrastructure.enabled = YES;
    btn_ComputeModels.enabled = YES;
    btn_Results.enabled = NO;
    
    
//    NSLog(@"\n\nCompute :");
//    NSLog(@"step3ViewController.selectedAnswer = %@", step3ViewController.selectedAnswer);
//    NSLog(@"step4ViewController.selectedAnswer = %@", step4ViewController.selectedAnswer);
}


//-------------------------------------------------------
- (IBAction)btn_Results_Tapped:(id)sender {
    
//    NSLog(@"\n\nResult :");
//    NSLog(@"step3ViewController.selectedAnswer = %@", step3ViewController.selectedAnswer);
//    NSLog(@"step4ViewController.selectedAnswer = %@", step4ViewController.selectedAnswer);
   
    // Add subview "ResultPart1ViewController" if need
    if (!resultPart1ViewController)
    {
        [self addSubViewToMasterView_ResultPart1ViewController];
    }
    
    // Final Results
    NSMutableArray *finalResultAnswers;
    
    if (isChoose_HasPlan)
    {
        // Has Plan
        finalResultAnswers = [NSMutableArray arrayWithObjects:step3ViewController.selectedAnswer, step4ViewController.selectedAnswer, hasPlan_Step5ViewController.selectedAnswer, nil];
        
        resultPart1ViewController.dataObject = finalResultAnswers;
        
        resultPart1ViewController.groupAnswerStep2 = hasPlan_Step2ViewController.selectedAnswer;
    }
    else
    {
        // No Plan
        finalResultAnswers = [NSMutableArray arrayWithObjects:step3ViewController.selectedAnswer, step4ViewController.selectedAnswer, noPlan_Step5ViewController.selectedAnswer, nil];
        
        resultPart1ViewController.dataObject = finalResultAnswers;
        
        resultPart1ViewController.groupAnswerStep2 = noPlan_Step2ViewController.selectedAnswer;
    }
    
    
    // Add ResultPart1ViewController
    [resultPart1ViewController reloadData];
    
    // Passed Buttons
    [self passed_ButtonMenu:btn_Start];
    [self passed_ButtonMenu:btn_BussinessDrivers];
    [self passed_ButtonMenu:btn_Current];
    [self passed_ButtonMenu:btn_Management];
    [self passed_ButtonMenu:btn_IT_Infrastructure];
    [self passed_ButtonMenu:btn_ComputeModels];
    
    
    // Selected Button
    [self select_ButtonMenu:btn_Results];
    
    
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(6144, 0) animated:YES];
    
    
    // Set By Hand
    btn_BussinessDrivers.enabled = YES;
    btn_Current.enabled = YES;
    btn_Management.enabled = YES;
    btn_IT_Infrastructure.enabled = YES;
    btn_ComputeModels.enabled = YES;
    btn_Results.enabled = YES;
    
    
    
}


/***************************************************************/




#pragma mark - StartViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) startViewControllerDelegate_ChooseHasPlan
{
    isChoose_HasPlan = YES;
    
    [self addSubViewsToMasterView];
    
    [self btn_Current_Tapped:nil];
    
    btn_Current.enabled = YES;
    
    [self select_ButtonMenu:btn_Current];
    
    [self checkEnableForMenuButtons];    
}


//-------------------------------------------------------
-(void) startViewControllerDelegate_ChooseNoPlan
{
    isChoose_HasPlan = NO;
    
    [self addSubViewsToMasterView];
    
    [self btn_Current_Tapped:nil];
    
    btn_Current.enabled = YES;
    
    [self select_ButtonMenu:btn_Current];
    
    [self checkEnableForMenuButtons];
}

/***************************************************************/





#pragma mark - CurrentViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) currentViewControllerDelegate_GoNext
{
    [self btn_BusinessDrivers_Tapped:nil];
}


//-------------------------------------------------------
-(void) currentViewControllerDelegate_GoBack
{
    // Back To StartViewController
    [self backToStartMenu_StartViewController];
}


//-------------------------------------------------------
-(void) currentViewControllerDelegate_ChangedValue
{
    
}

/***************************************************************/




#pragma mark - NoPlan_Step2ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) noPlan_Step2ViewControllerDelegate_GoNext
{
    [self btn_Management_Tapped:nil];
}


//-------------------------------------------------------
-(void) noPlan_Step2ViewControllerDelegate_GoBack
{
    [self btn_Current_Tapped:nil];
}


//-------------------------------------------------------
-(void) noPlan_Step2ViewControllerDelegate_ChangedValue
{
    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    // Show Decription
//    [appDelegate.queueResultField showDecription];
}


/***************************************************************/




#pragma mark - HasPlan_Step2ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) hasPlan_Step2ViewControllerDelegate_GoNext
{
    [self btn_Management_Tapped:nil];
}


//-------------------------------------------------------
-(void) hasPlan_Step2ViewControllerDelegate_GoBack
{
    [self btn_Current_Tapped:nil];
}


//-------------------------------------------------------
-(void) hasPlan_Step2ViewControllerDelegate_ChangedValue
{
    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    // Show Decription
//    [appDelegate.queueResultField showDecription];
}


/***************************************************************/




#pragma mark - Step3ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) step3ViewControllerDelegate_GoNext
{
    [self btn_IT_Infrastructure:nil];
}


//-------------------------------------------------------
-(void) step3ViewControllerDelegate_GoBack
{
    // Go Back
    [self btn_BusinessDrivers_Tapped:nil];
}


//-------------------------------------------------------
-(void) step3ViewControllerDelegate_ChangedValue
{
//    if (step4ViewController.isDidAppear)
//    {
//        [step4ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer];
//        
//        if (isChoose_HasPlan)
//        {
//            [hasPlan_Step5ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer andelectedAnswerOfStep4:step4ViewController.selectedAnswer];
//        }
//        else
//        {
//            [noPlan_Step5ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer andelectedAnswerOfStep4:step4ViewController.selectedAnswer];
//        }
//        
//        
//        // Check Enable "Menu Buttons" by Logic
//        [self checkEnableForMenuButtons];
//
//        
//        // Show Decription
////        [appDelegate.queueResultField showDecription];
//    }
}


/***************************************************************/



#pragma mark - Step4ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) step4ViewControllerDelegate_GoNext
{
    [self btn_ComputeModels_Tapped:nil];
}


//-------------------------------------------------------
-(void) step4ViewControllerDelegate_GoBack
{
    // Set Default Value
    [step3ViewController setStatusDefault_And_ClearCurrentAnswer];
    
    [self btn_Management_Tapped:nil];
}


//-------------------------------------------------------
-(void) step4ViewControllerDelegate_ChangedValue
{
//    if (isChoose_HasPlan)
//    {
//        [hasPlan_Step5ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer andelectedAnswerOfStep4:step4ViewController.selectedAnswer];
//    }
//    else
//    {
//        [noPlan_Step5ViewController processWithSelectedAnswerOfStep3:step3ViewController.selectedAnswer andelectedAnswerOfStep4:step4ViewController.selectedAnswer];
//    }
//    
//    
//    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
//    
//    // Show Decription
////    [appDelegate.queueResultField showDecription];
}


/***************************************************************/



#pragma mark - NoPlan_Step5ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) noPlan_Step5ViewControllerDelegate_GoNext
{
    [self btn_Results_Tapped:nil];
}


//-------------------------------------------------------
-(void) noPlan_Step5ViewControllerDelegate_GoBack
{
    [self btn_IT_Infrastructure:nil];
}


//-------------------------------------------------------
-(void) noPlan_Step5ViewControllerDelegate_ChangedValue
{
    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    // Show Decription
//    [appDelegate.queueResultField showDecription];
}

/***************************************************************/



#pragma mark - HasPlan_Step5ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void) hasPlan_Step5ViewControllerDelegate_GoNext
{
    [self btn_Results_Tapped:nil];
}


//-------------------------------------------------------
-(void) hasPlan_Step5ViewControllerDelegate_GoBack
{
    // Set Default Value
    [step4ViewController setStatusDefault_And_ClearCurrentAnswer];
    
    [self btn_IT_Infrastructure:nil];
}


//-------------------------------------------------------
-(void) hasPlan_Step5ViewControllerDelegate_ChangedValue
{
    // Check Enable "Menu Buttons" by Logic
//    [self checkEnableForMenuButtons];
    
    // Show Decription
//    [appDelegate.queueResultField showDecription];
}

/***************************************************************/




#pragma mark - ResultPart1ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void)didResult1GoBack
{
    // Set Default Value
    if (isChoose_HasPlan)
    {
        [hasPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    else
    {
        [noPlan_Step5ViewController setStatusDefault_And_ClearCurrentAnswer];
    }
    
    
    // Go Back
    [self btn_ComputeModels_Tapped:nil];
}


//-------------------------------------------------------
-(void)didResult1SeeFullModel: (ResultField *)result
{
    // Add subview "ResultPart2ViewController" if need
    if (!resultPart2ViewController)
    {
        [self addSubViewToMasterView_ResultPart2ViewController];
    }

    resultPart2ViewController.result = result;
    
    [resultPart2ViewController viewWillAppear:NO];
    
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(7168, 0) animated:YES];
    
    
//    QueueResultField *tmp = appDelegate.queueResultField;
}

/***************************************************************/




#pragma mark - ResultPart2ViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void)didResult2GoBack
{
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(6144, 0) animated:YES];
}


//-------------------------------------------------------
-(void)didResult2Restart
{
    [self btn_Start_Tapped:nil];
}


//-------------------------------------------------------
-(void)didResult2SendResult:(ResultField *)result
{
    // Scroll View
//    [scrollerView_Content setContentOffset:CGPointMake(7168, 0) animated:YES];
    
    if (!resultPopUpViewController)
    {
        resultPopUpViewController = [ResultPopUpViewController new];
        resultPopUpViewController.delegate = self;
        resultPopUpViewController.view.alpha = 0;
        [self.view addSubview:resultPopUpViewController.view];
    }
    
    resultPopUpViewController.result =  result;
   [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
       resultPopUpViewController.view.alpha = 1;
   } completion:^(BOOL finished) {
       
   }];


}

/***************************************************************/




#pragma mark - ResultPopUpViewControllerDelegate

/***************************************************************/

//-------------------------------------------------------
-(void)didPopUpCancel
{
    // Scroll View
    [scrollerView_Content setContentOffset:CGPointMake(7168, 0) animated:YES];
    
    [UIView animateWithDuration:0.3 animations:^{
        resultPopUpViewController.view.alpha = 0;
    } completion:^(BOOL finished) {
        [resultPopUpViewController.view removeFromSuperview];
        resultPopUpViewController = nil;
    }];
}


//-------------------------------------------------------
-(void)didPopUpSumbit
{
    
}


//-------------------------------------------------------
-(void)didPopUpRestart
{
    [self btn_Start_Tapped:nil];
}

/***************************************************************/

@end
