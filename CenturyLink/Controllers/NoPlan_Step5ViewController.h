//
//  NoPlan_Step5ViewController.h
//  CenturyLink
//
//  Created by MacMini  on 11/7/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol NoPlan_Step5ViewControllerDelegate <NSObject>

-(void) noPlan_Step5ViewControllerDelegate_GoNext;

-(void) noPlan_Step5ViewControllerDelegate_GoBack;

-(void) noPlan_Step5ViewControllerDelegate_ChangedValue;

@end


@interface NoPlan_Step5ViewController : UIViewController
{
    __weak IBOutlet UILabel *lb_Title;
    __weak IBOutlet UILabel *lb_smallDecription;
    
    // CheckBox
    __weak IBOutlet UIButton *btn_checkBox1;
    __weak IBOutlet UIButton *btn_checkBox2;
    __weak IBOutlet UIButton *btn_checkBox3;
    __weak IBOutlet UIButton *btn_checkBox4;
    
    // Footer
    __weak IBOutlet UIButton *btn_Back;
    __weak IBOutlet UIButton *btn_Next;
}


// Properties
@property(nonatomic, weak) id<NoPlan_Step5ViewControllerDelegate> delegate;

@property(nonatomic, strong) NSMutableArray *selectedAnswer;

@property(nonatomic, assign) BOOL isDidAppear;



// Methods
-(void) processWithSelectedAnswerOfStep3:(NSString *) selectedAnswerOfStep3 andelectedAnswerOfStep4:(NSString *) selectedAnswerOfStep4;

-(void) setStatusDefault_And_ClearCurrentAnswer;


@end
