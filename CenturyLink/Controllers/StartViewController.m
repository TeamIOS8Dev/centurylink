//
//  StartViewController.m
//  CenturyLink
//
//  Created by MacMini  on 11/5/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "StartViewController.h"

#import "Common.h"
#import "SaveResultToQueue.h"


@interface StartViewController ()
{
    AppDelegate *appDelegate;
}

@property (nonatomic,strong) SaveResultToQueue *saveResultToQueue;

@end

@implementation StartViewController


// Properties
@synthesize delegate;


#pragma mark - Init View

/***************************************************************/


//-------------------------------------------------------
-(void) prepareForLayout
{
    lb_Title.font = kFont_UniversLTStd_Light(30);
    
    btn_ChooseHasPlan.exclusiveTouch = YES;
    btn_ChooseNoPlan.exclusiveTouch = YES;   
}


//-------------------------------------------------------
- (void)viewWillAppear:(BOOL)animated
{
            [super viewWillAppear:YES];
    [self prepareForLayout];
}


//-------------------------------------------------------
- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    // Prepare Layout
    [self prepareForLayout];
    
    
    // Get AppDelegate
    appDelegate = [Common getAppDelegate];
}


//-------------------------------------------------------
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/***************************************************************/




#pragma mark - IBActions

/***************************************************************/


//-------------------------------------------------------
- (IBAction)btn_ChooseHasPlan_Tapped:(id)sender
{
    
    // Save Plan Status
    appDelegate.queueResultField.planStatus = @"I already have a plan";
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(startViewControllerDelegate_ChooseHasPlan)])
    {
        [self.delegate startViewControllerDelegate_ChooseHasPlan];
    }
}


//-------------------------------------------------------
- (IBAction)btn_ChooseNoPlan_Tapped:(id)sender
{
    // Save Plan Status
    appDelegate.queueResultField.planStatus = @"I do not yet have a plan";
    
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(startViewControllerDelegate_ChooseNoPlan)])
    {
        [self.delegate startViewControllerDelegate_ChooseNoPlan];
    }
}

/***************************************************************/


/********************************************************************************/
#pragma mark - Submit all data in queue

- (IBAction)action_submitAllDataInQueue:(id)sender {
    
    if (![Common checkNetwork]) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Message" message:@"Internet connection not available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        return;
    }
    
    
    //  get all queue data
    QueueResultModel *queueResultModel = [QueueResultModel new];
    NSMutableArray *queueData = [queueResultModel getAllRowsInTableQueueResult];
    
    if ([queueData count] != 0)
    {
        //  submit
        _saveResultToQueue = [SaveResultToQueue new];
        [_saveResultToQueue submitAllQeueDataInDatabase:queueData];
    }
    else
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Message" message:@"No queued responses" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
        
        [alert show];
    }

    
    
}


/********************************************************************************/
@end
