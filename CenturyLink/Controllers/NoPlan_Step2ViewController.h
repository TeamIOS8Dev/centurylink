//
//  Step2ViewController.h
//  CenturyLink
//
//  Created by MacMini  on 11/6/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol NoPlan_Step2ViewControllerDelegate <NSObject>

-(void) noPlan_Step2ViewControllerDelegate_GoNext;

-(void) noPlan_Step2ViewControllerDelegate_GoBack;

-(void) noPlan_Step2ViewControllerDelegate_ChangedValue;

@end



@interface NoPlan_Step2ViewController : UIViewController
{
    __weak IBOutlet UILabel *lb_Title;
    
    // CheckBox
    __weak IBOutlet UIButton *btn_checkBox1;
    __weak IBOutlet UIButton *btn_checkBox2;
    __weak IBOutlet UIButton *btn_checkBox3;
    
    __weak IBOutlet UIButton *btn_checkBox4;
    __weak IBOutlet UIButton *btn_checkBox5;
    __weak IBOutlet UIButton *btn_checkBox6;
    
    
    // Footer
    __weak IBOutlet UIButton *btn_Back;
    __weak IBOutlet UIButton *btn_Next;
}


// Properties
@property(nonatomic, weak) id<NoPlan_Step2ViewControllerDelegate> delegate;

@property(nonatomic, strong) NSMutableArray *selectedAnswer;


// Methods
-(void) setStatusDefault_And_ClearCurrentAnswer;

@end
