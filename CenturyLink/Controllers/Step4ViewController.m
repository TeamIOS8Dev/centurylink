//
//  Step3ViewController.m
//  CenturyLink
//
//  Created by MacMini  on 11/7/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "Step4ViewController.h"
#import "Common.h"

@interface Step4ViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation Step4ViewController


// Properties
@synthesize delegate;
@synthesize selectedAnswer;


@synthesize isDidAppear;
@synthesize isHasPlan;


UIFont *font_LabelSlider_When_NoSelected;
UIFont *font_LabelSlider_When_Selected;
UIColor *color_LabelSlider_When_Selected;
UIColor *color_LabelSlider_When_NoSelected;



#pragma mark - Init View

/***************************************************************/

//-------------------------------------------------------
-(void) prepareForLayout
{
    lb_Title.font = kFont_UniversLTStd_Light(30);
    
    lb_smallDecription.font = kFont_UniversLTStd_Italic(13);
    lb_TipTop.font =  kFont_UniversLTStd_Italic(13);
    lb_TipTop.textColor = [Common resultContentColor];
    
    font_LabelSlider_When_NoSelected = kFont_UniversLTStd_Bold(14);
    font_LabelSlider_When_Selected = kFont_UniversLTStd_Bold(14);
    
    color_LabelSlider_When_Selected = [UIColor colorWithRed:0 green:133/255.0f blue:63/255.0f alpha:1.0f];
    color_LabelSlider_When_NoSelected = [UIColor colorWithRed:238/255.0f green:238/255.0f blue:238/255.0f alpha:1.0f];
    
    [self deseclectAllLabelOf_Slider1];
    
    
    // SliderView1
    UIImage *image_Thumb = [UIImage imageNamed:@"slider_Button.png"];
    [sliderView1 setThumbImage:image_Thumb forState:UIControlStateNormal];
    [sliderView1 setThumbImage:image_Thumb forState:UIControlStateHighlighted];
    
    UIImage *minTrackImage = [UIImage imageNamed:@"trackLine.png"];
    [sliderView1 setMinimumTrackImage:minTrackImage forState:UIControlStateNormal];
    [sliderView1 setMaximumTrackImage:minTrackImage forState:UIControlStateNormal];
    
    
    // Tap on SliderView1
    UITapGestureRecognizer *tapOnSlider = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderView1Tapped:)];
    [sliderView1 addGestureRecognizer:tapOnSlider];
    
    tapOnSlider = nil;
    
    
    // Slide End Sliding
    [sliderView1 addTarget:self action:@selector(sliderDidEndSliding:) forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    
    
    // Footer
    btn_Back.exclusiveTouch = YES;
    btn_Next.exclusiveTouch = YES;
}


//-------------------------------------------------------
- (void)viewWillAppear:(BOOL)animated
{
            [super viewWillAppear:YES];
    lb_Title.text = (isHasPlan == YES) ? @"Where will the IT infrastructure be physically located?" : @"Where would the infrastructure be located?";
}


//-------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self prepareForLayout];
    
    // Get AppDelegate
    appDelegate = [Common getAppDelegate];
    
    
    
    // Default Value
    sliderView1.value = 0.5f;
    
    [self getAnswer];
    
    [self setStatusByValueOf_SliderView1];
    
    
    
}


//-------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


/***************************************************************/



#pragma mark - Ulities

/***************************************************************/

//-------------------------------------------------------
-(void) setStatusDefault_And_ClearCurrentAnswer
{
    // Default Value
    sliderView1.value = 0.5f;
    
    [self getAnswer];
    
    [self setStatusByValueOf_SliderView1];
}


//-------------------------------------------------------
-(void) getAnswer
{
    NSString *oldValue = selectedAnswer;
    
    if (sliderView1.value == 0)
    {
        selectedAnswer = @"d";
    }
    
    if (sliderView1.value == 0.25f
        || sliderView1.value == 0.5f
        || sliderView1.value == 0.75f)
    {
        selectedAnswer = @"f";
    }
    
    if (sliderView1.value == 1.0f)
    {
        selectedAnswer = @"e";
    }

    
    // Save Result
    int value = sliderView1.value * 100;
    
    switch (value) {
        case 0:
        {
            appDelegate.queueResultField.infrastructureLocation = @"100% / 0%";
            
            break;
        }
            
        case 25:
        {
            appDelegate.queueResultField.infrastructureLocation = @"75% / 25%";
            
            break;
        }
            
        case 50:
        {
            appDelegate.queueResultField.infrastructureLocation = @"50% / 50%";
            
            break;
        }
            
        case 75:
        {
            appDelegate.queueResultField.infrastructureLocation = @"25% / 75%";
            
            break;
        }
            
        case 100:
        {
            appDelegate.queueResultField.infrastructureLocation = @"0% / 100%";
            
            break;
        }
            
        default:
            break;
    }
    
    
    // Changed Value
    if (![oldValue isEqualToString:selectedAnswer])
    {
        if (self.delegate
            && [self.delegate respondsToSelector:@selector(step4ViewControllerDelegate_ChangedValue)])
        {
            [self.delegate step4ViewControllerDelegate_ChangedValue];
        }
    }
}


//-------------------------------------------------------
-(void) processWithSelectedAnswerOfStep3:(NSString *) selectedAnswerOfStep3
{
    if ([selectedAnswerOfStep3 isEqualToString:@"b"])
    {
        sliderView1.value = 1.0f;
        [self setStatusByValueOf_SliderView1];
        
        // Lock slider
        sliderView1.userInteractionEnabled = NO;
        sliderView1.alpha = 0.5f;
        
        lb_smallDecription.text = @"You chose 100% 3rd-Party Managed";
        lb_smallDecription.textColor = [UIColor redColor];
    }
    else
    {
        // Unclock slider
        sliderView1.userInteractionEnabled = YES;
        sliderView1.alpha = 1.0f;
        
        lb_smallDecription.text = @"* Please slide left or right";
        
        lb_smallDecription.textColor = [UIColor colorWithRed:97/255.0f green:99/255.0f blue:101/255.0f alpha:1.0f];
    }
    
    
    // Get Answer
    [self getAnswer];
    
}


//-------------------------------------------------------
-(void) deseclectAllLabelOf_Slider1
{
    lb_0_100.font = font_LabelSlider_When_NoSelected;
    lb_0_100.textColor = color_LabelSlider_When_NoSelected;
    
    lb_25_75.font = font_LabelSlider_When_NoSelected;
    lb_25_75.textColor = color_LabelSlider_When_NoSelected;
    
    lb_50_50.font = font_LabelSlider_When_NoSelected;
    lb_50_50.textColor = color_LabelSlider_When_NoSelected;
    
    lb_75_25.font = font_LabelSlider_When_NoSelected;
    lb_75_25.textColor = color_LabelSlider_When_NoSelected;
    
    lb_100_0.font = font_LabelSlider_When_NoSelected;
    lb_100_0.textColor = color_LabelSlider_When_NoSelected;
}


//-------------------------------------------------------
-(void) setStatusByValueOf_SliderView1
{
    // Status normal all Label Slider
    [self deseclectAllLabelOf_Slider1];
    
    // 100% - 0% : 0 ... 0,13
    if (sliderView1.value >= 0 && sliderView1.value <= 0.13f)
    {
        lb_100_0.font = font_LabelSlider_When_Selected;
        lb_100_0.textColor = color_LabelSlider_When_Selected;
        
        image_DataCenter.hidden = NO;
        image_Third.hidden = YES;
        
        image_DataCenter.image = [UIImage imageNamed:@"Selector-100"];
        
        return;
    }
    
    // 75% - 25% : 0,13 ... 0,38
    if (sliderView1.value > 0.13f && sliderView1.value <= 0.38f)
    {
        lb_75_25.font = font_LabelSlider_When_Selected;
        lb_75_25.textColor = color_LabelSlider_When_Selected;
        
        image_DataCenter.hidden = NO;
        image_Third.hidden = NO;
        
        image_DataCenter.image = [UIImage imageNamed:@"Selector-75_25"];
        image_Third.image = [UIImage imageNamed:@"Selector-75_25_right"];
        
        return;
    }
    
    // 50% - 50% : 0,38 ... 0,63
    if (sliderView1.value > 0.38f && sliderView1.value <= 0.63f)
    {
        lb_50_50.font = font_LabelSlider_When_Selected;
        lb_50_50.textColor = color_LabelSlider_When_Selected;
        
        image_DataCenter.hidden = NO;
        image_Third.hidden = NO;
        
        image_DataCenter.image = [UIImage imageNamed:@"Selector-50_50"];
        image_Third.image = [UIImage imageNamed:@"Selector-50_50_right"];
        
        return;
    }
    
    // 75% - 25% : 0,63 ... 0,88
    if (sliderView1.value > 0.63f && sliderView1.value <= 0.88f)
    {
        lb_25_75.font = font_LabelSlider_When_Selected;
        lb_25_75.textColor = color_LabelSlider_When_Selected;
        
        image_DataCenter.hidden = NO;
        image_Third.hidden = NO;
        
        image_DataCenter.image = [UIImage imageNamed:@"Selector-25_75"];
        image_Third.image = [UIImage imageNamed:@"Selector-25_75_right"];
        
        return;
    }
    
    // 0% - 100% : 0,88 ... 1
    if (sliderView1.value > 0.88f && sliderView1.value <= 1.0f)
    {
        lb_0_100.font = font_LabelSlider_When_Selected;
        lb_0_100.textColor = color_LabelSlider_When_Selected;
        
        
        image_DataCenter.hidden = YES;
        image_Third.hidden = NO;
        
        image_Third.image = [UIImage imageNamed:@"Selector-100_right"];
        
        return;
    }
}

/***************************************************************/




#pragma mark - Slider IBActions

/***************************************************************/

//-------------------------------------------------------
- (void)sliderDidEndSliding:(NSNotification *)notification
{
    if (sliderView1.value >= 0 && sliderView1.value <= 0.13f)
    {
        sliderView1.value = 0;
        
        [self getAnswer];
        
        return;
    }
    
    // 75% - 25% : 0,13 ... 0,38
    if (sliderView1.value > 0.13f && sliderView1.value <= 0.38f)
    {
        sliderView1.value = 0.25f;
        
        [self getAnswer];
        
        return;
    }
    
    // 50% - 50% : 0,38 ... 0,63
    if (sliderView1.value > 0.38f && sliderView1.value <= 0.63f)
    {
        sliderView1.value = 0.5f;
        
        [self getAnswer];
        
        return;
    }
    
    // 75% - 25% : 0,63 ... 0,88
    if (sliderView1.value > 0.63f && sliderView1.value <= 0.88f)
    {
        sliderView1.value = 0.75f;
        
        [self getAnswer];
        
        return;
    }
    
    // 0% - 100% : 0,88 ... 1
    if (sliderView1.value >= 0.88f && sliderView1.value <= 1.0f)
    {
        sliderView1.value = 1.0f;
        
        [self getAnswer];
        
        return;
    }
}


//-------------------------------------------------------
- (IBAction)sliderView1_ValueChanged:(id)sender {
    
    //    NSLog(@"value = %f", sliderView1.value);
    [self setStatusByValueOf_SliderView1];
}


//-------------------------------------------------------
- (void)sliderView1Tapped:(UIGestureRecognizer *)tapGesture
{
    UISlider *slider = (UISlider *) tapGesture.view;
    
//    if (slider.highlighted)
//    {
//        // tap on thumb, let slider deal with it
//        return;
//    }
    
    if (slider == sliderView1)
    {
        CGPoint point = [tapGesture locationInView:slider];
        CGFloat percentage = point.x / slider.bounds.size.width;
        CGFloat delta = percentage * (slider.maximumValue - slider.minimumValue);
        CGFloat value = slider.minimumValue + delta;
        
        [slider setValue:value animated:NO];
        
        [self sliderDidEndSliding:nil];
        
        [self setStatusByValueOf_SliderView1];
    }
}

/***************************************************************/




#pragma mark - IBActions

/***************************************************************/

//-------------------------------------------------------
- (IBAction)btn_Back_Tapped:(id)sender {
    
    // Set Default
    [self setStatusDefault_And_ClearCurrentAnswer];
    
    // Go Back    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(step4ViewControllerDelegate_GoBack)])
    {
        [self.delegate step4ViewControllerDelegate_GoBack];
    }
    
}


//-------------------------------------------------------
- (IBAction)btn_Next_Tapped:(id)sender {
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(step4ViewControllerDelegate_GoNext)])
    {
        [self.delegate step4ViewControllerDelegate_GoNext];
    }
    
}


/***************************************************************/

@end
