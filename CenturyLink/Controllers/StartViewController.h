//
//  StartViewController.h
//  CenturyLink
//
//  Created by MacMini  on 11/5/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol StartViewControllerDelegate <NSObject>

-(void) startViewControllerDelegate_ChooseHasPlan;

-(void) startViewControllerDelegate_ChooseNoPlan;

@end


@interface StartViewController : UIViewController
{
    // Controls
    __weak IBOutlet UILabel *lb_Title;
    
    
    __weak IBOutlet UIButton *btn_ChooseHasPlan;
    __weak IBOutlet UIButton *btn_ChooseNoPlan;   
    
}


// Properties
@property(nonatomic, weak) id<StartViewControllerDelegate> delegate;

//  action
- (IBAction)action_submitAllDataInQueue:(id)sender;

@end
