//
//  Step4ViewController.h
//  CenturyLink
//
//  Created by MacMini  on 11/7/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol Step4ViewControllerDelegate <NSObject>

-(void) step4ViewControllerDelegate_GoNext;

-(void) step4ViewControllerDelegate_GoBack;

-(void) step4ViewControllerDelegate_ChangedValue;

@end


@interface Step4ViewController : UIViewController
{
    __weak IBOutlet UILabel *lb_Title;
    __weak IBOutlet UILabel *lb_smallDecription;
    __weak IBOutlet UILabel *lb_TipTop;
    
    __weak IBOutlet UILabel *lb_100_0;
    __weak IBOutlet UILabel *lb_75_25;
    __weak IBOutlet UILabel *lb_50_50;
    __weak IBOutlet UILabel *lb_25_75;
    __weak IBOutlet UILabel *lb_0_100;
    __weak IBOutlet UISlider *sliderView1;
    
    __weak IBOutlet UIImageView *image_DataCenter;
    __weak IBOutlet UIImageView *image_Third;
    
    
    // Footer
    __weak IBOutlet UIButton *btn_Back;
    __weak IBOutlet UIButton *btn_Next;
}


// Properties
@property(nonatomic, weak) id<Step4ViewControllerDelegate> delegate;

@property(nonatomic, weak) NSString *selectedAnswer;

@property(nonatomic, assign) BOOL isDidAppear;

@property (nonatomic, assign) BOOL isHasPlan;



// Methods
-(void) processWithSelectedAnswerOfStep3:(NSString *) selectedAnswerOfStep3;

-(void) setStatusDefault_And_ClearCurrentAnswer;

@end