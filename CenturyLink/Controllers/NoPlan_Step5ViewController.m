//
//  NoPlan_Step5ViewController.m
//  CenturyLink
//
//  Created by MacMini  on 11/7/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "NoPlan_Step5ViewController.h"
#import "Common.h"

@interface NoPlan_Step5ViewController ()
{
    // Properties
    int numberCheckBox_Selected; // == 0
    
    AppDelegate *appDelegate;
}
@end

@implementation NoPlan_Step5ViewController


// Properties
@synthesize selectedAnswer;
@synthesize delegate;

@synthesize isDidAppear;


#pragma mark - Init View

/***************************************************************/

//-------------------------------------------------------
-(void) prepareForLayout
{
    lb_Title.font = kFont_UniversLTStd_Light(30);
    lb_smallDecription.font = kFont_UniversLTStd_Italic(13);
    
    // Checkbox
    UIFont *font_ButtonCheckBox = kFont_UniversLTStd(18);
    
    btn_checkBox1.exclusiveTouch = YES;
    btn_checkBox1.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox2.exclusiveTouch = YES;
    btn_checkBox2.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox3.exclusiveTouch = YES;
    btn_checkBox3.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox4.exclusiveTouch = YES;
    btn_checkBox4.titleLabel.font = font_ButtonCheckBox;
    
    
    // Footer
    btn_Back.exclusiveTouch = YES;
    btn_Next.exclusiveTouch = YES;
}


//-------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self prepareForLayout];
    
    // Get AppDelegate
    appDelegate = [Common getAppDelegate];
}


//-------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/***************************************************************/




#pragma mark - Ulities

/***************************************************************/

//-------------------------------------------------------
-(void) setStatusDefault_And_ClearCurrentAnswer
{
    [btn_checkBox1 setSelected:NO];
    [btn_checkBox2 setSelected:NO];
    [btn_checkBox3 setSelected:NO];
    [btn_checkBox4 setSelected:NO];    
    
    // GetAnswer
    [self getAnswer];
}

//-------------------------------------------------------
-(void) getAnswer
{
    NSString *text_SelectedValue = @"";
    
    if (!selectedAnswer)
    {
        selectedAnswer = [NSMutableArray new];
    }
    
    [selectedAnswer removeAllObjects];
    
    
    if (btn_checkBox1.isSelected)
    {
        text_SelectedValue = btn_checkBox1.titleLabel.text;
        
        [selectedAnswer addObject:@"g"];
    }
    
    if (btn_checkBox2.isSelected)
    {
        if (text_SelectedValue.length > 0)
            text_SelectedValue = [text_SelectedValue stringByAppendingString:@";"];
        
        text_SelectedValue = [text_SelectedValue stringByAppendingString:btn_checkBox2.titleLabel.text];
        
        [selectedAnswer addObject:@"h"];
    }
    
    if (btn_checkBox3.isSelected)
    {
        if (text_SelectedValue.length > 0)
            text_SelectedValue = [text_SelectedValue stringByAppendingString:@";"];
        
        text_SelectedValue = [text_SelectedValue stringByAppendingString:btn_checkBox3.titleLabel.text];
        
        [selectedAnswer addObject:@"i"];
    }
    
    if (btn_checkBox4.isSelected)
    {
        if (text_SelectedValue.length > 0)
            text_SelectedValue = [text_SelectedValue stringByAppendingString:@";"];
        
        text_SelectedValue = [text_SelectedValue stringByAppendingString:btn_checkBox4.titleLabel.text];
        
        [selectedAnswer addObject:@"j"];
    }
    
   
    // lb_smallDecription
    if (selectedAnswer.count == 0)
    {
        lb_smallDecription.textColor = [UIColor redColor];
    }
    else
    {
        lb_smallDecription.textColor = [UIColor colorWithRed:97/255.0f green:99/255.0f blue:101/255.0f alpha:1.0f];
    }
    
    
    btn_Next.enabled = (selectedAnswer.count > 0) ? YES : NO;
    
    // Save Result
    appDelegate.queueResultField.computeModels = text_SelectedValue;
    
    
    // Changed Value
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(noPlan_Step5ViewControllerDelegate_ChangedValue)])
    {
        [self.delegate noPlan_Step5ViewControllerDelegate_ChangedValue];
    }

}



//-------------------------------------------------------
-(void) processWithSelectedAnswerOfStep3:(NSString *) selectedAnswerOfStep3 andelectedAnswerOfStep4:(NSString *) selectedAnswerOfStep4
{
    btn_checkBox1.enabled = YES;
    btn_checkBox2.enabled = YES;
    btn_checkBox3.enabled = YES;
    btn_checkBox4.enabled = YES;
    
    if ([selectedAnswerOfStep3 isEqualToString:@"b"])
    {
        btn_checkBox1.enabled = NO;
        [btn_checkBox1 setSelected:NO];
    }
    
    if ([selectedAnswerOfStep3 isEqualToString:@"a"])
    {
        btn_checkBox2.enabled = NO;
        [btn_checkBox2 setSelected:NO];
    }
    
    if ([selectedAnswerOfStep3 isEqualToString:@"a"]
        || [selectedAnswerOfStep4 isEqualToString:@"d"])
    {
        btn_checkBox3.enabled = NO;
        [btn_checkBox3 setSelected:NO];
        
        btn_checkBox4.enabled = NO;
        [btn_checkBox4 setSelected:NO];
    }
    
    // Get Answer
    [self getAnswer];
}

/***************************************************************/




#pragma mark - IBActions

/***************************************************************/

//-------------------------------------------------------
- (IBAction)btn_CheckBox_Tapped:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    
    [btn setSelected:!btn.isSelected];
    
    // Get Answer
    [self getAnswer];
}


//-------------------------------------------------------
- (IBAction)btn_Back_Tapped:(id)sender {
    
    // Set Default
    [self setStatusDefault_And_ClearCurrentAnswer];
    
    // Go Back
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(noPlan_Step5ViewControllerDelegate_GoBack)])
    {
        [self.delegate noPlan_Step5ViewControllerDelegate_GoBack];
    }
}


//-------------------------------------------------------
- (IBAction)btn_Next_Tapped:(id)sender {
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(noPlan_Step5ViewControllerDelegate_GoNext)])
    {
        [self.delegate noPlan_Step5ViewControllerDelegate_GoNext];
    }
}

/***************************************************************/




@end
