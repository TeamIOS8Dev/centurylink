//
//  ResultPopUpViewController.m
//  CenturyLink
//
//  Created by Gau Uni on 11/10/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "ResultPopUpViewController.h"
#import "PopUpTableViewCell.h"
#import "Common.h"
#import "SaveResultToQueue.h"
#import "Reachability.h"

typedef void (^ completionBlock)();

typedef enum : NSUInteger {
    kCellFirstName,
    kCellLastName,
    kCellEmail,
    kCellJobTitle,
    kCellCompany,
    kCellBusinessPhone,
    kCellCountry,
    kCellZipCode,
    kCellPromoCode,
    kCellSalesRep,
} kCellOrder;
@interface ResultPopUpViewController ()

@property (nonatomic, strong) NSArray *listInfo;
@property (nonatomic, strong) NSArray *listCountry;
@property (nonatomic, strong) UIView *alertView;
@property (nonatomic, strong) UILabel *alertLabel;
@property (nonatomic, strong) UIButton *alertDismissButton;
@property (nonatomic, strong) UIImageView *alertImageView;

//AccessoryView
@property (nonatomic, strong)  UIView *inputAccView;
@property (nonatomic, strong)  UIButton *btnNext;
@property (nonatomic, strong)  UIButton *btnPrev;

//@property (nonatomic, strong) UIImageView *alertImageView;

//  varriable
@property (nonatomic,strong) SaveResultToQueue *saveResultToQueue;

@end

@implementation ResultPopUpViewController

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/
#define kDEFAULT_CONTENTSIZE 768
//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    _btn_Cancel.exclusiveTouch = _btn_Submit.exclusiveTouch = YES;
    _tableView_Country.delegate = self;
    _tableView_Country.dataSource = self;
//    _tableView_Country.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView_Country.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView_Country.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    _view_ContainTableCountry.alpha = 0;
    _tableView_Country.layer.borderColor = [UIColor blackColor].CGColor;
    _tableView_Country.layer.borderWidth = 1;
    _tableView_Country.backgroundColor = [UIColor whiteColor];
    
    self.view.backgroundColor = [UIColor clearColor];

    _scrollView_Form.backgroundColor = [UIColor clearColor];
    _scrollView_Form.scrollEnabled = NO;
    _contentView_Form.backgroundColor = [UIColor clearColor];
    _imageView_Background.backgroundColor = [UIColor blackColor];
    _imageView_Background.alpha = 0.8;
    
    _view_Form.layer.cornerRadius = 10;
    _view_Form.layer.borderColor = [UIColor blackColor].CGColor;
    _view_Form.layer.borderWidth = 1;
    _view_Form.backgroundColor = [UIColor whiteColor];
    
     _btn_Restart.alpha = _btn_Dismiss.alpha = 0;
    
    [self setUpInputFields];
    [self setUpAlertView];
    [self createInputAccessoryView];
    
    _label_SendResult.font = kFont_UniversLTStd(30);
    _label_Title.font = kFont_UniversLTStd(14);
    _label_CheckboxTitle.font = kFont_UniversLTStd_Light(15);

    _label_SaleCheckboxTitle.font = kFont_UniversLTStd(16);
        _label_SaleCheckboxTitle.textColor = [Common resultContentColor];
//    _label_Title.text = @"Fusce dapibus, tellus ac cursus commodo, tortor mauris condium nibh, ut fermentum massa justo sit amet risus. Integer posuere erat ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.";
    
    _label_Title.textColor = [Common resultContentColor];
    _label_Title.alpha = 0;
    
    _constraint_ContentSize_ScrollView.constant = kDEFAULT_CONTENTSIZE;

}


//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    NSString *pathToListCountryFile = [[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"json"];

    NSData *jsonData = [NSData dataWithContentsOfFile:pathToListCountryFile];
    NSError *error = nil;
    _listCountry = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    if (_listCountry) {
        // do something with jsonArray
    } else {
        NSLog(@"Couldn't load JSON from %@: %@", pathToListCountryFile, error);
    }
    
    [_tableView_Country reloadData];

    
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSubmitResult:)
                                                 name:kNotificationFailed
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSubmitResult:)
                                                 name:kNotificationSuccess
                                               object:nil];
}

/*************************************************************************************************************************/



#pragma mark -
#pragma mark AlertView
/*************************************************************************************************************************/
#define kKeyboardHeight 352
#define kAlertHeight 44
//------------------------------------------------------------------------------------------
/**   */
-(void)setUpAlertView
{
    _alertView = [[UIView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.frame) - CGRectGetWidth(_view_Form.frame) )/2 , CGRectGetHeight(self.view.frame) - kKeyboardHeight - 40.0 - kAlertHeight, CGRectGetWidth(_view_Form.frame), kAlertHeight)];
    _alertView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"NotificationBackgroundError.png"]];

    _alertView.alpha = 0;
    
    

    
    [self.view addSubview:_alertView];
    
    _alertImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, (kAlertHeight - 18)/2, 17, 18)];
    _alertImageView.image = [UIImage imageNamed:@"NotificationBackgroundErrorIcon.png"];
    [_alertView addSubview:_alertImageView];
    
    _alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_alertImageView.frame) + 30, 0, CGRectGetWidth(_alertView.frame) - CGRectGetMaxX(_alertImageView.frame) - 10 - 30, CGRectGetHeight(_alertView.frame))];
    _alertLabel.numberOfLines = 0;
    _alertLabel.textColor = [UIColor whiteColor];
    [_alertView addSubview:_alertLabel];
    
    _alertDismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _alertDismissButton.backgroundColor = [UIColor clearColor];
    _alertDismissButton.frame = CGRectMake(0, 0, CGRectGetWidth(_alertView.frame), CGRectGetHeight(_alertView.frame));
    [_alertDismissButton addTarget:self action:@selector(btn_DismissAlert_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    [_alertView addSubview:_alertDismissButton];
}

//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_DismissAlert_Pressed:(id)sender
{
    [self dismissAlertViewWithBlock:nil];
}

#define kAlertMessageAnimationDuration  0.3
//------------------------------------------------------------------------------------------
/**   */
-(void)showAlertViewWithMessage: (NSString *)message autoDismiss: (BOOL)automatic
{
    _alertLabel.text = message;
  
    [self dismissAlertViewWithBlock:^{
        [UIView animateWithDuration:kAlertMessageAnimationDuration
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                         animations:^{ _alertView.alpha = 0.95; }
                         completion:nil];
        if (automatic) {
            [self performSelector:@selector(dismissAlertViewWithBlock:) withObject:nil afterDelay:5];
        }
        
    }];
}
//------------------------------------------------------------------------------------------
/**   */
-(void)dismissAlertViewWithBlock: (completionBlock)completion
{
    [UIView animateWithDuration:kAlertMessageAnimationDuration + 0.1
                          delay:0
         usingSpringWithDamping:0.8
          initialSpringVelocity:0.f
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^{ _alertView.alpha = 0; }
                     completion:^(BOOL finished) {
                         if (completion) {
                             completion();
                         }
                     }];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}
/*************************************************************************************************************************/






#pragma mark -
#pragma mark Actions
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_Cancel_Pressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didPopUpCancel)]) {
        [_delegate didPopUpCancel];
    }
//    _alertDismissButton.enabled = YES;
//    _alertImageView.image = [UIImage imageNamed:@"NotificationBackgroundErrorIcon.png"];
//    _alertView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"NotificationBackgroundError.png"]];
}


//------------------------------------------------------------------------------------------
/**   */
-(void)didSubmitResult: (NSNotification *)notif
{
    for (UIView *view in _input_Views) {
        view.alpha = 0;
    }
    _label_CheckboxTitle.alpha = 0;
    _imageView_Checkbox.alpha = 0;
    _btn_CheckBox.alpha = 0;
    
    _label_SaleCheckboxTitle.alpha = 0;
    _imageView_SaleCheckbox.alpha = 0;
    _btn_SaleCheckBox.alpha = 0;
    
    
    [self dismissCountryTable];
    _btn_Cancel.alpha = _btn_Submit.alpha = _label_SendResult.alpha = 0;
    _btn_Restart.alpha = _btn_Dismiss.alpha = 1;
    
    _alertView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"NotificationBackgroundSuccess.png"]];
    _alertImageView.image = [UIImage imageNamed:@"NotificationBackgroundSuccessIcon.png"];
    _alertDismissButton.enabled = NO;
    _alertView.frame = CGRectMake(CGRectGetMinX(_view_Form.frame), CGRectGetMinY(_view_Form.frame), CGRectGetWidth(_alertView.frame), CGRectGetHeight(_alertView.frame));

    // Create the path (with only the top-left corner rounded)
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_alertView.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight                                                       cornerRadii:CGSizeMake(10.0, 10.0)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = _alertView.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shape layer as the mask for the image view's layer
    _alertView.layer.mask = maskLayer;
    
    if ([notif.name isEqualToString:kNotificationSuccess]) {
        [self showAlertViewWithMessage:@"Thank you! Check your inbox for your detailed report." autoDismiss: NO];
    }
    else
    {
        [self showAlertViewWithMessage:@"Thank you! You will receive your detailed report soon." autoDismiss: NO];
    }
    
    self.view.userInteractionEnabled = YES;

  }

#define kImageUncheck @"img_UnCheckedBox.png"
#define kImageCheck @"img_CheckedBox.png"

//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_Submit_Pressed:(id)sender
{
    if ([self isFormValid]) {
        PersonalInfoField *personalInfoField = [[PersonalInfoField alloc] init];
        personalInfoField.firstName = [self.textFieldFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        personalInfoField.lastName = [self.textFieldLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        personalInfoField.email = self.textFieldEmail.text;
        personalInfoField.jobTitle = [self.textFieldJobTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        personalInfoField.company = [self.textFieldCompany.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        personalInfoField.businessPhone = self.textFieldBusinessPhone.text ;
        personalInfoField.country = [self.textFieldCountry.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        personalInfoField.zipCode = self.textFieldZipCode.text;
        personalInfoField.promoCode = [self.textFieldPromoCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                personalInfoField.saleRepCheckbox =  [self.textFieldSalesRep.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        personalInfoField.valueCheckbox = [NSString stringWithFormat:@"%li",(long)_btn_CheckBox.tag];

        
        if (!_saveResultToQueue) {
            _saveResultToQueue = [SaveResultToQueue new];
        }
        
        if (![Common checkNetwork]) {
            [_saveResultToQueue saveResultToQueue:personalInfoField andResultData:_result andISNeedToSave:YES];
        }
        else
        {
            [_saveResultToQueue saveResultToQueue:personalInfoField andResultData:_result andISNeedToSave:NO];
        }
        
    
        
        self.view.userInteractionEnabled = NO;
        for (UITextField *input_TextField in _inputTextFields) {
            input_TextField.text = @"";
        }

            _imageView_Checkbox.image = [UIImage imageNamed:kImageUncheck];
            _btn_CheckBox.tag =0;
        
        _imageView_SaleCheckbox.image = [UIImage imageNamed:kImageUncheck];
        _btn_SaleCheckBox.tag =0;
        
        [self dismissAlertViewWithBlock:^{
            
        }];
    }
}


//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_Restart_Pressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didPopUpRestart)]) {
        [_delegate didPopUpRestart];
    }
}

//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_Checkbox_Pressed:(id)sender
{
    if (_btn_CheckBox.tag == 0) {
        _imageView_Checkbox.image = [UIImage imageNamed:kImageCheck];
        _btn_CheckBox.tag =1;
    }
    else
    {
        _imageView_Checkbox.image = [UIImage imageNamed:kImageUncheck];
        _btn_CheckBox.tag =0;
    }
}

//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_SaleCheckbox_Pressed:(id)sender
{
    if (_btn_SaleCheckBox.tag == 0) {
        _imageView_SaleCheckbox.image = [UIImage imageNamed:kImageCheck];
        _btn_SaleCheckBox.tag =1;
    }
    else
    {
        _imageView_SaleCheckbox.image = [UIImage imageNamed:kImageUncheck];
        _btn_SaleCheckBox.tag =0;
    }
}

/*************************************************************************************************************************/




#pragma mark -
#pragma mark Validate Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(BOOL)isFormValid
{
    [self.view endEditing:YES];
    
    //FirstName
    BOOL isFirstNameValid = [self validateFirstName];
    if (!isFirstNameValid) {
        return isFirstNameValid;
    }
    
    //LastName
    BOOL isLastNameValid = [self validateLastName];
    if (!isLastNameValid) {
        return isLastNameValid;
    }
    
    //Email
    BOOL isEmailVaild = [self validateEmail];
    if (!isEmailVaild) {
        return isEmailVaild;
    }
    
    //Job Title
    BOOL isJobTitleValid = [self validateJobTitle];
    if (!isJobTitleValid) {
        return isJobTitleValid;
    }
    
    //Company
    BOOL isCompanyValid = [self validateCompany];
    if (!isCompanyValid) {
        return isCompanyValid;
    }
    
    //BusinessPhone
    BOOL isBusinessPhoneValid = [self validateBusinessPhone];
    if (!isBusinessPhoneValid) {
        return isBusinessPhoneValid;
    }
    
    //Country
    BOOL isCountryValid = [self validateCountry];
    if (!isCountryValid) {
        return isCountryValid;
    }
    
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateFirstName
{
    NSString *firstName = [self.textFieldFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (firstName.length < 2 ||  firstName.length > 100) {
        [self showAlertViewWithMessage:@"Your first name must be at least 2 characters long and 100 characters maximum" autoDismiss: YES];
        [self.textFieldFirstName becomeFirstResponder];
        return NO;
    }
    
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateLastName
{
    NSString *lastName = [self.textFieldLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (lastName.length < 2 || lastName.length > 100) {
        [self showAlertViewWithMessage:@"Your last name must be at least 2 characters long and 100 characters maximum" autoDismiss: YES];
        [self.textFieldLastName becomeFirstResponder];
        return NO;
    }
//    
//    NSString *lastName_firstLetter = [lastName substringToIndex:1];
//    if ([lastName_firstLetter isEqualToString:@" "]) {
//        [self showAlertViewWithMessage:@"Last name must not start by whitespace"];
//        [self.textFieldLastName becomeFirstResponder];
//        return NO;
//    }

    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateEmail
{
    NSString *email = [self.textFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (email.length == 0) {
        [self showAlertViewWithMessage:@"Please enter your email" autoDismiss: YES];
        [self.textFieldEmail becomeFirstResponder];
        return NO;
    }
    
    NSRange emailRange = [email rangeOfString:@" "];
    if (emailRange.location != NSNotFound) {
        [self showAlertViewWithMessage:@"Email cannot contain white space" autoDismiss: YES];
        [self.textFieldEmail becomeFirstResponder];
        return NO;
    }
    
    if (![Common validateEmail:email]) {
        [self showAlertViewWithMessage:@"Email is invalid" autoDismiss: YES];
        [self.textFieldEmail becomeFirstResponder];
        return NO;
    }
    
    if ([Common isContainEnglishNotCharacter:email]) {
        [self showAlertViewWithMessage:@"Email cannot contain non-english character" autoDismiss: YES];
        [self.textFieldEmail becomeFirstResponder];
        return NO;
    }
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateJobTitle
{
    NSString *jobTitle = [self.textFieldJobTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (jobTitle.length < 3 || jobTitle.length > 100) {
        [self showAlertViewWithMessage:@"Your job title must be at least 3 characters long and 100 characters maximum" autoDismiss: YES];
        [self.textFieldJobTitle becomeFirstResponder];
        return NO;
    }
    
//    NSString *jobTitle_firstLetter = [jobTitle substringToIndex:1];
//    if ([jobTitle_firstLetter isEqualToString:@" "]) {
//        [self showAlertViewWithMessage:@"Job title must not start by whitespace"];
//        [self.textFieldJobTitle becomeFirstResponder];
//        return NO;
//    }
    
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateCompany
{
    NSString *company = [self.textFieldCompany.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (company.length < 2 || company.length > 100) {
        [self showAlertViewWithMessage:@"Your company must be at least 2 characters long and 100 characters maximum" autoDismiss: YES];
        [self.textFieldCompany becomeFirstResponder];
        return NO;
    }
    
//    NSString *company_firstLetter = [company substringToIndex:1];
//    if ([company_firstLetter isEqualToString:@" "]) {
//        [self showAlertViewWithMessage:@"Company must not start by whitespace"];
//        [self.textFieldCompany becomeFirstResponder];
//        return NO;
//    }
    
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateBusinessPhone
{
    NSString *businessPhone = [self.textFieldBusinessPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (businessPhone.length == 0) {
        [self showAlertViewWithMessage:@"Please enter your business phone" autoDismiss: YES];
        [self.textFieldBusinessPhone becomeFirstResponder];
        return NO;
    }
    
//    NSString *businessPhone_firstLetter = [businessPhone substringToIndex:1];
//    if ([businessPhone_firstLetter isEqualToString:@" "]) {
//        [self showAlertViewWithMessage:@"Business phone must not contain whitespace"];
//        [self.textFieldLastName becomeFirstResponder];
//        return NO;
//    }
    
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateCountry
{
        NSString *country = [self.textFieldCountry.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (country.length == 0) {
        [self showAlertViewWithMessage:@"Please select your country" autoDismiss: YES];
//        [self.textFieldCountry becomeFirstResponder];
        return NO;
    }
    
//    NSString *country_firstLetter = [country substringToIndex:1];
//    if ([country_firstLetter isEqualToString:@" "]) {
//        [self showAlertViewWithMessage:@"Country must not start by whitespace"];
//        [self.textFieldCountry becomeFirstResponder];
//        return NO;
//    }
    
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateSalesRep
{
    return YES;
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark UITextField Collection
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)setUpInputFields
{
    _listInfo = @[@"First name*", @"Last name*", @"Email*", @"Job title*", @"Company*", @"Business phone*", @"Country*", @"Zip code", @"Promo code", @"CenturyLink Sales Rep"];
    
    //Labels
    [self configTitleLabels];
    
    //Backgrounds
    [self configBackgroundTextfield];

    //TextFields
    [self configInputTextField];
 }


//------------------------------------------------------------------------------------------
/**   */
-(void)configTitleLabels
{
    [_titleLabels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UILabel *label_Title = obj;
        NSString *title = [_listInfo objectAtIndex:idx];
        label_Title.text = title;
        label_Title.font = kFont_UniversLTStd(16);
        label_Title.textColor = [Common resultContentColor];
        
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: label_Title.attributedText];
        NSRange range = [title rangeOfString:@"*"];
        [text addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        [label_Title setAttributedText: text];
    }];

}

//------------------------------------------------------------------------------------------
/**   */
-(void)configBackgroundTextfield
{
    [_backgroundTextFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIImageView *imageView_InputField = obj;
        imageView_InputField.layer.borderWidth = 1;
        imageView_InputField.layer.borderColor = [UIColor colorWithRed:128 /255.0f green:128 /255.0f blue:128 /255.0f alpha:1].CGColor;
    }];
}

//------------------------------------------------------------------------------------------
/**   */
-(void)configInputTextField
{
    [_inputTextFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UITextField *textField_InputField = obj;
        textField_InputField.delegate = self;
        textField_InputField.font = kFont_UniversLTStd(16);
        switch (idx) {
            case kCellEmail:
                textField_InputField.keyboardType = UIKeyboardTypeEmailAddress;
                break;
            case kCellZipCode:
                textField_InputField.keyboardType = UIKeyboardTypeNumberPad;
                break;
            case kCellBusinessPhone:
                textField_InputField.keyboardType = UIKeyboardTypePhonePad;
                break;
                
            default:
                textField_InputField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                break;
        }
    }];

}


//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldFirstName
{
    return _inputTextFields[kCellFirstName];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldLastName
{
    return _inputTextFields[kCellLastName];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldEmail
{
    return _inputTextFields[kCellEmail];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldJobTitle
{
    return _inputTextFields[kCellJobTitle];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldCompany
{
    return _inputTextFields[kCellCompany];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldBusinessPhone
{
    return _inputTextFields[kCellBusinessPhone];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldCountry
{
    return _inputTextFields[kCellCountry];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldZipCode
{
    return _inputTextFields[kCellZipCode];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldPromoCode
{
    return _inputTextFields[kCellPromoCode];
}

//------------------------------------------------------------------------------------------
/**   */
-(UITextField *)textFieldSalesRep
{
    return _inputTextFields[kCellSalesRep];
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark UITextField Delegate
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [textField setInputAccessoryView:_inputAccView];
    if(_view_ContainTableCountry.alpha == 1)
    {
        [self btn_SelectCountry_Pressed:nil];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.textFieldBusinessPhone || textField == self.textFieldZipCode)
    {
        if (![Common validateNumber:string]) {
            return  NO;
        }
    }

    
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    if (_constraint_ContentSize_ScrollView.constant != 1200){
        _constraint_ContentSize_ScrollView.constant = 1200;
        [_scrollView_Form setContentOffset:CGPointMake(0, 210) animated:YES];
    }

}


//------------------------------------------------------------------------------------------
/**   */
- (void)keyboardDidHide: (NSNotification *) notif{

    // Do something here
    if (_constraint_ContentSize_ScrollView.constant != kDEFAULT_CONTENTSIZE) {
             _constraint_ContentSize_ScrollView.constant = kDEFAULT_CONTENTSIZE;
    }
}

//------------------------------------------------------------------------------------------
/**   */
- (void)keyboardWillHide: (NSNotification *) notif{
    [self dismissAlertViewWithBlock:nil];

}

//------------------------------------------------------------------------------------------
/**   */
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.textFieldFirstName) {
        [self.textFieldLastName becomeFirstResponder];
        return NO;
    }
    if (textField == self.textFieldLastName) {
        [self.textFieldEmail becomeFirstResponder];
        return NO;
    }
    if (textField == self.textFieldEmail) {
        [self.textFieldJobTitle becomeFirstResponder];
        return NO;
    }
    if (textField == self.textFieldJobTitle) {
        [self.textFieldCompany becomeFirstResponder];
        return NO;
    }
    if (textField == self.textFieldCompany) {
        [self.textFieldBusinessPhone becomeFirstResponder];
        return NO;
    }
    if (textField == self.textFieldBusinessPhone) {
        [self showCountryTable];
//        [self.textFieldBusinessPhone resignFirstResponder];
        [self.view endEditing:YES];
        return YES;
    }
    
    if (textField == self.textFieldZipCode) {
        [self.textFieldPromoCode becomeFirstResponder];
        return NO;
    }
    
    if (textField == self.textFieldPromoCode) {
        [self.textFieldSalesRep becomeFirstResponder];
        return NO;
    }
    if (textField == self.textFieldSalesRep) {
        [self btn_Submit_Pressed:nil];
    }

 
    return YES;
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark InputAccessoryView
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    _inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, CGRectGetWidth(self.view.frame), 40.0)];
    
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [_inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [_inputAccView setAlpha: 0.8];
    
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    
    // Let’s create our buttons now. First the previous button.
    _btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [_btnPrev setFrame: CGRectMake(10.0, 0.0, 80.0, 40.0)];
    // Title.
    [_btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    //    [_btnPrev setBackgroundColor: [UIColor blueColor]];
    _btnPrev.adjustsImageWhenHighlighted = YES;
    
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [_btnPrev addTarget: self action: @selector(btn_NaviBack_Pressed:) forControlEvents: UIControlEventTouchUpInside];
    
    // Do the same for the two buttons left.
    _btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnNext setFrame:CGRectMake(95.0f, 0.0f, 80.0f, 40.0f)];
    [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
    //    [_btnNext setBackgroundColor:[UIColor blueColor]];
    [_btnNext addTarget:self action:@selector(btn_NaviNext_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    _btnNext.adjustsImageWhenHighlighted = YES;
    // Now that our buttons are ready we just have to add them to our view.
    [_inputAccView addSubview:_btnPrev];
    [_inputAccView addSubview:_btnNext];
}


//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_NaviNext_Pressed:(id)sender
{
    if ([self.textFieldFirstName isFirstResponder]) {
        [self.textFieldLastName becomeFirstResponder];
        return ;
    }
    if ([self.textFieldLastName isFirstResponder]) {
        [self.textFieldEmail becomeFirstResponder];
        return ;
    }
    if ([self.textFieldEmail isFirstResponder]) {
        [self.textFieldJobTitle becomeFirstResponder];
        return ;
    }
    if ([self.textFieldJobTitle isFirstResponder]) {
        [self.textFieldCompany becomeFirstResponder];
        return ;
    }
    if ([self.textFieldCompany isFirstResponder]) {
        [self.textFieldBusinessPhone becomeFirstResponder];
        return ;
    }
    if ([self.textFieldBusinessPhone isFirstResponder]) {
        [self.view endEditing:YES];
//        [self.textFieldBusinessPhone resignFirstResponder];
         [self showCountryTable];
        return ;
    }
    if ([self.textFieldZipCode isFirstResponder]) {
        [self.textFieldPromoCode becomeFirstResponder];
        return;
    }
    if ([self.textFieldPromoCode isFirstResponder]) {
        [self.textFieldSalesRep becomeFirstResponder];
        return;
    }
}

//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_NaviBack_Pressed:(id)sender
{
    if ([self.textFieldSalesRep isFirstResponder]) {
        [self.textFieldPromoCode becomeFirstResponder];
        return;
    }
    if ([self.textFieldPromoCode isFirstResponder]) {
        [self.textFieldZipCode becomeFirstResponder];
        return;
    }
    if ([self.textFieldZipCode isFirstResponder]) {
//        [self.textFieldZipCode resignFirstResponder];
        [self.view endEditing:YES];
         [self showCountryTable];
        return ;
    }
    if ([self.textFieldBusinessPhone isFirstResponder]) {
        [self.textFieldCompany becomeFirstResponder];
        return ;
    }
    if ([self.textFieldCompany isFirstResponder]) {
        [self.textFieldJobTitle becomeFirstResponder];
        return ;
    }
    if ([self.textFieldJobTitle isFirstResponder]) {
        [self.textFieldEmail becomeFirstResponder];
        return ;
    }
    if ([self.textFieldEmail isFirstResponder]) {
        [self.textFieldLastName becomeFirstResponder];
        return ;
    }
    if ([self.textFieldLastName isFirstResponder]) {
        [self.textFieldFirstName becomeFirstResponder];
        return ;
    }
}
/*************************************************************************************************************************/




#pragma mark -
#pragma mark UITableView Delegate
/*************************************************************************************************************************/
#define kNUMBER_ROWS 4
//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listCountry.count;
}


//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

#define kCountryNameKey @"name"
//------------------------------------------------------------------------------------------
//* */
-(void)configureCell: (UITableViewCell *)cell forRowAtIndexPath: (NSIndexPath *)indexPath
{
    NSString *countryName = [_listCountry objectAtIndex:indexPath.row][kCountryNameKey];
    cell.textLabel.text = countryName;
    cell.textLabel.font = kFont_UniversLTStd(15);
    cell.textLabel.textColor = [UIColor blackColor];
    
}



//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}


//------------------------------------------------------------------------------------------
/**   */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *countryName = [_listCountry objectAtIndex:indexPath.row][kCountryNameKey];
    self.textFieldCountry.text = countryName;
    UITableViewCell *cell = [_tableView_Country cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    [self dismissCountryTable];
}

//------------------------------------------------------------------------------------------
/**   */
-(void)showCountryTable
{
        [UIView animateWithDuration:kAlertMessageAnimationDuration
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                         animations:^{ _view_ContainTableCountry.alpha = 1; }
                         completion:nil];
}

//------------------------------------------------------------------------------------------
/**   */
-(void)dismissCountryTable
{
    [UIView animateWithDuration:kAlertMessageAnimationDuration + 0.1
                          delay:0
         usingSpringWithDamping:0.8
          initialSpringVelocity:0.f
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                     animations:^{ _view_ContainTableCountry.alpha = 0; }
                     completion:nil];
}


//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_SelectCountry_Pressed:(id)sender
{

    if (_view_ContainTableCountry.alpha == 0) {
            [self.view endEditing:YES];
        [self showCountryTable];
    }
    else
    {
        [self dismissCountryTable];
    }
}

/*************************************************************************************************************************/




//#pragma mark -
//#pragma mark Cell
///*************************************************************************************************************************/
////------------------------------------------------------------------------------------------
///**   */
//-(PopUpTableViewCell *)cellFirstName
//{
//    return (PopUpTableViewCell *)[_tableView_ReportInfo cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kCellFirstName inSection:0]];
//}
//
////------------------------------------------------------------------------------------------
///**   */
//-(PopUpTableViewCell *)cellLastName
//{
//    return (PopUpTableViewCell *)[_tableView_ReportInfo cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kCellLastName inSection:0]];
//}
//
////------------------------------------------------------------------------------------------
///**   */
//-(PopUpTableViewCell *)cellEmail
//{
//    return (PopUpTableViewCell *)[_tableView_ReportInfo cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kCellEmail inSection:0]];
//}
//
////------------------------------------------------------------------------------------------
///**   */
//-(PopUpTableViewCell *)cellCompany
//{
//    return (PopUpTableViewCell *)[_tableView_ReportInfo cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kCellCompany inSection:0]];
//}
///*************************************************************************************************************************/
//
//
//
//#pragma mark -
//#pragma mark UITableView Delegate
///*************************************************************************************************************************/
//#define kNUMBER_ROWS 4
////------------------------------------------------------------------------------------------
///**   */
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return _listInfo.count;
//}
//
//
//
//
////------------------------------------------------------------------------------------------
///**   */
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
////    static NSString *CellIdentifier = @"PopUpTableViewCell";
////    PopUpTableViewCell *cell = (PopUpTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
////    if (cell == nil)
////    {
////        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PopUpTableViewCell" owner:self options:nil];
////        cell = [nib objectAtIndex:0];
////    }
////    cell.selectionStyle = UITableViewCellSelectionStyleNone;
////    [self configureCell:cell forRowAtIndexPath:indexPath];
//    
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    
//    return cell;
//}
//
//
////------------------------------------------------------------------------------------------
///**   */
////-(void)configureCell: (PopUpTableViewCell *)cell forRowAtIndexPath: (NSIndexPath *)indexPath
////{
////    NSString *title = [_listInfo objectAtIndex:indexPath.row];
////    cell.label_Title.text = title;
////    cell.label_Title.font = kFont_UniversLTStd(16);
////    cell.label_Title.textColor = [Common resultContentColor];
////    
////    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: cell.label_Title.attributedText];
////    NSRange range = [title rangeOfString:@"*"];
////    [text addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
////    [cell.label_Title setAttributedText: text];
////    
////    cell.textField_Info.font = kFont_UniversLTStd(16);
////    cell.textField_Info.delegate = self;
////    
////    cell.imageView_BackgroundTextField.layer.borderWidth = 1;
////    cell.imageView_BackgroundTextField.layer.borderColor = [UIColor colorWithRed:128 /255.0f green:128 /255.0f blue:128 /255.0f alpha:1].CGColor;
////    
////    switch (indexPath.row) {
////        case kCellEmail:
////            cell.textField_Info.keyboardType = UIKeyboardTypeEmailAddress;
////            break;
////            
////        default:
////            break;
////    }
////}
//
//
//
////------------------------------------------------------------------------------------------
///**   */
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 44;
//}
//
//
////------------------------------------------------------------------------------------------
///**   */
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}
//
///*************************************************************************************************************************/

@end
