//
//  PopUpTableViewCell.h
//  CenturyLink
//
//  Created by Gau Uni on 11/12/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopUpTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView  *imageView_BackgroundTextField;
@property (nonatomic, weak) IBOutlet UILabel  *label_Title;
@property (nonatomic, weak) IBOutlet UITextField  *textField_Info;

@end
