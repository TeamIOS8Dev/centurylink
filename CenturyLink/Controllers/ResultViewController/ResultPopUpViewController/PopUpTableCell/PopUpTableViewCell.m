//
//  PopUpTableViewCell.m
//  CenturyLink
//
//  Created by Gau Uni on 11/12/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "PopUpTableViewCell.h"

@implementation PopUpTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
