//
//  ResultPopUpViewController.h
//  CenturyLink
//
//  Created by Gau Uni on 11/10/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultField.h"

@protocol ResultPopUpViewControllerDelegate <NSObject>

-(void)didPopUpCancel;
-(void)didPopUpSumbit;
-(void)didPopUpRestart;

@end

@interface ResultPopUpViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *label_CheckboxTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imageView_Checkbox;
@property (nonatomic, weak) IBOutlet UIButton *btn_CheckBox;

@property (nonatomic, weak) IBOutlet UILabel *label_SaleCheckboxTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imageView_SaleCheckbox;
@property (nonatomic, weak) IBOutlet UIButton *btn_SaleCheckBox;

@property (nonatomic, weak)IBOutlet UITableView *tableView_Country;
@property (nonatomic, weak)IBOutlet UIView *view_ContainTableCountry;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView_Form;
@property (nonatomic, weak) IBOutlet UIView *contentView_Form;
@property (nonatomic, weak) IBOutlet UIView *view_Form;
@property (nonatomic, weak) IBOutlet UIImageView *imageView_Background;
@property (nonatomic, weak) IBOutlet UILabel *label_SendResult;
@property (nonatomic, weak) IBOutlet UILabel *label_Title;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraint_ContentSize_ScrollView;

@property (nonatomic, weak) IBOutlet UIButton *btn_Cancel;
@property (nonatomic, weak) IBOutlet UIButton *btn_Submit;

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *titleLabels;
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *backgroundTextFields;
@property (nonatomic, strong) IBOutletCollection(UITextField) NSArray *inputTextFields;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *input_Views;

@property (nonatomic, weak) IBOutlet UIButton *btn_Dismiss;
@property (nonatomic, weak) IBOutlet UIButton *btn_Restart;

//Text field
//------------------------------------------------------------------------------------------
/** For testing  */
@property (nonatomic, strong)UITextField *textFieldFirstName;
@property (nonatomic, strong)UITextField *textFieldLastName;
@property (nonatomic, strong)UITextField *textFieldEmail;
@property (nonatomic, strong)UITextField *textFieldJobTitle;
@property (nonatomic, strong)UITextField *textFieldCompany;
@property (nonatomic, strong)UITextField *textFieldBusinessPhone;
@property (nonatomic, strong)UITextField *textFieldCountry;
@property (nonatomic, strong)UITextField *textFieldZipCode;
@property (nonatomic, strong)UITextField *textFieldPromoCode;
@property (nonatomic, strong)UITextField *textFieldSalesRep;

@property (nonatomic, strong) ResultField *result;
@property (nonatomic, weak) id <ResultPopUpViewControllerDelegate> delegate;


#pragma mark -
#pragma mark Validate Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(BOOL)isFormValid;

//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateFirstName;


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateLastName;


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateEmail;


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateJobTitle;

//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateCompany;


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateBusinessPhone;

//------------------------------------------------------------------------------------------
/**   */
-(BOOL)validateCountry;

/*************************************************************************************************************************/

@end
