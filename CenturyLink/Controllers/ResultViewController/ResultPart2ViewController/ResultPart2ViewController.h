//
//  ResultPart2ViewController.h
//  CenturyLink
//
//  Created by Gau Uni on 11/10/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultField.h"

@protocol  ResultPart2ViewControllerDelegate<NSObject>

-(void)didResult2GoBack;
-(void)didResult2Restart;
-(void)didResult2SendResult: (ResultField *)result;

@end

@interface ResultPart2ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextView *textView_ScenarioContent;
@property (nonatomic, weak) IBOutlet UIWebView *webView_ScenarioContent;
@property (nonatomic, strong) NSString *hybridText;
@property (nonatomic, weak) IBOutlet UILabel *label_Description;

@property (nonatomic, weak) IBOutlet UIButton *btn_Back;
@property (nonatomic, weak) IBOutlet UIButton *btn_Restart;
@property (nonatomic, weak) IBOutlet UIButton *btn_Submit;
@property (nonatomic, strong) ResultField *result;
@property (nonatomic, weak) id <ResultPart2ViewControllerDelegate> delegate;

@end
