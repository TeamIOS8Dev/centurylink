//
//  ResultPart2ViewController.m
//  CenturyLink
//
//  Created by Gau Uni on 11/10/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "ResultPart2ViewController.h"
#import "Common.h"
#import "MasterViewController.h"
#import "ResultPopUpViewController.h"

@interface ResultPart2ViewController ()
{
    ResultPopUpViewController *resultPopup;
}
@end

@implementation ResultPart2ViewController

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];

    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//------------------------------------------------------------------------------------------
/**   */
-(void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:YES];
        [self setDataForViewDidLoad];
}

//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
     _btn_Back.exclusiveTouch = _btn_Restart.exclusiveTouch = _btn_Submit.exclusiveTouch = YES;
    
    _textView_ScenarioContent.font =  kFont_UniversLTStd(28/2);
    _textView_ScenarioContent.textColor = [Common resultContentColor];
    _textView_ScenarioContent.editable = NO;
    _textView_ScenarioContent.selectable = NO;
    _textView_ScenarioContent.alpha = 0;
    
    // Footer
    _label_Description.font = kFont_UniversLTStd_Italic(12);
    _label_Description.alpha = 0;
    [Common setLineHeight:kLineHeightOf_Lb_Footer for_UILabel:_label_Description];
    
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    NSString *pathToContentHTMLFile = [[NSBundle mainBundle] pathForResource:@"scenarioContentTemplate" ofType:@"html"];
    NSString *htmlContentString = [NSString stringWithContentsOfFile:
                                   pathToContentHTMLFile encoding:NSUTF8StringEncoding error:nil];
    if (_result.HybridITScenarioCombinationsValue.length != 0) {
        htmlContentString = [htmlContentString stringByReplacingOccurrencesOfString:@"ResultContent" withString:_result.HybridITScenarioCombinationsValue];
        _btn_Submit.enabled = YES;
//        _webView_ScenarioContent.scrollView.showsVerticalScrollIndicator = NO;
    }
    else
    {
            htmlContentString = [htmlContentString stringByReplacingOccurrencesOfString:@"ResultContent" withString:@"The choices you selected do not result in a hybrid IT solution. Hybrid IT solutions include some combination of internal and third-party-provided IT services. Please restart the Hybrid Navigator and modify your selections appropriately."];
        _btn_Submit.enabled = NO;
    }
    [_webView_ScenarioContent loadHTMLString:htmlContentString baseURL:nil];
    NSString * jsCallBack = @"window.getSelection().removeAllRanges();";
    [_webView_ScenarioContent stringByEvaluatingJavaScriptFromString:jsCallBack];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(copy:) ||
        action == @selector(paste:)||
        action == @selector(cut:))
    {
        return NO;
    }
    return [super canPerformAction:action withSender:sender];
}

//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/


#pragma mark -
#pragma mark Actions
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_GoBack_Pressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didResult2GoBack)]) {
        [_delegate didResult2GoBack];
    }
}

//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_Restart_Pressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didResult2Restart)]) {
        [_delegate didResult2Restart];
    }
}

//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_SendResult_Pressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didResult2SendResult:)]) {
        [_delegate didResult2SendResult:_result];
    }
}

/*************************************************************************************************************************/
@end
