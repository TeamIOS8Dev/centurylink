//
//  SaveResultToQueue.h
//  CenturyLink
//
//  Created by Thien Thanh on 11/14/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"
#import "LoadAllRequest.h"


@interface SaveResultToQueue : NSObject<LoadAllRequestDelegate>

-(BOOL)saveResultToQueue:(PersonalInfoField *)personalInfoField andResultData:(ResultField *)resultField andISNeedToSave:(BOOL)isNeedToSave;

-(void)submitAllQeueDataInDatabase:(NSMutableArray *)queueData;

@end
