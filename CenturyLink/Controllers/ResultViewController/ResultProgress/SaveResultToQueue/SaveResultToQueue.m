//
//  SaveResultToQueue.m
//  CenturyLink
//
//  Created by Thien Thanh on 11/14/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "SaveResultToQueue.h"

#import "SubmitData.h"
#import "Common.h"


@interface SaveResultToQueue ()


@property (nonatomic , strong) QueueResultField *queueResultField;
@property (nonatomic, strong) NSMutableArray *arrayAllQueueDataInDatabase;
@property (nonatomic,strong) LoadAllRequest *loadAllRequest;
@property (nonatomic, strong) NSMutableArray *successArray,*failedArray;
@property (nonatomic) NSInteger numberDataInQueue;
@end


@implementation SaveResultToQueue

@synthesize queueResultField;



/**
 * Save result to queue
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(BOOL)saveResultToQueue:(PersonalInfoField *)personalInfoField andResultData:(ResultField *)resultField andISNeedToSave:(BOOL)isNeedToSave
{
    
    AppDelegate *appDelegate;
    if (!appDelegate) {
        appDelegate = [Common getAppDelegate];
    }
    
    queueResultField = [QueueResultField new];
    queueResultField = appDelegate.queueResultField;
    
    //  save data to queue
    //  Personal Info
    queueResultField.firstName          = personalInfoField.firstName;
    queueResultField.lastName           = personalInfoField.lastName;
    queueResultField.email              = personalInfoField.email;
    queueResultField.company            = personalInfoField.company;
    queueResultField.jobTitle           = personalInfoField.jobTitle;
    queueResultField.businessPhone      = personalInfoField.businessPhone;
    queueResultField.country            = personalInfoField.country;
    queueResultField.zipCode            = personalInfoField.zipCode;
    queueResultField.promoCode          = personalInfoField.promoCode;
    queueResultField.valueCheckbox      = personalInfoField.valueCheckbox;
    queueResultField.salesRepCheckbox   = personalInfoField.saleRepCheckbox;
    
    //  result field
    queueResultField.otherConsiderations               = resultField.otherConsiderations;
    queueResultField.resultBusinessDriverAddressed     = resultField.resultBusinessDriverAddressed;
    queueResultField.isPlan                            = resultField.isPlan;
    queueResultField.HybridITScenarioCombinationsValue = resultField.HybridITScenarioCombinationsValue;
    
    
    //  submit data
    NSDictionary *dicData = [SubmitData createObjectSubmit:queueResultField];
    if (!_loadAllRequest) {
        _loadAllRequest = [LoadAllRequest new];
    }
    _loadAllRequest.delegate = self;
    
    //  neu khong co mang user van duoc phep submit data se duoc luu vo hang doi cho lan admin submit sau
    if (isNeedToSave)
    {
        [_loadAllRequest requestPostDataFromServer:@"Failed" andParameters:dicData andTag:kTagPostData];
    }
    else
    {
       
        [_loadAllRequest requestPostDataFromServer:kSubmitFormData andParameters:dicData andTag:kTagPostData];
    }
    

    return YES;
}







/**
 * Submit bulk data
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)submitAllQeueDataInDatabase:(NSMutableArray *)queueData
{
    //  clear array
    if (!_successArray) {
        _successArray = [NSMutableArray new];
        _failedArray = [NSMutableArray new];
    }
    [_successArray removeAllObjects];
    [_failedArray removeAllObjects];
    

    //  get all queue data
    _arrayAllQueueDataInDatabase = queueData;
    _numberDataInQueue = _arrayAllQueueDataInDatabase.count;
    
    if ([_arrayAllQueueDataInDatabase count] != 0) {
        
        //  submit
        [self submitDataToEloqua];
    }
}




/**
 * Submit data
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (void)submitDataToEloqua
{
    //  submit
    queueResultField = _arrayAllQueueDataInDatabase[0];
    NSDictionary *dicData = [SubmitData createObjectSubmit:queueResultField];
    if (!_loadAllRequest) {
        _loadAllRequest = [LoadAllRequest new];
    }
    
    _loadAllRequest.delegate = self;
    [_loadAllRequest requestPostDataFromServer:kSubmitFormData andParameters:dicData andTag:kTagPostBulkData];
}


/********************************************************************************/
#pragma mark - LoadAllRequest Delegate


//------------------------------------------------------------------------------------------
/**   */
-(void)didSucessfullRequestPostData:(id)responseObject
{
    // post NSNotification
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSuccess object:nil userInfo:nil];
}


//------------------------------------------------------------------------------------------
/**   */
-(void)didFaildRequestPostData:(NSError *)error
{
    

    
    QueueResultModel *queueResultModel = [QueueResultModel new];
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory  inDomains:NSUserDomainMask] lastObject]);
    [queueResultModel createRowInTableQueueResult:queueResultField];
    
    // post NSNotification
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFailed object:nil userInfo:nil];
}




/**
 * Bulk Data
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)didSucessfullRequestPostBulkData:(id)responseObject
{
    //  if submit queueResultField successfull delete in database
    QueueResultModel *queueResultModel = [QueueResultModel new];
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory  inDomains:NSUserDomainMask] lastObject]);
    [queueResultModel deleteRowByRowId:queueResultField.rowid];
    
    //  remove in array queue
    [_successArray addObject:queueResultField];
    [_arrayAllQueueDataInDatabase removeObject:queueResultField];
    
    //  show loading
    [_loadAllRequest.progressBarViewController.lblSuccessNumber setText:[NSString stringWithFormat:@"%lu",(unsigned long)_successArray.count]];
    _loadAllRequest.progressBarViewController.progressBar.progress = (float) (_numberDataInQueue - _arrayAllQueueDataInDatabase.count) / (float)_numberDataInQueue;
    
    //  check if more data in database continute submit
    if ([_arrayAllQueueDataInDatabase count] != 0) {
        [self submitDataToEloqua];
        
    }
    else
    {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:2.0]];
        _loadAllRequest.progressBarViewController.view.hidden = YES;
        [_loadAllRequest.progressBarViewController viewDidLoad];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Message" message:@"Finish" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
    }
}



//------------------------------------------------------------------------------------------
/**   */
-(void)didFaildRequestPostBulkData:(NSError *)error
{
//    NSArray *arrayKey = [[error  userInfo] allKeys];
//    if ([arrayKey containsObject:@"com.alamofire.serialization.response.error.data"]) {
//        NSArray *json = [NSJSONSerialization JSONObjectWithData:[error  userInfo][@"com.alamofire.serialization.response.error.data"] options:0 error:nil];
//        
//        if ([json count] != 0) {
//            <#statements#>
//        }
//        NSLog(@" dic : %@ ",json[0]);
//        
////        if (json != nil) {
////            arrayKey = [json allKeys];
////            if ([arrayKey containsObject:@"type"]) {
////                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Message" message:json[@"type"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
////                
////                [alert show];
////                
////                return;
////            }
////        }
//    }
    
    
    //  remove in array queue
    [_failedArray addObject:queueResultField];
    [_arrayAllQueueDataInDatabase removeObject:queueResultField];
    
    //  show loading
    [_loadAllRequest.progressBarViewController.lblFailedNumber setText:[NSString stringWithFormat:@"%lu",(unsigned long)_failedArray.count]];
    _loadAllRequest.progressBarViewController.progressBar.progress = (float) (_numberDataInQueue - _arrayAllQueueDataInDatabase.count) / (float)_numberDataInQueue;
    
    //  check if more data in database continute submit
    if ([_arrayAllQueueDataInDatabase count] != 0) {
        [self submitDataToEloqua];
    }
    else
    {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:2.0]];
        _loadAllRequest.progressBarViewController.view.hidden = YES;
        [_loadAllRequest.progressBarViewController viewDidLoad];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Message" message:@"Finish" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
    }

}

/********************************************************************************/



@end
