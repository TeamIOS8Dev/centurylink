//
//  SubmitData.h
//  CenturyLink
//
//  Created by Thien Thanh on 11/13/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "QueueResultField.h"

@interface SubmitData : NSObject

+ (NSDictionary *)createObjectSubmit:(QueueResultField *)queueResult;

@end
