//
//  SubmitData.m
//  CenturyLink
//
//  Created by Thien Thanh on 11/13/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "SubmitData.h"
#import <objc/runtime.h>


#import <objc/runtime.h>

#import "FieldValue.h"

#define kFieldValue @"FieldValue"
#define kFormId @"1073"
#define kTypeForm @"FormData"

@interface NSDictionary(dictionaryWithObject)

+(NSDictionary *) dictionaryWithPropertiesOfObject:(id) obj;

@end
@implementation NSDictionary(dictionaryWithObject)

+(NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        
        //  if nil set value = @""
        if ([[obj valueForKey:key] length] == 0) {
            [obj setValue:@"" forKey:key];
        }
        
        if ([key isEqualToString:@"idString"]) {
            [dict setObject:[obj valueForKey:key] forKey:@"id"];
        }
        else
            [dict setObject:[obj valueForKey:key] forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

@end


@implementation SubmitData



+ (NSDictionary *)createObjectSubmit:(QueueResultField *)queueResult
{
    //  First Name
    FieldValue *firstNameField              = [FieldValue new];
    firstNameField.idString                 = @"12652";
    firstNameField.type                     = kFieldValue;
    firstNameField.value                    = queueResult.firstName;
    
    
    //  Last Name
    FieldValue *lastNameField               = [FieldValue new];
    lastNameField.idString                  = @"12653";
    lastNameField.type                      = kFieldValue;
    lastNameField.value                     = queueResult.lastName;
    
    
    //  Email Address
    FieldValue *emailField                  = [FieldValue new];
    emailField.idString                     = @"12651";
    emailField.type                         = kFieldValue;
    emailField.value                        = queueResult.email;
    
    
    //  Business Phone
    FieldValue *businessPhoneField          = [FieldValue new];
    businessPhoneField.idString             = @"12647";
    businessPhoneField.type                 = kFieldValue;
    businessPhoneField.value                = queueResult.businessPhone;

    
    //  Company
    FieldValue *companyField                = [FieldValue new];
    companyField.idString                   = @"12648";
    companyField.type                       = kFieldValue;
    companyField.value                      = queueResult.company;

    
    //  Job Title
    FieldValue *jobTitleField               = [FieldValue new];
    jobTitleField.idString                  = @"12659";
    jobTitleField.type                      = kFieldValue;
    jobTitleField.value                     = queueResult.jobTitle;
    
    
    //  Country
    FieldValue *countryField                = [FieldValue new];
    countryField.idString                   = @"12650";
    countryField.type                       = kFieldValue;
    countryField.value                      = queueResult.country;

    
    //  Zip code
    FieldValue *zipCodeField                = [FieldValue new];
    zipCodeField.idString                   = @"12664";
    zipCodeField.type                       = kFieldValue;
    zipCodeField.value                      = queueResult.zipCode;
    

    //  promo code
    FieldValue *promoCodeField              = [FieldValue new];
    promoCodeField.idString                 = @"12665";
    promoCodeField.type                     = kFieldValue;
    promoCodeField.value                    = queueResult.promoCode;
    
    
    //  Plan status
    FieldValue *planStatusField             = [FieldValue new];
    planStatusField.idString                = @"12666";
    planStatusField.type                    = kFieldValue;
    planStatusField.value                   = queueResult.planStatus;
    
    
    //  Business driver 1
    FieldValue *businessDriver1Field        = [FieldValue new];
    businessDriver1Field.idString           = @"12667";
    businessDriver1Field.type               = kFieldValue;
    businessDriver1Field.value              = queueResult.businessDriver1;
    
    
    //  Business driver 2
    FieldValue *businessDriver2Field        = [FieldValue new];
    businessDriver2Field.idString           = @"12668";
    businessDriver2Field.type               = kFieldValue;
    businessDriver2Field.value              = queueResult.businessDriver2;
    
    
    //  Management
    FieldValue *managementField             = [FieldValue new];
    managementField.idString                = @"12669";
    managementField.type                    = kFieldValue;
    managementField.value                   = queueResult.management;
    
    
    //  Infrastructure Location
    FieldValue *infrastructureLocationField = [FieldValue new];
    infrastructureLocationField.idString    = @"12670";
    infrastructureLocationField.type        = kFieldValue;
    infrastructureLocationField.value       = queueResult.infrastructureLocation;
    
    
    //  Compute models
    FieldValue *computeModelsField          = [FieldValue new];
    computeModelsField.idString             = @"12671";
    computeModelsField.type                 = kFieldValue;
    computeModelsField.value                = queueResult.computeModels;
    
    
    //  Output PDF
    FieldValue *outputPDFField              = [FieldValue new];
    outputPDFField.idString                 = @"12672";
    outputPDFField.type                     = kFieldValue;
    outputPDFField.value                    = queueResult.outputPDF;

    
    
    //  LeadSourceDetail
    FieldValue *leadSourceDetailField       = [FieldValue new];
    leadSourceDetailField.idString          = @"12654";
    leadSourceDetailField.type              = kFieldValue;
    leadSourceDetailField.value             = @"2014 Q4 Cloud Request a Trial Form - Cloud Trial Intel";

    //  LeadSource-MostRecent
    FieldValue *leadSourceMostRecentField   = [FieldValue new];
    leadSourceMostRecentField.idString      = @"12655";
    leadSourceMostRecentField.type          = kFieldValue;
    leadSourceMostRecentField.value         = @"iPad App";

    //  LeadSource-Original
    FieldValue *leadSourceOriginalField     = [FieldValue new];
    leadSourceOriginalField.idString        = @"12656";
    leadSourceOriginalField.type            = kFieldValue;
    leadSourceOriginalField.value           = @"iPad App";
    
    //  Wish to discuss
    FieldValue *singleCheckboxField         = [FieldValue new];
    singleCheckboxField.idString            = @"12827";
    singleCheckboxField.type                = kFieldValue;
    singleCheckboxField.value               = queueResult.valueCheckbox;
    
    //  CenturyLink Sales Rep
    FieldValue *salesRepCheckboxField       = [FieldValue new];
    salesRepCheckboxField.idString          = @"13353";
    salesRepCheckboxField.type              = kFieldValue;
    salesRepCheckboxField.value             = queueResult.salesRepCheckbox;


    // array
    NSArray *arrayValueObject = @[businessPhoneField,companyField,countryField,emailField,firstNameField,lastNameField,leadSourceDetailField,leadSourceMostRecentField,leadSourceOriginalField,jobTitleField,zipCodeField,promoCodeField,planStatusField,businessDriver1Field,businessDriver2Field,managementField,infrastructureLocationField,computeModelsField,outputPDFField,singleCheckboxField,salesRepCheckboxField];

    NSMutableArray *arrayFinal = [NSMutableArray new];
    for (FieldValue *object in arrayValueObject)
    {
        NSDictionary *dict  = [NSDictionary dictionaryWithPropertiesOfObject: object];
        [arrayFinal addObject:dict];
    }
    
    NSDictionary *dicData = @{@"fieldValues" : arrayFinal,@"submittedByContactId" : kFormId,@"type" : kTypeForm};
    
    return dicData;

    
}



@end
