


////HybridITScenarioCombinationsField.h 
//  Project 
// 
// Created by TeamiOS on 2014-11-26 06:49:21 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface HybridITScenarioCombinationsField : NSObject
{
	NSString *rowid;
	NSString *condition;
	NSString *value;
	NSString *key;
}

@property (nonatomic, strong) NSString *rowid;
@property (nonatomic, strong) NSString *condition;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSString *key;
 

@end