


////BusinessDriverAddressedField.h 
//  Project 
// 
// Created by TeamiOS on 2014-11-20 02:27:54 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface BusinessDriverAddressedField : NSObject
{
	NSString *rowid;
	NSString *keyBusinessDriverAddressed;
	NSString *valueBusinessDriverAddressed;
}

@property (nonatomic, strong) NSString *rowid;
@property (nonatomic, strong) NSString *keyBusinessDriverAddressed;
@property (nonatomic, strong) NSString *valueBusinessDriverAddressed;
 

@end