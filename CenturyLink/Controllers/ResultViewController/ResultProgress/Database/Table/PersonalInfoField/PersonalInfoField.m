//
//  PersonalInfoField.m
//  CenturyLink
//
//  Created by Thien Thanh on 11/14/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "PersonalInfoField.h"

@implementation PersonalInfoField

@synthesize firstName;
@synthesize lastName;
@synthesize email;
@synthesize company;
@synthesize jobTitle;
@synthesize businessPhone;
@synthesize country;
@synthesize zipCode;
@synthesize promoCode;
@synthesize valueCheckbox;
@synthesize saleRepCheckbox;
@end
