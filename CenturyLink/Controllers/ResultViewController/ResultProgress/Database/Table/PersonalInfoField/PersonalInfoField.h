//
//  PersonalInfoField.h
//  CenturyLink
//
//  Created by Thien Thanh on 11/14/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonalInfoField : NSObject
{
    NSString *firstName;
    NSString *lastName;
    NSString *email;
    NSString *company;
    NSString *jobTitle;
    NSString *businessPhone;
    NSString *country;
    NSString *zipCode;
    NSString *promoCode;
    NSString *valueCheckbox;
    NSString *saleRepCheckbox;
}


@property (nonatomic, strong) NSString *rowid;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *jobTitle;
@property (nonatomic, strong) NSString *businessPhone;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *zipCode;
@property (nonatomic, strong) NSString *promoCode;
@property (nonatomic, strong) NSString *valueCheckbox;
@property (nonatomic, strong) NSString *saleRepCheckbox;

@end
