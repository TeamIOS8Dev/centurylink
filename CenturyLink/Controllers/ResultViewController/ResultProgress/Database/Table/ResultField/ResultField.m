

////ResultField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-11-10 03:03:19 +0000. 
//
// 

#import "ResultField.h"

@implementation ResultField
@synthesize rowid;
@synthesize choice;
@synthesize resultTitle;
@synthesize otherConsiderations;
@synthesize resultBusinessDriverAddressed;
@synthesize isPlan;

//  more info
@synthesize arrayBusinessDriverAddressed;
@synthesize HybridITScenarioCombinationsValue;
@synthesize arrayAnswerInStep2;

@end