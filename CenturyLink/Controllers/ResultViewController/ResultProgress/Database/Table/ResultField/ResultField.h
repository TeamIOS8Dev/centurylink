


////ResultField.h 
//  Project 
// 
// Created by TeamiOS on 2014-11-10 03:03:19 +0000. 
//
// 

#import <Foundation/Foundation.h>


@interface ResultField : NSObject
{
	NSString *rowid;
	NSString *choice;
	NSString *resultTitle;
	NSString *otherConsiderations;
	NSString *resultBusinessDriverAddressed;
	NSString *isPlan;
    
    //  more info
    NSMutableArray *arrayBusinessDriverAddressed;
    NSMutableArray *arrayAnswerInStep2;
    NSString *HybridITScenarioCombinationsValue;
    
}

@property (nonatomic, strong) NSString *rowid;
@property (nonatomic, strong) NSString *choice;
@property (nonatomic, strong) NSString *resultTitle;
@property (nonatomic, strong) NSString *otherConsiderations;
@property (nonatomic, strong) NSString *resultBusinessDriverAddressed;
@property (nonatomic, strong) NSString *isPlan;

//  more info
@property (nonatomic, strong) NSString *HybridITScenarioCombinationsValue;
@property (nonatomic, strong) NSMutableArray *arrayBusinessDriverAddressed;
@property (nonatomic, strong) NSMutableArray *arrayAnswerInStep2;

@end