


////QueueResultField.h 
//  Project 
// 
// Created by TeamiOS on 2014-11-19 02:48:25 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface QueueResultField : NSObject
{
	NSString *rowid;
	NSString *firstName;
	NSString *lastName;
	NSString *email;
	NSString *company;
	NSString *resultTitle;
	NSString *otherConsiderations;
	NSString *jobTitle;
	NSString *businessPhone;
	NSString *country;
	NSString *zipCode;
	NSString *promoCode;
    
	NSString *resultBusinessDriverAddressed;
	NSString *HybridITScenarioCombinationsValue;
    NSString *isPlan;
    
    NSString *planStatus; // I Have a Plan
	NSString *businessDriver1; // Text
	NSString *businessDriver2; // Text
	NSString *management; // 75% / 25%
	NSString *infrastructureLocation; // 50% / 50%
	NSString *computeModels; // Text1;Text2
    
	NSString *outputPDF;
    NSString *valueCheckbox;
    NSString *salesRepCheckbox;
}

@property (nonatomic, strong) NSString *rowid;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *resultTitle;
@property (nonatomic, strong) NSString *otherConsiderations;
@property (nonatomic, strong) NSString *jobTitle;
@property (nonatomic, strong) NSString *businessPhone;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *zipCode;
@property (nonatomic, strong) NSString *promoCode;
@property (nonatomic, strong) NSString *resultBusinessDriverAddressed;
@property (nonatomic, strong) NSString *isPlan;
@property (nonatomic, strong) NSString *HybridITScenarioCombinationsValue;
@property (nonatomic, strong) NSString *planStatus;
@property (nonatomic, strong) NSString *businessDriver1;
@property (nonatomic, strong) NSString *businessDriver2;
@property (nonatomic, strong) NSString *management;
@property (nonatomic, strong) NSString *infrastructureLocation;
@property (nonatomic, strong) NSString *computeModels;
@property (nonatomic, strong) NSString *outputPDF;
@property (nonatomic, strong) NSString *valueCheckbox;
@property (nonatomic, strong) NSString *salesRepCheckbox;


-(void) showDecription;

@end