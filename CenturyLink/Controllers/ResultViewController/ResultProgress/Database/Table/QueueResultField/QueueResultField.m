

////QueueResultField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-11-19 02:48:26 +0000. 
//
// 

#import "QueueResultField.h"

@implementation QueueResultField
@synthesize rowid;
@synthesize firstName;
@synthesize lastName;
@synthesize email;
@synthesize company;
@synthesize resultTitle;
@synthesize otherConsiderations;
@synthesize jobTitle;
@synthesize businessPhone;
@synthesize country;
@synthesize zipCode;
@synthesize promoCode;
@synthesize resultBusinessDriverAddressed;
@synthesize isPlan;
@synthesize HybridITScenarioCombinationsValue;
@synthesize planStatus;
@synthesize businessDriver1;
@synthesize businessDriver2;
@synthesize management;
@synthesize infrastructureLocation;
@synthesize computeModels;
@synthesize outputPDF;
@synthesize valueCheckbox;
@synthesize salesRepCheckbox;



-(void) showDecription
{
    NSLog(@"\n- QueueResultField:\n +planStatus = %@ \n +businessDriver1 = %@ \n +businessDriver2 = %@ \n +management = %@ \n +infrastructureLocation = %@ \n computeModels = %@ \n salesRepCheckbox = %@", planStatus, businessDriver1, businessDriver2, management, infrastructureLocation, computeModels,salesRepCheckbox);
}


@end