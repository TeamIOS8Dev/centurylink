//
//  Core.h
//  SpexBeta
//
//  Created by DucNguyen on 9/14/12.
//  Copyright (c) 2012 Carbon8. All rights reserved.
//

/** This is a file. Describle some infomations about app. Maybe, fileNameDatabase, variable, method will be using in the app
**/

/** Name Database */
#define kNameDatabase @"ResultDatabase.sqlite"


/** flag has already log in */
#define keyFlagAlreadyLogin @"flagLogin"

typedef enum {
    kNotLoggedIn = 0,
    kLoggedIn = 1
} kAppLogin;


/** define type def for active user */
typedef enum {
    kUserActive = 1,
    kUserDeactive = 0,
} kAccountActive;


/** define typedef for UIAlertView */
typedef  enum {
    kTypeAlertOk = 0,
    kTypeAlertYesNo = 1,
}kTypeAlert;


/** Define title alert */
//#define kTitleAlert @"NewSpex"


/** Email address send feedback */
#define kEmailAddressFeedback @"feedback@spex.it"


/** Subject email feedback */
#define kSubjectEmailFeedback @"SPEX Feedback"


/** Website address SPEX */
#define kWebsiteAddressSpex @"http://www.spex.it"


/** define kname Group */
#define kNameGroupInterior @"interior"
#define kNameGroupExterior @"exterior"
#define kNameGroupExteriorExteriorperiphery @"exterior periphery"
#define kNameGroupProjectconsiderations @"wrap up"
#define kNameGroupWrapup @"wrap up"


/** fonts */
#define KFontHelveticaNeueLTStd @"HelveticaNeue"
#define KFontHelveticaNeueLTStdMedium @"HelveticaNeueLTStd-Md"
#define KFontHelveticaNeueLTStdBold @"HelveticaNeueLTStd-Bd"
#define KFontHelveticaNeueLTStdLight @"HelveticaNeue-Light"
#define KFontInspectionType @"HelveticaNeue-Regular"

/** favorites */
#define kMessage_DeleteFavorites @"Are you sure you want to delete this category and remove its items?"
#define kMessageInputFaildRequire @"Please enter infomation."
#define kMessageCategoryNameIsExist @"Category exist. Please try again."


/** type detail */
#define kDetailTypeDD @"DD"
#define kDetailTypeINPUT @"INPUT"
#define kDetailTypeINPUTCALC @"INPUTCALC"

#define kDetailTypeINPUTMEASURE @"INPUTMEASURE"
#define kDetailTypeDDINPUT @"DDINPUT"
#define kDetailTypeDDINPUTCALC @"DDINPUTCALC"

#define kDetailTypeDDINPUTMEASURE @"DDINPUTMEASURE"
#define kDetailTypeFORMULA @"FORMULA"





/** Define item modifier "ORTHER"*/
//typedef struct
//{
//    NSString *itemModifierId;
//    NSString *modifierName;
//    NSString *defaultUnit;
//}ItemModifierOther;


/** type action*/
#define kActionTypeAmount @"Amount"
#define kActionTypeDescription @"Description"


#define kItemListIdMisc @"-10"

#define kDetailCategoryNameMiscActionDetail @"Action Detail"
#define kDetailCategoryNameMiscAddInfo @"Addl. Info"


/*** Define for type media */
#define kVideoType @"1"
#define kAudioType @"2"
#define kPhotoType @"3"


/** Message when push some */
#define kMessageWhenPushSymbolNotValid @"Enter symbol is not valid"


/** Message when delete item list */
#define kMessageWhenDeleteItemList @"“Are you sure you want to delete this item and its components (if any)?"


/** Define Category contain two actions other and no damage*/
#define kCategoryIdContaintActionOtherAndNoDamage @"15"


/** Define id action other*/
#define kActionOtherId @"-1"


/** Define id action no damage */
#define kActionNoDamageId @"-2"


/** Define message when delete a project */
#define kMessage_DeleteProject @"Are you sure you want to delete this project and remove its inspections?"




#define kURLSubmitPdf @"http://public.carbon-8.com/inspex/index.php"

#define kMessage_WaitingReport @"A request was sent to the server. Please wait while the server generates the report. This may take a few minutes."
#define kMessage_WaitingReport4minutes @"Server is generating report. Please wait"
#define kMessage_CanNotDownReport @"Sorry, there's error occurred while generating report. Please try again."
#define kMessage_NoPDFReport @"File not found."
#define kMessage_CanNotCreateReport @"Can not create new report."
#define kFileNameReport @"report.pdf"



/** Define id detail other */
#define kDetailOtherId @"-100"


/** Define id detail unknow*/
#define kDetailUnknowId @"-101"


/** Define Other Misc. Detail id */
#define kOtherMiscDetailId @"-858"
#define kOtherMiscDetailName @"Other Misc. Detail"


/** Name reason for no action */
#define kSelectReasonforNoAction @"Select Reason for No Action"





