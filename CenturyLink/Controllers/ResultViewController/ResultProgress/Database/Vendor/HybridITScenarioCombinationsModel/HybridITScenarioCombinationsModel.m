

////HybridITScenarioCombinationsField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-11-26 06:49:21 +0000. 
//

//--CREATE TABLE "HybridITScenarioCombinations" ("condition" TEXT, "value" TEXT, "key" TEXT, )

#import "HybridITScenarioCombinationsModel.h"

@implementation HybridITScenarioCombinationsModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"HybridITScenarioCombinations";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableHybridITScenarioCombinations:(HybridITScenarioCombinationsField *) data
{
	NSString *condition = data.condition;
	NSString *value = data.value;
	NSString *key = data.key;

	NSString *conditionInit = @"";
	NSString *valueInit = @"";
	NSString *keyInit = @"";


	if (condition)
		conditionInit = condition;

	if (value)
		valueInit = value;

	if (key)
		keyInit = key;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"HybridITScenarioCombinations\" (\"condition\",\"value\",\"key\") VALUES (?1,?2,?3)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--condition
	sqlite3_bind_text(createStmt, 1, [conditionInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--value
	sqlite3_bind_text(createStmt, 2, [valueInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--key
	sqlite3_bind_text(createStmt, 3, [keyInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableHybridITScenarioCombinations:(HybridITScenarioCombinationsField *) data
{

	NSString *condition = data.condition;
	NSString *value = data.value;
	NSString *key = data.key;

	NSString *conditionInit = @"";
	NSString *valueInit = @"";
	NSString *keyInit = @"";


	if (condition)
		conditionInit = condition;

	if (value)
		valueInit = value;

	if (key)
		keyInit = key;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"HybridITScenarioCombinations\" SET \"condition\" = ?, \"value\" = ?, \"key\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--condition
	sqlite3_bind_text(createStmt, 1, [conditionInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--value
	sqlite3_bind_text(createStmt, 2, [valueInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--key
	sqlite3_bind_text(createStmt, 3, [keyInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableHybridITScenarioCombinations
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--condition
			char *condition = (char *) sqlite3_column_text(statement, 1);
			NSString *conditionStr = (condition == NULL) ? @"":[NSString stringWithUTF8String:condition];

			//--value
			char *value = (char *) sqlite3_column_text(statement, 2);
			NSString *valueStr = (value == NULL) ? @"":[NSString stringWithUTF8String:value];

			//--key
			char *key = (char *) sqlite3_column_text(statement, 3);
			NSString *keyStr = (key == NULL) ? @"":[NSString stringWithUTF8String:key];



			//--add Object
			HybridITScenarioCombinationsField *object = [[HybridITScenarioCombinationsField alloc]init];

			object.rowid = rowidStr;
			object.condition = conditionStr;
			object.value = valueStr;
			object.key = keyStr;
			
			[array addObject:object];

			//--free memory
			

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(HybridITScenarioCombinationsField *) getRowByRowsInTableHybridITScenarioCombinationsByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_HybridITScenarioCombinationsField_BySQL:qsql];
}


//--Retrieving Data
-(HybridITScenarioCombinationsField *) getData_HybridITScenarioCombinationsField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	HybridITScenarioCombinationsField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--condition
			char *condition = (char *) sqlite3_column_text(statement, 1);
			NSString *conditionStr = (condition == NULL) ? @"":[NSString stringWithUTF8String:condition];

			//--value
			char *value = (char *) sqlite3_column_text(statement, 2);
			NSString *valueStr = (value == NULL) ? @"":[NSString stringWithUTF8String:value];

			//--key
			char *key = (char *) sqlite3_column_text(statement, 3);
			NSString *keyStr = (key == NULL) ? @"":[NSString stringWithUTF8String:key];



			//--add Object
			object = [[HybridITScenarioCombinationsField alloc] init];

			object.rowid = rowidStr;
			object.condition = conditionStr;
			object.value = valueStr;
			object.key = keyStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end