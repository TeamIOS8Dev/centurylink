

////HybridITScenarioCombinationsModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-11-26 06:49:21 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "HybridITScenarioCombinationsField.h"

@interface HybridITScenarioCombinationsModel : AppModel
{

}


/** Get all rows in Table HybridITScenarioCombinations **/
-(NSMutableArray *) getAllRowsInTableHybridITScenarioCombinations;

/** Get rows by rowid in Table HybridITScenarioCombinations **/
-(HybridITScenarioCombinationsField *) getRowByRowsInTableHybridITScenarioCombinationsByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableHybridITScenarioCombinations:(HybridITScenarioCombinationsField *) data;

/** update row **/
-(BOOL)updateRowInTableHybridITScenarioCombinations:(HybridITScenarioCombinationsField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end