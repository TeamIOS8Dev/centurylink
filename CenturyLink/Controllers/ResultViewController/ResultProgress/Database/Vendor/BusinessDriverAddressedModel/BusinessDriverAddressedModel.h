

////BusinessDriverAddressedModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-11-20 02:27:54 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "BusinessDriverAddressedField.h"

@interface BusinessDriverAddressedModel : AppModel
{

}


/** Get all rows in Table BusinessDriverAddressed **/
-(NSMutableArray *) getAllRowsInTableBusinessDriverAddressed;

/** Get rows by rowid in Table BusinessDriverAddressed **/
-(BusinessDriverAddressedField *) getRowByRowsInTableBusinessDriverAddressedByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableBusinessDriverAddressed:(BusinessDriverAddressedField *) data;

/** update row **/
-(BOOL)updateRowInTableBusinessDriverAddressed:(BusinessDriverAddressedField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end