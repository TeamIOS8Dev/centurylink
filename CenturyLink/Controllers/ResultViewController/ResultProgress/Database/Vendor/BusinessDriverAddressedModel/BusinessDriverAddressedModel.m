

////BusinessDriverAddressedField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-11-20 02:27:54 +0000. 
//

//--CREATE TABLE "BusinessDriverAddressed" ("keyBusinessDriverAddressed" TEXT, "valueBusinessDriverAddressed" TEXT, )

#import "BusinessDriverAddressedModel.h"

@implementation BusinessDriverAddressedModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"BusinessDriverAddressed";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableBusinessDriverAddressed:(BusinessDriverAddressedField *) data
{
	NSString *keyBusinessDriverAddressed = data.keyBusinessDriverAddressed;
	NSString *valueBusinessDriverAddressed = data.valueBusinessDriverAddressed;

	NSString *keyBusinessDriverAddressedInit = @"";
	NSString *valueBusinessDriverAddressedInit = @"";


	if (keyBusinessDriverAddressed)
		keyBusinessDriverAddressedInit = keyBusinessDriverAddressed;

	if (valueBusinessDriverAddressed)
		valueBusinessDriverAddressedInit = valueBusinessDriverAddressed;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"BusinessDriverAddressed\" (\"keyBusinessDriverAddressed\",\"valueBusinessDriverAddressed\") VALUES (?1,?2)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--keyBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 1, [keyBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--valueBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 2, [valueBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableBusinessDriverAddressed:(BusinessDriverAddressedField *) data
{

	NSString *keyBusinessDriverAddressed = data.keyBusinessDriverAddressed;
	NSString *valueBusinessDriverAddressed = data.valueBusinessDriverAddressed;

	NSString *keyBusinessDriverAddressedInit = @"";
	NSString *valueBusinessDriverAddressedInit = @"";


	if (keyBusinessDriverAddressed)
		keyBusinessDriverAddressedInit = keyBusinessDriverAddressed;

	if (valueBusinessDriverAddressed)
		valueBusinessDriverAddressedInit = valueBusinessDriverAddressed;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"BusinessDriverAddressed\" SET \"keyBusinessDriverAddressed\" = ?, \"valueBusinessDriverAddressed\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--keyBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 1, [keyBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--valueBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 2, [valueBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableBusinessDriverAddressed
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--keyBusinessDriverAddressed
			char *keyBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 1);
			NSString *keyBusinessDriverAddressedStr = (keyBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:keyBusinessDriverAddressed];

			//--valueBusinessDriverAddressed
			char *valueBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 2);
			NSString *valueBusinessDriverAddressedStr = (valueBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:valueBusinessDriverAddressed];



			//--add Object
			BusinessDriverAddressedField *object = [[BusinessDriverAddressedField alloc]init];

			object.rowid = rowidStr;
			object.keyBusinessDriverAddressed = keyBusinessDriverAddressedStr;
			object.valueBusinessDriverAddressed = valueBusinessDriverAddressedStr;
			
			[array addObject:object];

			//--free memory
			

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(BusinessDriverAddressedField *) getRowByRowsInTableBusinessDriverAddressedByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_BusinessDriverAddressedField_BySQL:qsql];
}


//--Retrieving Data
-(BusinessDriverAddressedField *) getData_BusinessDriverAddressedField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	BusinessDriverAddressedField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--keyBusinessDriverAddressed
			char *keyBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 1);
			NSString *keyBusinessDriverAddressedStr = (keyBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:keyBusinessDriverAddressed];

			//--valueBusinessDriverAddressed
			char *valueBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 2);
			NSString *valueBusinessDriverAddressedStr = (valueBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:valueBusinessDriverAddressed];



			//--add Object
			object = [[BusinessDriverAddressedField alloc] init];

			object.rowid = rowidStr;
			object.keyBusinessDriverAddressed = keyBusinessDriverAddressedStr;
			object.valueBusinessDriverAddressed = valueBusinessDriverAddressedStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end