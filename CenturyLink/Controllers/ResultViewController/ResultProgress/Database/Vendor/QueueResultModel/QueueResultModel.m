

////QueueResultField.m 
//  Project 
// 
//  Created by TeamiOS on 2015-01-20 07:01:22 +0000. 
//

//--CREATE TABLE "QueueResult" ("firstName" TEXT, "lastName" TEXT, "email" TEXT, "company" TEXT, "resultTitle" TEXT, "otherConsiderations" TEXT, "jobTitle" TEXT, "businessPhone" TEXT, "country" TEXT, "zipCode" TEXT, "promoCode" TEXT, "resultBusinessDriverAddressed" TEXT, "isPlan" TEXT, "HybridITScenarioCombinationsValue" TEXT, "planStatus" TEXT, "businessDriver1" TEXT, "businessDriver2" TEXT, "management" TEXT, "infrastructureLocation" TEXT, "computeModels" TEXT, "outputPDF" TEXT, "valueCheckbox" TEXT, "salesRepCheckbox" TEXT, )

#import "QueueResultModel.h"

@implementation QueueResultModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"QueueResult";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableQueueResult:(QueueResultField *) data
{
	NSString *firstName = data.firstName;
	NSString *lastName = data.lastName;
	NSString *email = data.email;
	NSString *company = data.company;
	NSString *resultTitle = data.resultTitle;
	NSString *otherConsiderations = data.otherConsiderations;
	NSString *jobTitle = data.jobTitle;
	NSString *businessPhone = data.businessPhone;
	NSString *country = data.country;
	NSString *zipCode = data.zipCode;
	NSString *promoCode = data.promoCode;
	NSString *resultBusinessDriverAddressed = data.resultBusinessDriverAddressed;
	NSString *isPlan = data.isPlan;
	NSString *HybridITScenarioCombinationsValue = data.HybridITScenarioCombinationsValue;
	NSString *planStatus = data.planStatus;
	NSString *businessDriver1 = data.businessDriver1;
	NSString *businessDriver2 = data.businessDriver2;
	NSString *management = data.management;
	NSString *infrastructureLocation = data.infrastructureLocation;
	NSString *computeModels = data.computeModels;
	NSString *outputPDF = data.outputPDF;
	NSString *valueCheckbox = data.valueCheckbox;
	NSString *salesRepCheckbox = data.salesRepCheckbox;

	NSString *firstNameInit = @"";
	NSString *lastNameInit = @"";
	NSString *emailInit = @"";
	NSString *companyInit = @"";
	NSString *resultTitleInit = @"";
	NSString *otherConsiderationsInit = @"";
	NSString *jobTitleInit = @"";
	NSString *businessPhoneInit = @"";
	NSString *countryInit = @"";
	NSString *zipCodeInit = @"";
	NSString *promoCodeInit = @"";
	NSString *resultBusinessDriverAddressedInit = @"";
	NSString *isPlanInit = @"";
	NSString *HybridITScenarioCombinationsValueInit = @"";
	NSString *planStatusInit = @"";
	NSString *businessDriver1Init = @"";
	NSString *businessDriver2Init = @"";
	NSString *managementInit = @"";
	NSString *infrastructureLocationInit = @"";
	NSString *computeModelsInit = @"";
	NSString *outputPDFInit = @"";
	NSString *valueCheckboxInit = @"";
	NSString *salesRepCheckboxInit = @"";


	if (firstName)
		firstNameInit = firstName;

	if (lastName)
		lastNameInit = lastName;

	if (email)
		emailInit = email;

	if (company)
		companyInit = company;

	if (resultTitle)
		resultTitleInit = resultTitle;

	if (otherConsiderations)
		otherConsiderationsInit = otherConsiderations;

	if (jobTitle)
		jobTitleInit = jobTitle;

	if (businessPhone)
		businessPhoneInit = businessPhone;

	if (country)
		countryInit = country;

	if (zipCode)
		zipCodeInit = zipCode;

	if (promoCode)
		promoCodeInit = promoCode;

	if (resultBusinessDriverAddressed)
		resultBusinessDriverAddressedInit = resultBusinessDriverAddressed;

	if (isPlan)
		isPlanInit = isPlan;

	if (HybridITScenarioCombinationsValue)
		HybridITScenarioCombinationsValueInit = HybridITScenarioCombinationsValue;

	if (planStatus)
		planStatusInit = planStatus;

	if (businessDriver1)
		businessDriver1Init = businessDriver1;

	if (businessDriver2)
		businessDriver2Init = businessDriver2;

	if (management)
		managementInit = management;

	if (infrastructureLocation)
		infrastructureLocationInit = infrastructureLocation;

	if (computeModels)
		computeModelsInit = computeModels;

	if (outputPDF)
		outputPDFInit = outputPDF;

	if (valueCheckbox)
		valueCheckboxInit = valueCheckbox;

	if (salesRepCheckbox)
		salesRepCheckboxInit = salesRepCheckbox;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"QueueResult\" (\"firstName\",\"lastName\",\"email\",\"company\",\"resultTitle\",\"otherConsiderations\",\"jobTitle\",\"businessPhone\",\"country\",\"zipCode\",\"promoCode\",\"resultBusinessDriverAddressed\",\"isPlan\",\"HybridITScenarioCombinationsValue\",\"planStatus\",\"businessDriver1\",\"businessDriver2\",\"management\",\"infrastructureLocation\",\"computeModels\",\"outputPDF\",\"valueCheckbox\",\"salesRepCheckbox\") VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16,?17,?18,?19,?20,?21,?22,?23)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--firstName
	sqlite3_bind_text(createStmt, 1, [firstNameInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--lastName
	sqlite3_bind_text(createStmt, 2, [lastNameInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--email
	sqlite3_bind_text(createStmt, 3, [emailInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--company
	sqlite3_bind_text(createStmt, 4, [companyInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultTitle
	sqlite3_bind_text(createStmt, 5, [resultTitleInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--otherConsiderations
	sqlite3_bind_text(createStmt, 6, [otherConsiderationsInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--jobTitle
	sqlite3_bind_text(createStmt, 7, [jobTitleInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--businessPhone
	sqlite3_bind_text(createStmt, 8, [businessPhoneInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--country
	sqlite3_bind_text(createStmt, 9, [countryInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--zipCode
	sqlite3_bind_text(createStmt, 10, [zipCodeInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--promoCode
	sqlite3_bind_text(createStmt, 11, [promoCodeInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 12, [resultBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--isPlan
	sqlite3_bind_text(createStmt, 13, [isPlanInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--HybridITScenarioCombinationsValue
	sqlite3_bind_text(createStmt, 14, [HybridITScenarioCombinationsValueInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--planStatus
	sqlite3_bind_text(createStmt, 15, [planStatusInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--businessDriver1
	sqlite3_bind_text(createStmt, 16, [businessDriver1Init UTF8String], -1, SQLITE_TRANSIENT);

 	//--businessDriver2
	sqlite3_bind_text(createStmt, 17, [businessDriver2Init UTF8String], -1, SQLITE_TRANSIENT);

 	//--management
	sqlite3_bind_text(createStmt, 18, [managementInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--infrastructureLocation
	sqlite3_bind_text(createStmt, 19, [infrastructureLocationInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--computeModels
	sqlite3_bind_text(createStmt, 20, [computeModelsInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--outputPDF
	sqlite3_bind_text(createStmt, 21, [outputPDFInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--valueCheckbox
	sqlite3_bind_text(createStmt, 22, [valueCheckboxInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--salesRepCheckbox
	sqlite3_bind_text(createStmt, 23, [salesRepCheckboxInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableQueueResult:(QueueResultField *) data
{

	NSString *firstName = data.firstName;
	NSString *lastName = data.lastName;
	NSString *email = data.email;
	NSString *company = data.company;
	NSString *resultTitle = data.resultTitle;
	NSString *otherConsiderations = data.otherConsiderations;
	NSString *jobTitle = data.jobTitle;
	NSString *businessPhone = data.businessPhone;
	NSString *country = data.country;
	NSString *zipCode = data.zipCode;
	NSString *promoCode = data.promoCode;
	NSString *resultBusinessDriverAddressed = data.resultBusinessDriverAddressed;
	NSString *isPlan = data.isPlan;
	NSString *HybridITScenarioCombinationsValue = data.HybridITScenarioCombinationsValue;
	NSString *planStatus = data.planStatus;
	NSString *businessDriver1 = data.businessDriver1;
	NSString *businessDriver2 = data.businessDriver2;
	NSString *management = data.management;
	NSString *infrastructureLocation = data.infrastructureLocation;
	NSString *computeModels = data.computeModels;
	NSString *outputPDF = data.outputPDF;
	NSString *valueCheckbox = data.valueCheckbox;
	NSString *salesRepCheckbox = data.salesRepCheckbox;

	NSString *firstNameInit = @"";
	NSString *lastNameInit = @"";
	NSString *emailInit = @"";
	NSString *companyInit = @"";
	NSString *resultTitleInit = @"";
	NSString *otherConsiderationsInit = @"";
	NSString *jobTitleInit = @"";
	NSString *businessPhoneInit = @"";
	NSString *countryInit = @"";
	NSString *zipCodeInit = @"";
	NSString *promoCodeInit = @"";
	NSString *resultBusinessDriverAddressedInit = @"";
	NSString *isPlanInit = @"";
	NSString *HybridITScenarioCombinationsValueInit = @"";
	NSString *planStatusInit = @"";
	NSString *businessDriver1Init = @"";
	NSString *businessDriver2Init = @"";
	NSString *managementInit = @"";
	NSString *infrastructureLocationInit = @"";
	NSString *computeModelsInit = @"";
	NSString *outputPDFInit = @"";
	NSString *valueCheckboxInit = @"";
	NSString *salesRepCheckboxInit = @"";


	if (firstName)
		firstNameInit = firstName;

	if (lastName)
		lastNameInit = lastName;

	if (email)
		emailInit = email;

	if (company)
		companyInit = company;

	if (resultTitle)
		resultTitleInit = resultTitle;

	if (otherConsiderations)
		otherConsiderationsInit = otherConsiderations;

	if (jobTitle)
		jobTitleInit = jobTitle;

	if (businessPhone)
		businessPhoneInit = businessPhone;

	if (country)
		countryInit = country;

	if (zipCode)
		zipCodeInit = zipCode;

	if (promoCode)
		promoCodeInit = promoCode;

	if (resultBusinessDriverAddressed)
		resultBusinessDriverAddressedInit = resultBusinessDriverAddressed;

	if (isPlan)
		isPlanInit = isPlan;

	if (HybridITScenarioCombinationsValue)
		HybridITScenarioCombinationsValueInit = HybridITScenarioCombinationsValue;

	if (planStatus)
		planStatusInit = planStatus;

	if (businessDriver1)
		businessDriver1Init = businessDriver1;

	if (businessDriver2)
		businessDriver2Init = businessDriver2;

	if (management)
		managementInit = management;

	if (infrastructureLocation)
		infrastructureLocationInit = infrastructureLocation;

	if (computeModels)
		computeModelsInit = computeModels;

	if (outputPDF)
		outputPDFInit = outputPDF;

	if (valueCheckbox)
		valueCheckboxInit = valueCheckbox;

	if (salesRepCheckbox)
		salesRepCheckboxInit = salesRepCheckbox;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"QueueResult\" SET \"firstName\" = ?, \"lastName\" = ?, \"email\" = ?, \"company\" = ?, \"resultTitle\" = ?, \"otherConsiderations\" = ?, \"jobTitle\" = ?, \"businessPhone\" = ?, \"country\" = ?, \"zipCode\" = ?, \"promoCode\" = ?, \"resultBusinessDriverAddressed\" = ?, \"isPlan\" = ?, \"HybridITScenarioCombinationsValue\" = ?, \"planStatus\" = ?, \"businessDriver1\" = ?, \"businessDriver2\" = ?, \"management\" = ?, \"infrastructureLocation\" = ?, \"computeModels\" = ?, \"outputPDF\" = ?, \"valueCheckbox\" = ?, \"salesRepCheckbox\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--firstName
	sqlite3_bind_text(createStmt, 1, [firstNameInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--lastName
	sqlite3_bind_text(createStmt, 2, [lastNameInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--email
	sqlite3_bind_text(createStmt, 3, [emailInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--company
	sqlite3_bind_text(createStmt, 4, [companyInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultTitle
	sqlite3_bind_text(createStmt, 5, [resultTitleInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--otherConsiderations
	sqlite3_bind_text(createStmt, 6, [otherConsiderationsInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--jobTitle
	sqlite3_bind_text(createStmt, 7, [jobTitleInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--businessPhone
	sqlite3_bind_text(createStmt, 8, [businessPhoneInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--country
	sqlite3_bind_text(createStmt, 9, [countryInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--zipCode
	sqlite3_bind_text(createStmt, 10, [zipCodeInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--promoCode
	sqlite3_bind_text(createStmt, 11, [promoCodeInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 12, [resultBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--isPlan
	sqlite3_bind_text(createStmt, 13, [isPlanInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--HybridITScenarioCombinationsValue
	sqlite3_bind_text(createStmt, 14, [HybridITScenarioCombinationsValueInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--planStatus
	sqlite3_bind_text(createStmt, 15, [planStatusInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--businessDriver1
	sqlite3_bind_text(createStmt, 16, [businessDriver1Init UTF8String], -1, SQLITE_TRANSIENT);

 	//--businessDriver2
	sqlite3_bind_text(createStmt, 17, [businessDriver2Init UTF8String], -1, SQLITE_TRANSIENT);

 	//--management
	sqlite3_bind_text(createStmt, 18, [managementInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--infrastructureLocation
	sqlite3_bind_text(createStmt, 19, [infrastructureLocationInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--computeModels
	sqlite3_bind_text(createStmt, 20, [computeModelsInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--outputPDF
	sqlite3_bind_text(createStmt, 21, [outputPDFInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--valueCheckbox
	sqlite3_bind_text(createStmt, 22, [valueCheckboxInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--salesRepCheckbox
	sqlite3_bind_text(createStmt, 23, [salesRepCheckboxInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableQueueResult
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--firstName
			char *firstName = (char *) sqlite3_column_text(statement, 1);
			NSString *firstNameStr = (firstName == NULL) ? @"":[NSString stringWithUTF8String:firstName];

			//--lastName
			char *lastName = (char *) sqlite3_column_text(statement, 2);
			NSString *lastNameStr = (lastName == NULL) ? @"":[NSString stringWithUTF8String:lastName];

			//--email
			char *email = (char *) sqlite3_column_text(statement, 3);
			NSString *emailStr = (email == NULL) ? @"":[NSString stringWithUTF8String:email];

			//--company
			char *company = (char *) sqlite3_column_text(statement, 4);
			NSString *companyStr = (company == NULL) ? @"":[NSString stringWithUTF8String:company];

			//--resultTitle
			char *resultTitle = (char *) sqlite3_column_text(statement, 5);
			NSString *resultTitleStr = (resultTitle == NULL) ? @"":[NSString stringWithUTF8String:resultTitle];

			//--otherConsiderations
			char *otherConsiderations = (char *) sqlite3_column_text(statement, 6);
			NSString *otherConsiderationsStr = (otherConsiderations == NULL) ? @"":[NSString stringWithUTF8String:otherConsiderations];

			//--jobTitle
			char *jobTitle = (char *) sqlite3_column_text(statement, 7);
			NSString *jobTitleStr = (jobTitle == NULL) ? @"":[NSString stringWithUTF8String:jobTitle];

			//--businessPhone
			char *businessPhone = (char *) sqlite3_column_text(statement, 8);
			NSString *businessPhoneStr = (businessPhone == NULL) ? @"":[NSString stringWithUTF8String:businessPhone];

			//--country
			char *country = (char *) sqlite3_column_text(statement, 9);
			NSString *countryStr = (country == NULL) ? @"":[NSString stringWithUTF8String:country];

			//--zipCode
			char *zipCode = (char *) sqlite3_column_text(statement, 10);
			NSString *zipCodeStr = (zipCode == NULL) ? @"":[NSString stringWithUTF8String:zipCode];

			//--promoCode
			char *promoCode = (char *) sqlite3_column_text(statement, 11);
			NSString *promoCodeStr = (promoCode == NULL) ? @"":[NSString stringWithUTF8String:promoCode];

			//--resultBusinessDriverAddressed
			char *resultBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 12);
			NSString *resultBusinessDriverAddressedStr = (resultBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:resultBusinessDriverAddressed];

			//--isPlan
			char *isPlan = (char *) sqlite3_column_text(statement, 13);
			NSString *isPlanStr = (isPlan == NULL) ? @"":[NSString stringWithUTF8String:isPlan];

			//--HybridITScenarioCombinationsValue
			char *HybridITScenarioCombinationsValue = (char *) sqlite3_column_text(statement, 14);
			NSString *HybridITScenarioCombinationsValueStr = (HybridITScenarioCombinationsValue == NULL) ? @"":[NSString stringWithUTF8String:HybridITScenarioCombinationsValue];

			//--planStatus
			char *planStatus = (char *) sqlite3_column_text(statement, 15);
			NSString *planStatusStr = (planStatus == NULL) ? @"":[NSString stringWithUTF8String:planStatus];

			//--businessDriver1
			char *businessDriver1 = (char *) sqlite3_column_text(statement, 16);
			NSString *businessDriver1Str = (businessDriver1 == NULL) ? @"":[NSString stringWithUTF8String:businessDriver1];

			//--businessDriver2
			char *businessDriver2 = (char *) sqlite3_column_text(statement, 17);
			NSString *businessDriver2Str = (businessDriver2 == NULL) ? @"":[NSString stringWithUTF8String:businessDriver2];

			//--management
			char *management = (char *) sqlite3_column_text(statement, 18);
			NSString *managementStr = (management == NULL) ? @"":[NSString stringWithUTF8String:management];

			//--infrastructureLocation
			char *infrastructureLocation = (char *) sqlite3_column_text(statement, 19);
			NSString *infrastructureLocationStr = (infrastructureLocation == NULL) ? @"":[NSString stringWithUTF8String:infrastructureLocation];

			//--computeModels
			char *computeModels = (char *) sqlite3_column_text(statement, 20);
			NSString *computeModelsStr = (computeModels == NULL) ? @"":[NSString stringWithUTF8String:computeModels];

			//--outputPDF
			char *outputPDF = (char *) sqlite3_column_text(statement, 21);
			NSString *outputPDFStr = (outputPDF == NULL) ? @"":[NSString stringWithUTF8String:outputPDF];

			//--valueCheckbox
			char *valueCheckbox = (char *) sqlite3_column_text(statement, 22);
			NSString *valueCheckboxStr = (valueCheckbox == NULL) ? @"":[NSString stringWithUTF8String:valueCheckbox];

			//--salesRepCheckbox
			char *salesRepCheckbox = (char *) sqlite3_column_text(statement, 23);
			NSString *salesRepCheckboxStr = (salesRepCheckbox == NULL) ? @"":[NSString stringWithUTF8String:salesRepCheckbox];



			//--add Object
			QueueResultField *object = [[QueueResultField alloc]init];

			object.rowid = rowidStr;
			object.firstName = firstNameStr;
			object.lastName = lastNameStr;
			object.email = emailStr;
			object.company = companyStr;
			object.resultTitle = resultTitleStr;
			object.otherConsiderations = otherConsiderationsStr;
			object.jobTitle = jobTitleStr;
			object.businessPhone = businessPhoneStr;
			object.country = countryStr;
			object.zipCode = zipCodeStr;
			object.promoCode = promoCodeStr;
			object.resultBusinessDriverAddressed = resultBusinessDriverAddressedStr;
			object.isPlan = isPlanStr;
			object.HybridITScenarioCombinationsValue = HybridITScenarioCombinationsValueStr;
			object.planStatus = planStatusStr;
			object.businessDriver1 = businessDriver1Str;
			object.businessDriver2 = businessDriver2Str;
			object.management = managementStr;
			object.infrastructureLocation = infrastructureLocationStr;
			object.computeModels = computeModelsStr;
			object.outputPDF = outputPDFStr;
			object.valueCheckbox = valueCheckboxStr;
			object.salesRepCheckbox = salesRepCheckboxStr;
			
			[array addObject:object];

			//--free memory
			

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(QueueResultField *) getRowByRowsInTableQueueResultByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_QueueResultField_BySQL:qsql];
}


//--Retrieving Data
-(QueueResultField *) getData_QueueResultField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	QueueResultField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--firstName
			char *firstName = (char *) sqlite3_column_text(statement, 1);
			NSString *firstNameStr = (firstName == NULL) ? @"":[NSString stringWithUTF8String:firstName];

			//--lastName
			char *lastName = (char *) sqlite3_column_text(statement, 2);
			NSString *lastNameStr = (lastName == NULL) ? @"":[NSString stringWithUTF8String:lastName];

			//--email
			char *email = (char *) sqlite3_column_text(statement, 3);
			NSString *emailStr = (email == NULL) ? @"":[NSString stringWithUTF8String:email];

			//--company
			char *company = (char *) sqlite3_column_text(statement, 4);
			NSString *companyStr = (company == NULL) ? @"":[NSString stringWithUTF8String:company];

			//--resultTitle
			char *resultTitle = (char *) sqlite3_column_text(statement, 5);
			NSString *resultTitleStr = (resultTitle == NULL) ? @"":[NSString stringWithUTF8String:resultTitle];

			//--otherConsiderations
			char *otherConsiderations = (char *) sqlite3_column_text(statement, 6);
			NSString *otherConsiderationsStr = (otherConsiderations == NULL) ? @"":[NSString stringWithUTF8String:otherConsiderations];

			//--jobTitle
			char *jobTitle = (char *) sqlite3_column_text(statement, 7);
			NSString *jobTitleStr = (jobTitle == NULL) ? @"":[NSString stringWithUTF8String:jobTitle];

			//--businessPhone
			char *businessPhone = (char *) sqlite3_column_text(statement, 8);
			NSString *businessPhoneStr = (businessPhone == NULL) ? @"":[NSString stringWithUTF8String:businessPhone];

			//--country
			char *country = (char *) sqlite3_column_text(statement, 9);
			NSString *countryStr = (country == NULL) ? @"":[NSString stringWithUTF8String:country];

			//--zipCode
			char *zipCode = (char *) sqlite3_column_text(statement, 10);
			NSString *zipCodeStr = (zipCode == NULL) ? @"":[NSString stringWithUTF8String:zipCode];

			//--promoCode
			char *promoCode = (char *) sqlite3_column_text(statement, 11);
			NSString *promoCodeStr = (promoCode == NULL) ? @"":[NSString stringWithUTF8String:promoCode];

			//--resultBusinessDriverAddressed
			char *resultBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 12);
			NSString *resultBusinessDriverAddressedStr = (resultBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:resultBusinessDriverAddressed];

			//--isPlan
			char *isPlan = (char *) sqlite3_column_text(statement, 13);
			NSString *isPlanStr = (isPlan == NULL) ? @"":[NSString stringWithUTF8String:isPlan];

			//--HybridITScenarioCombinationsValue
			char *HybridITScenarioCombinationsValue = (char *) sqlite3_column_text(statement, 14);
			NSString *HybridITScenarioCombinationsValueStr = (HybridITScenarioCombinationsValue == NULL) ? @"":[NSString stringWithUTF8String:HybridITScenarioCombinationsValue];

			//--planStatus
			char *planStatus = (char *) sqlite3_column_text(statement, 15);
			NSString *planStatusStr = (planStatus == NULL) ? @"":[NSString stringWithUTF8String:planStatus];

			//--businessDriver1
			char *businessDriver1 = (char *) sqlite3_column_text(statement, 16);
			NSString *businessDriver1Str = (businessDriver1 == NULL) ? @"":[NSString stringWithUTF8String:businessDriver1];

			//--businessDriver2
			char *businessDriver2 = (char *) sqlite3_column_text(statement, 17);
			NSString *businessDriver2Str = (businessDriver2 == NULL) ? @"":[NSString stringWithUTF8String:businessDriver2];

			//--management
			char *management = (char *) sqlite3_column_text(statement, 18);
			NSString *managementStr = (management == NULL) ? @"":[NSString stringWithUTF8String:management];

			//--infrastructureLocation
			char *infrastructureLocation = (char *) sqlite3_column_text(statement, 19);
			NSString *infrastructureLocationStr = (infrastructureLocation == NULL) ? @"":[NSString stringWithUTF8String:infrastructureLocation];

			//--computeModels
			char *computeModels = (char *) sqlite3_column_text(statement, 20);
			NSString *computeModelsStr = (computeModels == NULL) ? @"":[NSString stringWithUTF8String:computeModels];

			//--outputPDF
			char *outputPDF = (char *) sqlite3_column_text(statement, 21);
			NSString *outputPDFStr = (outputPDF == NULL) ? @"":[NSString stringWithUTF8String:outputPDF];

			//--valueCheckbox
			char *valueCheckbox = (char *) sqlite3_column_text(statement, 22);
			NSString *valueCheckboxStr = (valueCheckbox == NULL) ? @"":[NSString stringWithUTF8String:valueCheckbox];

			//--salesRepCheckbox
			char *salesRepCheckbox = (char *) sqlite3_column_text(statement, 23);
			NSString *salesRepCheckboxStr = (salesRepCheckbox == NULL) ? @"":[NSString stringWithUTF8String:salesRepCheckbox];



			//--add Object
			object = [[QueueResultField alloc] init];

			object.rowid = rowidStr;
			object.firstName = firstNameStr;
			object.lastName = lastNameStr;
			object.email = emailStr;
			object.company = companyStr;
			object.resultTitle = resultTitleStr;
			object.otherConsiderations = otherConsiderationsStr;
			object.jobTitle = jobTitleStr;
			object.businessPhone = businessPhoneStr;
			object.country = countryStr;
			object.zipCode = zipCodeStr;
			object.promoCode = promoCodeStr;
			object.resultBusinessDriverAddressed = resultBusinessDriverAddressedStr;
			object.isPlan = isPlanStr;
			object.HybridITScenarioCombinationsValue = HybridITScenarioCombinationsValueStr;
			object.planStatus = planStatusStr;
			object.businessDriver1 = businessDriver1Str;
			object.businessDriver2 = businessDriver2Str;
			object.management = managementStr;
			object.infrastructureLocation = infrastructureLocationStr;
			object.computeModels = computeModelsStr;
			object.outputPDF = outputPDFStr;
			object.valueCheckbox = valueCheckboxStr;
			object.salesRepCheckbox = salesRepCheckboxStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end