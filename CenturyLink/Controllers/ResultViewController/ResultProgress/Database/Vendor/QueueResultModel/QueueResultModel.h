

////QueueResultModel.h 
//  Project 
// 
//  Created by TeamiOS on 2015-01-20 07:01:22 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "QueueResultField.h"

@interface QueueResultModel : AppModel
{

}


/** Get all rows in Table QueueResult **/
-(NSMutableArray *) getAllRowsInTableQueueResult;

/** Get rows by rowid in Table QueueResult **/
-(QueueResultField *) getRowByRowsInTableQueueResultByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableQueueResult:(QueueResultField *) data;

/** update row **/
-(BOOL)updateRowInTableQueueResult:(QueueResultField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end