

////ResultModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-11-10 03:03:19 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "ResultField.h"

@interface ResultModel : AppModel
{

}


/** Get all rows in Table Result **/
-(NSMutableArray *) getAllRowsInTableResult;

/** Get rows by rowid in Table Result **/
-(ResultField *) getRowByRowsInTableResultByRowId:(NSString *) rowid;

/** Get rows by rowid in Table Result isplan or not **/
-(NSMutableArray *) getAllRowsInTableResultISPlanOrNot:(NSString *)isPlan;


/** create a new row **/
-(BOOL)createRowInTableResult:(ResultField *) data;

/** update row **/
-(BOOL)updateRowInTableResult:(ResultField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end