

////ResultField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-11-10 03:03:19 +0000. 
//

//--CREATE TABLE "Result" ("choice" TEXT, "resultTitle" TEXT, "otherConsiderations" TEXT, "resultBusinessDriverAddressed" TEXT, "isPlan" TEXT, )

#import "ResultModel.h"

@implementation ResultModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"Result";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableResult:(ResultField *) data
{
	NSString *choice = data.choice;
	NSString *resultTitle = data.resultTitle;
	NSString *otherConsiderations = data.otherConsiderations;
	NSString *resultBusinessDriverAddressed = data.resultBusinessDriverAddressed;
	NSString *isPlan = data.isPlan;

	NSString *choiceInit = @"";
	NSString *resultTitleInit = @"";
	NSString *otherConsiderationsInit = @"";
	NSString *resultBusinessDriverAddressedInit = @"";
	NSString *isPlanInit = @"";


	if (choice)
		choiceInit = choice;

	if (resultTitle)
		resultTitleInit = resultTitle;

	if (otherConsiderations)
		otherConsiderationsInit = otherConsiderations;

	if (resultBusinessDriverAddressed)
		resultBusinessDriverAddressedInit = resultBusinessDriverAddressed;

	if (isPlan)
		isPlanInit = isPlan;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"Result\" (\"choice\",\"resultTitle\",\"otherConsiderations\",\"resultBusinessDriverAddressed\",\"isPlan\") VALUES (?1,?2,?3,?4,?5)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--choice
	sqlite3_bind_text(createStmt, 1, [choiceInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultTitle
	sqlite3_bind_text(createStmt, 2, [resultTitleInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--otherConsiderations
	sqlite3_bind_text(createStmt, 3, [otherConsiderationsInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 4, [resultBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--isPlan
	sqlite3_bind_text(createStmt, 5, [isPlanInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableResult:(ResultField *) data
{

	NSString *choice = data.choice;
	NSString *resultTitle = data.resultTitle;
	NSString *otherConsiderations = data.otherConsiderations;
	NSString *resultBusinessDriverAddressed = data.resultBusinessDriverAddressed;
	NSString *isPlan = data.isPlan;

	NSString *choiceInit = @"";
	NSString *resultTitleInit = @"";
	NSString *otherConsiderationsInit = @"";
	NSString *resultBusinessDriverAddressedInit = @"";
	NSString *isPlanInit = @"";


	if (choice)
		choiceInit = choice;

	if (resultTitle)
		resultTitleInit = resultTitle;

	if (otherConsiderations)
		otherConsiderationsInit = otherConsiderations;

	if (resultBusinessDriverAddressed)
		resultBusinessDriverAddressedInit = resultBusinessDriverAddressed;

	if (isPlan)
		isPlanInit = isPlan;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"Result\" SET \"choice\" = ?, \"resultTitle\" = ?, \"otherConsiderations\" = ?, \"resultBusinessDriverAddressed\" = ?, \"isPlan\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--choice
	sqlite3_bind_text(createStmt, 1, [choiceInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultTitle
	sqlite3_bind_text(createStmt, 2, [resultTitleInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--otherConsiderations
	sqlite3_bind_text(createStmt, 3, [otherConsiderationsInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--resultBusinessDriverAddressed
	sqlite3_bind_text(createStmt, 4, [resultBusinessDriverAddressedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--isPlan
	sqlite3_bind_text(createStmt, 5, [isPlanInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableResult
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


-(NSMutableArray *) getAllRowsInTableResultISPlanOrNot:(NSString *)isPlan
{
    //--retrive rows
    NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE isPlan = \"%@\" ", self.usesTable,isPlan];
    
    return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--choice
			char *choice = (char *) sqlite3_column_text(statement, 1);
			NSString *choiceStr = (choice == NULL) ? @"":[NSString stringWithUTF8String:choice];

			//--resultTitle
			char *resultTitle = (char *) sqlite3_column_text(statement, 2);
			NSString *resultTitleStr = (resultTitle == NULL) ? @"":[NSString stringWithUTF8String:resultTitle];

			//--otherConsiderations
			char *otherConsiderations = (char *) sqlite3_column_text(statement, 3);
			NSString *otherConsiderationsStr = (otherConsiderations == NULL) ? @"":[NSString stringWithUTF8String:otherConsiderations];

			//--resultBusinessDriverAddressed
			char *resultBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 4);
			NSString *resultBusinessDriverAddressedStr = (resultBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:resultBusinessDriverAddressed];

			//--isPlan
			char *isPlan = (char *) sqlite3_column_text(statement, 5);
			NSString *isPlanStr = (isPlan == NULL) ? @"":[NSString stringWithUTF8String:isPlan];



			//--add Object
			ResultField *object = [[ResultField alloc]init];

			object.rowid = rowidStr;
			object.choice = choiceStr;
			object.resultTitle = resultTitleStr;
			object.otherConsiderations = otherConsiderationsStr;
			object.resultBusinessDriverAddressed = resultBusinessDriverAddressedStr;
			object.isPlan = isPlanStr;
			
			[array addObject:object];

			//--free memory
			

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(ResultField *) getRowByRowsInTableResultByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_ResultField_BySQL:qsql];
}


//--Retrieving Data
-(ResultField *) getData_ResultField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	ResultField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--choice
			char *choice = (char *) sqlite3_column_text(statement, 1);
			NSString *choiceStr = (choice == NULL) ? @"":[NSString stringWithUTF8String:choice];

			//--resultTitle
			char *resultTitle = (char *) sqlite3_column_text(statement, 2);
			NSString *resultTitleStr = (resultTitle == NULL) ? @"":[NSString stringWithUTF8String:resultTitle];

			//--otherConsiderations
			char *otherConsiderations = (char *) sqlite3_column_text(statement, 3);
			NSString *otherConsiderationsStr = (otherConsiderations == NULL) ? @"":[NSString stringWithUTF8String:otherConsiderations];

			//--resultBusinessDriverAddressed
			char *resultBusinessDriverAddressed = (char *) sqlite3_column_text(statement, 4);
			NSString *resultBusinessDriverAddressedStr = (resultBusinessDriverAddressed == NULL) ? @"":[NSString stringWithUTF8String:resultBusinessDriverAddressed];

			//--isPlan
			char *isPlan = (char *) sqlite3_column_text(statement, 5);
			NSString *isPlanStr = (isPlan == NULL) ? @"":[NSString stringWithUTF8String:isPlan];



			//--add Object
			object = [[ResultField alloc] init];

			object.rowid = rowidStr;
			object.choice = choiceStr;
			object.resultTitle = resultTitleStr;
			object.otherConsiderations = otherConsiderationsStr;
			object.resultBusinessDriverAddressed = resultBusinessDriverAddressedStr;
			object.isPlan = isPlanStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end