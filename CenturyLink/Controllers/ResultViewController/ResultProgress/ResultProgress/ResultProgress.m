//
//  ViewController.m
//  DemoTestCenteryLink
//
//  Created by Thien Thanh on 11/6/14.
//  Copyright (c) 2014 Thien Thanh. All rights reserved.
//

#import "ResultProgress.h"
#import "AppDelegate.h"
#import "Common.h"


#define kDefineStyleTagli @"<li style='list-style-type:none;'>"
#define kBusinessdriversmet @"<li>Business drivers met: "

@interface ResultProgress ()

//  input answer Q&A Outcome FLow(with Plan)
@property (nonatomic , strong) NSArray *arrayAnswerReturnResultK;
@property (nonatomic , strong) NSArray *arrayAnswerReturnResultL;
@property (nonatomic , strong) NSArray *arrayAnswerReturnResultM;
@property (nonatomic , strong) NSArray *arrayAnswerReturnResultN;
@property (nonatomic , strong) NSArray *arrayAnswerReturnResultO;

@property (nonatomic , strong) NSArray *arrayResult;
@property (nonatomic , strong) NSArray *arrayHybridITScenarioCombinations;

@end

@implementation ResultProgress



/********************************************************************************/
#pragma mark - Answer


-(BOOL)validateListAnswerPass:(NSArray *)arrayAnswer
{
    //  If B is selected in step 3, the slider is locked at 100% 3rd Party Data Centers (you cannot move it) -- show a note "You chose 100% 3rd Party Managed"
    if ([arrayAnswer[0] isEqualToString:@"b"] && ![arrayAnswer[1] isEqualToString:@"e"]) {
        return NO;
    }
    
    //  G is disabled if B is selected
    if ([arrayAnswer[0] isEqualToString:@"b"] && [arrayAnswer[2] containsObject:@"g"]) {
        return NO;
    }
    
    //  H is disabled if A is selected
    if ([arrayAnswer[0] isEqualToString:@"a"] && [arrayAnswer[2] containsObject:@"h"]) {
        return NO;
    }
    
    //  i is disabled if D or A is selected
    if (([arrayAnswer[1] isEqualToString:@"d"] && [arrayAnswer[2] containsObject:@"i"]) || ([arrayAnswer[0] isEqualToString:@"a"] && [arrayAnswer[2] containsObject:@"i"])) {
        return NO;
    }
    
    
    //  j is disabled if D or A is selected
    if (([arrayAnswer[1] isEqualToString:@"d"] && [arrayAnswer[2] containsObject:@"j"]) || ([arrayAnswer[0] isEqualToString:@"a"] && [arrayAnswer[2] containsObject:@"j"])) {
        return NO;
    }
    
    return YES;
}




/**
 * Init Condition
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)initCondition:(BOOL)isPlan
{
//    if (isPlan) {
//        //  List Answer return result K
//        _arrayAnswerReturnResultK = [NSArray arrayWithObjects:@"a",@"c",@"d",@"f",@"g",@"j", nil];
//        
//        //  List Answer return result L
//        _arrayAnswerReturnResultL = [NSArray arrayWithObjects:@"g",@"h",@"e",@"a", nil];
//        
//        //  List Answer return result M
//        _arrayAnswerReturnResultM = [NSArray arrayWithObjects:@"b",@"c",@"e",@"f",@"i", nil];
//        
//        //  List Answer return result N
//        _arrayAnswerReturnResultN = [NSArray arrayWithObjects:@"b",@"c",@"e",@"f",@"j", nil];
//        
//        //  List Answer return result O
////        _arrayAnswerReturnResultO = [NSArray arrayWithObjects:@"b",@"c",@"e",@"f",@"h", nil];
//    }
//    else
//    {
//        //  List Answer return result K
//        _arrayAnswerReturnResultK = [NSArray arrayWithObjects:@"c",@"g",@"h",@"e",@"a", nil];
//        
//        //  List Answer return result L
//        _arrayAnswerReturnResultL = [NSArray arrayWithObjects:@"a",@"c",@"d",@"f",@"g",@"j", nil];
//        
//        //  List Answer return result M
//        _arrayAnswerReturnResultM = [NSArray arrayWithObjects:@"b",@"c",@"e",@"f",@"i", nil];
//        
//        //  List Answer return result N
//        _arrayAnswerReturnResultN = [NSArray arrayWithObjects:@"b",@"c",@"e",@"f",@"j", nil];
//        
//        //  List Answer return result O
////        _arrayAnswerReturnResultO = [NSArray arrayWithObjects:@"b",@"c",@"e",@"f",@"h", nil];
//    }

    if (isPlan) {
        //  List Result
        _arrayResult = [NSArray arrayWithObjects:@"k",@"l",@"m",@"n",@"o", nil];
    }
    else
    {
        //  List Result
        _arrayResult = [NSArray arrayWithObjects:@"k",@"l",@"m",@"n", nil];
    }
    
}



/**
 * Result K
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(ResultField *)returnResultWhenUserSelectedAnswer:(NSMutableArray *)arrayAnswer andArrayAnswerStep2:(NSMutableArray *)arrayAnswerStep2 andIsPlanOrNot:(BOOL)isPlan
{

    NSMutableArray *arrayResult = [NSMutableArray new];

    //  validate input answer
    if (![self validateListAnswerPass:arrayAnswer]) {
        return nil;
    }

    
    //  loop all answer
    NSMutableSet *arrayChoice = [self checkAnswerShowFinalResult:arrayAnswer andIsPlanOrNot:isPlan];
    
    
    //  Value Default Business Driver Addressed
    NSMutableArray *arrayBusinessDriverAddressedValue          = [NSMutableArray new];
    BusinessDriverAddressedModel *businessDriverAddressedModel = [BusinessDriverAddressedModel new];
    NSArray *listBusinessDriverAddressed                       = [businessDriverAddressedModel getAllRowsInTableBusinessDriverAddressed];
    
    for (NSString *key in arrayAnswerStep2) {
        for (BusinessDriverAddressedField *businessDriverAddressedField in listBusinessDriverAddressed) {
            if ([[businessDriverAddressedField.keyBusinessDriverAddressed lowercaseString] isEqualToString:[key lowercaseString]])
            {
                businessDriverAddressedField.valueBusinessDriverAddressed = [businessDriverAddressedField.valueBusinessDriverAddressed stringByReplacingOccurrencesOfString:@"<li>" withString:@"<li><span>"];
                businessDriverAddressedField.valueBusinessDriverAddressed = [businessDriverAddressedField.valueBusinessDriverAddressed stringByReplacingOccurrencesOfString:@"</li>" withString:@"</span></li>"];
                [arrayBusinessDriverAddressedValue addObject:businessDriverAddressedField.valueBusinessDriverAddressed];
            }
        }
    }
    
    
    //  all plan result
    ResultModel *resultModel    = [ResultModel new];
    NSString *isPlanValue       = isPlan ? @"1" : @"0";
    NSArray *arrayAllPlanResult = [resultModel getAllRowsInTableResultISPlanOrNot:isPlanValue];

    //  get result match with choice
    for (NSString *choice in arrayChoice) {
        for (ResultField *resultField in arrayAllPlanResult) {
            if ([resultField.choice isEqualToString:choice]) {
                resultField.resultBusinessDriverAddressed = [resultField.resultBusinessDriverAddressed stringByReplacingOccurrencesOfString:@"\n" withString:@""];

                [arrayResult addObject:resultField];
                break;
            }
        }
    }
    
    
    // sumup final
    NSMutableSet *arrayBusinessDriverAddressed = [NSMutableSet new];
    NSString *title                            = @"";
    NSString *choices                          = @"";
    NSString *resultBusinessDriverAddressed    = @"";
    for (ResultField *resultField in arrayResult) {
        [arrayBusinessDriverAddressed addObjectsFromArray:resultField.arrayBusinessDriverAddressed];
        choices = [choices stringByAppendingString:[NSString stringWithFormat:@"%@ , ",resultField.choice]];
        resultField.resultTitle = [resultField.resultTitle stringByReplacingOccurrencesOfString:@"<li>" withString:@"<li><span>"];
        resultField.resultTitle = [resultField.resultTitle stringByReplacingOccurrencesOfString:@"</li>" withString:@"</span></li>"];
        title = [title stringByAppendingString:[NSString stringWithFormat:@"%@",resultField.resultTitle]];
    }
    
    
    //  co phương án (k) hay không (k) đều được nên add thêm vào mảng nếu ko có (k)
    NSMutableArray *groupResultHybridITScenarioCombinations = [NSMutableArray arrayWithArray:[arrayChoice allObjects]];
    if (![groupResultHybridITScenarioCombinations containsObject:@"k"]) {
        [groupResultHybridITScenarioCombinations addObject:@"k"];
    }
    
    HybridITScenarioCombinationsField *hybridITScenarioCombinationsField = [self returnResultHybridITScenarioCombinations:groupResultHybridITScenarioCombinations];

    for (int i = 0; i < arrayBusinessDriverAddressed.count; i++) {
        NSString *businessDriverAddressed = [arrayBusinessDriverAddressed allObjects][i];
        businessDriverAddressed           = [businessDriverAddressed stringByReplacingOccurrencesOfString:@"<li>" withString:kDefineStyleTagli];
        resultBusinessDriverAddressed     = [resultBusinessDriverAddressed stringByAppendingString:[NSString stringWithFormat:@"%@",businessDriverAddressed]];
    }

    
    //  save data
    ResultField *resultFinal                      = [ResultField new];
    resultFinal.resultTitle                       = title;
    resultFinal.choice                            = choices;
    resultFinal.otherConsiderations               = @"";
    resultFinal.resultBusinessDriverAddressed     = @" ";
    resultFinal.arrayBusinessDriverAddressed      = (NSMutableArray *)[arrayBusinessDriverAddressed allObjects];
    resultFinal.HybridITScenarioCombinationsValue = hybridITScenarioCombinationsField.value;
    resultFinal.arrayAnswerInStep2                = arrayBusinessDriverAddressedValue;
    
    // save into data submit
    AppDelegate *appDelegate;
    if (!appDelegate) {
        appDelegate = [Common getAppDelegate];
    }
    appDelegate.queueResultField.outputPDF        = hybridITScenarioCombinationsField.key;
    
    return resultFinal;
}




/**
 * show final result
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(NSMutableSet *)checkAnswerShowFinalResult:(NSArray *)arrayAnswer andIsPlanOrNot:(BOOL)isPlan
{
    NSMutableSet *choices = [NSMutableSet new];
    
    //  k always show
    [choices addObject:@"k"];

    //  show if these are selected: i
    if ([arrayAnswer[2] containsObject:@"i"])
    {
        [choices addObject:@"m"];
    }
    
    //  show if these are selected: j
    if ([arrayAnswer[2] containsObject:@"j"])
    {
        [choices addObject:@"n"];
    }
    
    //  show if these are selected: h
    if ([arrayAnswer[2] containsObject:@"h"])
    {
        [choices addObject:@"o"];
    }
    
    
    //  show if these are selected: j
    //  (a or c)
    if ([arrayAnswer[0] isEqualToString:@"a"] || [arrayAnswer[0] isEqualToString:@"c"]) {
        //  (e or f)
        if ([arrayAnswer[1] isEqualToString:@"e"] || [arrayAnswer[1] isEqualToString:@"f"]) {
            //  g
            if ([arrayAnswer[2] containsObject:@"g"])
            {
                [choices addObject:@"l"];
            }
        }
    }
    
    return choices;
}


/**
 * Hybrid IT Scenario Combinations
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(HybridITScenarioCombinationsField *)returnResultHybridITScenarioCombinations:(NSArray *)groupResult
{
    
    
    HybridITScenarioCombinationsModel *hybridITScenarioCombinationsModel = [HybridITScenarioCombinationsModel new];
    _arrayHybridITScenarioCombinations = [hybridITScenarioCombinationsModel getAllRowsInTableHybridITScenarioCombinations];

    
    int condition = 0;
    for (HybridITScenarioCombinationsField *hybridITScenarioCombinationsField in _arrayHybridITScenarioCombinations) {
        
        // lấy số lượng phần tử của mảng điều kiện
        NSMutableArray *conditionArray = [NSMutableArray arrayWithArray:[hybridITScenarioCombinationsField.condition componentsSeparatedByString:@"+"]];
        for (int i = 0; i < conditionArray.count ; i++) {
            if ([conditionArray[i] length] == 0) {
                [conditionArray removeObjectAtIndex:i];
            }
        }
        
        // so sánh số lượng phần tử của mạng điều kiện và mảng câu trả lời nếu bằng nhau thì bắt đầu kiểm tra
        if (conditionArray.count == groupResult.count) {
            condition = 0;
            //  kiểm tra từng câu trả lời có nằm trong mảng điều kiện ko
            for (NSString *result in groupResult) {
                if ([hybridITScenarioCombinationsField.condition rangeOfString:result].location != NSNotFound) {
                    condition++;
                }
            }
            
            //  nếu toàn bộ câu trả lời thỏa tất cả các điều kiện thì true
            if (condition == groupResult.count) {
                return hybridITScenarioCombinationsField;
            }
        }
        
        
        
        
    }

    
    return nil;
}


/********************************************************************************/

@end
