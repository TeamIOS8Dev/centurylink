//
//  ViewController.h
//  DemoTestCenteryLink
//
//  Created by Thien Thanh on 11/6/14.
//  Copyright (c) 2014 Thien Thanh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Model.h"

@interface ResultProgress : NSObject

//  validate group answer
-(BOOL)validateListAnswerPass:(NSArray *)arrayAnswer;

//  result
-(ResultField *)returnResultWhenUserSelectedAnswer:(NSMutableArray *)arrayAnswer andArrayAnswerStep2:(NSMutableArray *)arrayAnswerStep2 andIsPlanOrNot:(BOOL)isPlan;


//  Hybrid IT Scenario Combinations
-(HybridITScenarioCombinationsField *)returnResultHybridITScenarioCombinations:(NSArray *)groupResult;
@end

