//
//  ProgressBarViewController.h
//  CenturyLink
//
//  Created by Thien Thanh on 11/27/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressBarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblFailedNumber;

@property (weak, nonatomic) IBOutlet UILabel *lblSuccessNumber;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@end
