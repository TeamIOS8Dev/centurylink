//
//  FieldValue.h
//  CenturyLink
//
//  Created by Thien Thanh on 11/24/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FieldValue : NSObject

@property (nonatomic , strong) NSString *idString;
@property (nonatomic , strong) NSString *type;
@property (nonatomic , strong) NSString *value;

@end
