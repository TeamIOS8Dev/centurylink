//
//  ResultPart1ViewController.h
//  CenturyLink
//
//  Created by Gau Uni on 11/10/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultField.h"

@protocol  ResultPart1ViewControllerDelegate<NSObject>

-(void)didResult1GoBack;
-(void)didResult1SeeFullModel: (ResultField *)result;

@end


@interface ResultPart1ViewController : UIViewController

//
@property (nonatomic, strong) NSMutableArray *dataObject;
@property (nonatomic, strong) NSMutableArray *groupAnswerStep2;
@property (nonatomic, assign) BOOL isHasPlan;

//Outlets

@property (nonatomic, weak) IBOutlet UILabel *label_HeaderText;
@property (nonatomic, weak) IBOutlet UILabel *label_Title;
@property (nonatomic, weak) IBOutlet UITextView *textView_OtherConsideration;
@property (nonatomic, weak) IBOutlet UITextView *textView_BusinessDriver;
@property (nonatomic, weak) IBOutlet UIWebView *webview_ResultContent;
@property (nonatomic, weak) IBOutlet UILabel *label_Description;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraint_Height_Header;
//Delegate
@property (nonatomic, weak) id <ResultPart1ViewControllerDelegate>delegate;
//------------------------------------------------------------------------------------------
/**   */
-(void)reloadData;
@end
