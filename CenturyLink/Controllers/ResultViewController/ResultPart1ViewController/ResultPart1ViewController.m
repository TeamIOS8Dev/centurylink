//
//  ResultPart1ViewController.m
//  CenturyLink
//
//  Created by Gau Uni on 11/10/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "ResultPart1ViewController.h"
#import "Common.h"
#import "ResultProgress.h"

#import "AppDelegate.h"

@interface ResultPart1ViewController ()
@property (nonatomic, strong)NSString *hybridText;

@property (nonatomic, weak) IBOutlet UIButton *btn_Back;
@property (nonatomic, weak) IBOutlet UIButton *btn_SeeModel;
@property (nonatomic, strong) ResultField *resultField;
@end

static NSString *text_WithPlan  = @"Here are the transformational hybrid IT choices that best fulfill your plan";
static NSString *text_NoPlan  = @"Based upon industry best practices and CenturyLink's experience, here are the transformational hybrid IT choices you should consider";

@implementation ResultPart1ViewController

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
//    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    NSString *pathToContentHTMLFile = [[NSBundle mainBundle] pathForResource:@"contentTemplate" ofType:@"html"];
    NSString *htmlContentString = [NSString stringWithContentsOfFile: pathToContentHTMLFile encoding:NSUTF8StringEncoding error:nil];
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [_webview_ResultContent loadHTMLString:htmlContentString baseURL:baseURL];
    
    NSString * jsCallBack = @"window.getSelection().removeAllRanges();";
    [_webview_ResultContent stringByEvaluatingJavaScriptFromString:jsCallBack];
    _btn_Back.exclusiveTouch = _btn_SeeModel.exclusiveTouch = YES;
    
    _label_HeaderText.font = kFont_UniversLTStd(60/2);
    _label_HeaderText.textColor = [UIColor blackColor];
    if (_isHasPlan) {
        _label_HeaderText.text = text_WithPlan;
    }
    else
    {
        _label_HeaderText.text = text_NoPlan;
    }
    
    _label_Title.font = kFont_UniversLTStd_Bold(36.5/2);
    _label_Title.textColor = [UIColor blackColor];

    _textView_OtherConsideration.font =  kFont_UniversLTStd(28/2);
    _textView_OtherConsideration.textColor = [Common resultContentColor];
    _textView_OtherConsideration.editable = NO;
    _textView_OtherConsideration.selectable = NO;
    
    _textView_BusinessDriver.font =  kFont_UniversLTStd(28/2);
    _textView_BusinessDriver.textColor = [Common resultContentColor];
    _textView_BusinessDriver.editable = NO;
    _textView_BusinessDriver.selectable = NO;
    _textView_BusinessDriver.showsVerticalScrollIndicator = NO;
    
    // Footer
    _label_Description.font = kFont_UniversLTStd_Italic(12);
    _label_Description.alpha = 0;
    [Common setLineHeight:kLineHeightOf_Lb_Footer for_UILabel:_label_Description];
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    
//    _webview_ResultContent.scrollView.showsVerticalScrollIndicator = NO;

}

//------------------------------------------------------------------------------------------
/**   */
-(void)reloadData
{
    ResultProgress *resultProgress = [[ResultProgress alloc] init];
    //    _dataObject = [NSMutableArray arrayWithArray: @[@"b",@"e",@[@"h"]]];
    _resultField = [resultProgress returnResultWhenUserSelectedAnswer:_dataObject andArrayAnswerStep2:_groupAnswerStep2 andIsPlanOrNot:_isHasPlan];

    NSString *titleString = _resultField.resultTitle;
    NSString *businessDriverAddressedString = [_resultField.arrayAnswerInStep2 componentsJoinedByString:@""];
    NSString *otherConsiderationString = _resultField.otherConsiderations;
    
    
    NSString *pathToContentHTMLFile = [[NSBundle mainBundle] pathForResource:@"contentTemplate" ofType:@"html"];
    NSString *htmlContentString = [NSString stringWithContentsOfFile: pathToContentHTMLFile encoding:NSUTF8StringEncoding error:nil];
    
    if (businessDriverAddressedString.length != 0) {
        htmlContentString = [htmlContentString stringByReplacingOccurrencesOfString:@"OtherBusinessDriver" withString:businessDriverAddressedString];
    }
    
    if (titleString.length != 0) {
        htmlContentString = [htmlContentString stringByReplacingOccurrencesOfString:@"TitleContent" withString:titleString];
    }
    
    if (otherConsiderationString.length != 0) {
        htmlContentString = [htmlContentString stringByReplacingOccurrencesOfString:@"OtherContent" withString:otherConsiderationString];
    }
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [_webview_ResultContent loadHTMLString:htmlContentString baseURL:baseURL];

}


//------------------------------------------------------------------------------------------
/**   */
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(copy:) ||
        action == @selector(paste:)||
        action == @selector(cut:))
    {
        return NO;
    }
    return [super canPerformAction:action withSender:sender];
}

//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/


#pragma mark -
#pragma mark Actions
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_GoBack_Pressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didResult1GoBack)]) {
        [_delegate didResult1GoBack];
    }
}


//------------------------------------------------------------------------------------------
/**   */
-(IBAction)btn_SeeFullModel_Pressed:(id)sender
{
    if ([_delegate respondsToSelector:@selector(didResult1SeeFullModel:)]) {
        [_delegate didResult1SeeFullModel: _resultField];
    }
}

/*************************************************************************************************************************/

@end
