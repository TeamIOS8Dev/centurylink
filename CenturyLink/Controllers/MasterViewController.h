//
//  MasterViewController.h
//  CenturyLink
//
//  Created by MacMini  on 11/6/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIButtonForMenu.h"

@interface MasterViewController : UIViewController
{
    // Controls
    __weak IBOutlet UILabel *lb_Logo;
    __weak IBOutlet UILabel *lb_Logo_SmallText;
    
    
    // Menu Buttons
    __weak IBOutlet UIButtonForMenu *btn_Start;
    __weak IBOutlet UIButtonForMenu *btn_BussinessDrivers;
    __weak IBOutlet UIButtonForMenu *btn_Current;
    __weak IBOutlet UIButtonForMenu *btn_Management;
    __weak IBOutlet UIButtonForMenu *btn_IT_Infrastructure;
    __weak IBOutlet UIButtonForMenu *btn_ComputeModels;
    __weak IBOutlet UIButtonForMenu *btn_Results;
   
    
    // ScrollerView_Content
    __weak IBOutlet UIScrollView *scrollerView_Content;
}

+ (MasterViewController *)shared;

@end
