//
//  CurrentViewController.m
//  CenturyLink
//
//  Created by Tan Duong Nhat on 1/19/15.
//  Copyright (c) 2015 CO2. All rights reserved.
//

#import "CurrentViewController.h"
#import "Common.h"

@interface CurrentViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation CurrentViewController


#pragma mark - Init View

/***************************************************************/

//-------------------------------------------------------
-(void) prepareForLayout
{
    lb_Title.font = kFont_UniversLTStd_Light(30);
    
    // Checkbox
    UIFont *font_ButtonCheckBox = kFont_UniversLTStd_BoldCn(23);
    
    btn_checkBox1.exclusiveTouch = YES;
    btn_checkBox1.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox2.exclusiveTouch = YES;
    btn_checkBox2.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox3.exclusiveTouch = YES;
    btn_checkBox3.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox4.exclusiveTouch = YES;
    btn_checkBox4.titleLabel.font = font_ButtonCheckBox;
   
    
    // Footer
    btn_Back.exclusiveTouch = YES;
    btn_Next.exclusiveTouch = YES;
}


//-------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self prepareForLayout];
    
    // Get AppDelegate
    appDelegate = [Common getAppDelegate];
}


//-------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */
    
/***************************************************************/




#pragma mark - Ulities

/***************************************************************/

//-------------------------------------------------------
-(void) setStatusDefault_And_ClearCurrentAnswer
{
    [btn_checkBox1 setSelected:NO];
    [btn_checkBox2 setSelected:NO];
    [btn_checkBox3 setSelected:NO];
    [btn_checkBox4 setSelected:NO];
   
    
    // GetAnswer
    [self getAnswer];
}


//-------------------------------------------------------
-(void) getAnswer
{
    NSString *s_ChosenAnswer = @"";
    
    if (btn_checkBox1.isSelected)
    {
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox1.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox1.titleLabel.text];
    }
    
    if (btn_checkBox2.isSelected)
    {
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox2.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox2.titleLabel.text];
    }
    
    if (btn_checkBox3.isSelected)
    {
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox3.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox3.titleLabel.text];
    }
    
    if (btn_checkBox4.isSelected)
    {
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox4.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox4.titleLabel.text];
    }
    
    // Save Results
    appDelegate.queueResultField.businessDriver2 = s_ChosenAnswer;
    
    
//    NSLog(@"hasPlan_step2.selectedAnswer = %@", s_ChosenAnswer);
    
    
    // Changed Value
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(currentViewControllerDelegate_ChangedValue)])
    {
        [self.delegate currentViewControllerDelegate_ChangedValue];
    }
}

/***************************************************************/





#pragma mark - IBActions

/***************************************************************/

//-------------------------------------------------------
- (IBAction)btn_CheckBox_Tapped:(id)sender {
    
    UIButton *button = (UIButton *) sender;
    [button setSelected:!button.isSelected];
    
    [self getAnswer];
}


//-------------------------------------------------------
- (IBAction)btn_Back_Tapped:(id)sender {
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(currentViewControllerDelegate_GoBack)])
    {
        [self.delegate currentViewControllerDelegate_GoBack];
    }
}


//-------------------------------------------------------
- (IBAction)btn_Next_Tapped:(id)sender {
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(currentViewControllerDelegate_GoNext)])
    {
        [self.delegate currentViewControllerDelegate_GoNext];
    }
}

/***************************************************************/




@end
