//
//  CurrentViewController.h
//  CenturyLink
//
//  Created by Tan Duong Nhat on 1/19/15.
//  Copyright (c) 2015 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CurrentViewControllerDelegate <NSObject>

-(void) currentViewControllerDelegate_GoNext;

-(void) currentViewControllerDelegate_GoBack;

-(void) currentViewControllerDelegate_ChangedValue;

@end




@interface CurrentViewController : UIViewController
{
    __weak IBOutlet UILabel *lb_Title;
    
    // CheckBox
    __weak IBOutlet UIButton *btn_checkBox1;
    __weak IBOutlet UIButton *btn_checkBox2;
    __weak IBOutlet UIButton *btn_checkBox3;
    __weak IBOutlet UIButton *btn_checkBox4;
    
    // Footer
    __weak IBOutlet UIButton *btn_Back;
    __weak IBOutlet UIButton *btn_Next;
}


// Properties
@property(nonatomic, weak) id<CurrentViewControllerDelegate> delegate;


// Methods
-(void) setStatusDefault_And_ClearCurrentAnswer;


@end
