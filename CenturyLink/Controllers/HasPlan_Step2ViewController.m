//
//  Step2ViewController.m
//  CenturyLink
//
//  Created by MacMini  on 11/6/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import "HasPlan_Step2ViewController.h"
#import "Common.h"
#import "Model.h"
#import "NSString+HTML.h"


@interface HasPlan_Step2ViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation HasPlan_Step2ViewController

// Properties
@synthesize delegate;

@synthesize selectedAnswer;



#pragma mark - Init View

/***************************************************************/

//-------------------------------------------------------
-(void) prepareForLayout
{
    lb_Title.font = kFont_UniversLTStd_Light(30);
    
    // Checkbox
    UIFont *font_ButtonCheckBox = kFont_UniversLTStd_BoldCn(23);
    
    btn_checkBox1.exclusiveTouch = YES;
    btn_checkBox1.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox2.exclusiveTouch = YES;
    btn_checkBox2.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox3.exclusiveTouch = YES;
    btn_checkBox3.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox4.exclusiveTouch = YES;
    btn_checkBox4.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox5.exclusiveTouch = YES;
    btn_checkBox5.titleLabel.font = font_ButtonCheckBox;
    
    btn_checkBox6.exclusiveTouch = YES;
    btn_checkBox6.titleLabel.font = font_ButtonCheckBox;
    
    
    // Footer
    btn_Back.exclusiveTouch = YES;
    btn_Next.exclusiveTouch = YES;
}


//-------------------------------------------------------
-(void) loadData
{
    // Get AppDelegate
    appDelegate = [Common getAppDelegate];
    
    // Load List Options
    BusinessDriverAddressedModel *model = [BusinessDriverAddressedModel new];
    NSMutableArray *list_Options = [model getAllRowsInTableBusinessDriverAddressed];
    model = nil;
    
    NSMutableDictionary *list_Answers = [NSMutableDictionary new];
    
    for (BusinessDriverAddressedField *item in list_Options)
    {
        // Save and Remove HTML
        NSString *decodeHTML = [item.valueBusinessDriverAddressed stringByConvertingHTMLToPlainText];
        
//        NSLog(@"s = %@", decodeHTML);
        
        list_Answers[item.keyBusinessDriverAddressed] = decodeHTML;
    }
    
    list_Options = nil;
    
    
    // Set Title for Button
    [btn_checkBox1 setTitle:list_Answers[@"1"] forState:UIControlStateNormal];
    [btn_checkBox2 setTitle:list_Answers[@"2"] forState:UIControlStateNormal];
    [btn_checkBox3 setTitle:list_Answers[@"3"] forState:UIControlStateNormal];
    [btn_checkBox4 setTitle:list_Answers[@"4"] forState:UIControlStateNormal];
    [btn_checkBox5 setTitle:list_Answers[@"5"] forState:UIControlStateNormal];
    [btn_checkBox6 setTitle:list_Answers[@"6"] forState:UIControlStateNormal];
    
    list_Answers = nil;
}


//-------------------------------------------------------
- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self prepareForLayout];
    
    [self loadData];
}


//-------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/***************************************************************/




#pragma mark - Ulities

/***************************************************************/

//-------------------------------------------------------
-(void) setStatusDefault_And_ClearCurrentAnswer
{
    [btn_checkBox1 setSelected:NO];
    [btn_checkBox2 setSelected:NO];
    [btn_checkBox3 setSelected:NO];
    [btn_checkBox4 setSelected:NO];
    [btn_checkBox5 setSelected:NO];
    [btn_checkBox6 setSelected:NO];
   
    
    // GetAnswer
    [self getAnswer];
}


//-------------------------------------------------------
-(void) getAnswer
{
    if (!selectedAnswer)
    {
        selectedAnswer = [NSMutableArray new];
    }
    
    [selectedAnswer removeAllObjects];
    
    NSString *s_index = @"";
    
    NSString *s_ChosenAnswer = @"";
   
    if (btn_checkBox1.isSelected)
    {
        s_index = [@(btn_checkBox1.tag) stringValue];
        [selectedAnswer addObject:s_index];
        
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox1.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox1.titleLabel.text];
    }
    
    if (btn_checkBox2.isSelected)
    {
        s_index = [@(btn_checkBox2.tag) stringValue];
        [selectedAnswer addObject:s_index];
        
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox2.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox2.titleLabel.text];
    }
    
    if (btn_checkBox3.isSelected)
    {
        s_index = [@(btn_checkBox3.tag) stringValue];
        [selectedAnswer addObject:s_index];
        
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox3.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox3.titleLabel.text];
    }
    
    if (btn_checkBox4.isSelected)
    {
        s_index = [@(btn_checkBox4.tag) stringValue];
        [selectedAnswer addObject:s_index];
        
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox4.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox4.titleLabel.text];
    }
    
    if (btn_checkBox5.isSelected)
    {
        s_index = [@(btn_checkBox5.tag) stringValue];
        [selectedAnswer addObject:s_index];
        
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox5.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox5.titleLabel.text];
    }
    
    if (btn_checkBox6.isSelected)
    {
        s_index = [@(btn_checkBox6.tag) stringValue];
        [selectedAnswer addObject:s_index];
        
        s_ChosenAnswer = (s_ChosenAnswer.length == 0) ? btn_checkBox6.titleLabel.text : [NSString stringWithFormat:@"%@;%@", s_ChosenAnswer, btn_checkBox6.titleLabel.text];
    }
    
    btn_Next.enabled = (selectedAnswer.count > 0) ? YES : NO;
    
    
    // Save Results
    appDelegate.queueResultField.businessDriver1 = s_ChosenAnswer;
   
    
//    NSLog(@"hasPlan_step2.selectedAnswer = %@", s_ChosenAnswer);
    
    
    // Changed Value
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(hasPlan_Step2ViewControllerDelegate_ChangedValue)])
    {
        [self.delegate hasPlan_Step2ViewControllerDelegate_ChangedValue];
    }
}

/***************************************************************/




#pragma mark - IBActions

/***************************************************************/

//-------------------------------------------------------
- (IBAction)btn_CheckBox_Tapped:(id)sender {
    
    UIButton *button = (UIButton *) sender;
    [button setSelected:!button.isSelected];
    
    [self getAnswer];
}


//-------------------------------------------------------
- (IBAction)btn_Back_Tapped:(id)sender {
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(hasPlan_Step2ViewControllerDelegate_GoBack)])
    {
        [self.delegate hasPlan_Step2ViewControllerDelegate_GoBack];
    }
}


//-------------------------------------------------------
- (IBAction)btn_Next_Tapped:(id)sender {
    
    if (self.delegate
        && [self.delegate respondsToSelector:@selector(hasPlan_Step2ViewControllerDelegate_GoNext)])
    {
        [self.delegate hasPlan_Step2ViewControllerDelegate_GoNext];
    }
}

/***************************************************************/

@end
