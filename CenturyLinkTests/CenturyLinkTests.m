//
//  CenturyLinkTests.m
//  CenturyLinkTests
//
//  Created by MacMini  on 11/5/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "Model.h"
#import "ResultProgress.h"

#define kStringReportDescriptionNeedReplaceInHTML @"DetailReportTest"

@interface CenturyLinkTests : XCTestCase

//  all answer
@property (nonatomic , strong) NSArray *arrayAllAnswer;

//  step 2
@property (nonatomic , strong) NSMutableArray *arrayAllAnswerStep2;

//  step 3
@property (nonatomic , strong) NSArray *arrayAllAnswerStep3;

//  step 4
@property (nonatomic , strong) NSArray *arrayAllAnswerStep4;

//  step 5
@property (nonatomic , strong) NSArray *arrayAllAnswerStep5;

//  group answer
@property (nonatomic , strong) NSMutableSet *listGroupAnswer;


//  input answer Q&A Outcome FLow(with Plan)
@property (nonatomic , strong) NSArray *arrayAnswerReturnResult;

@property (nonatomic , strong) NSArray *arrayHybridITScenarioCombinationsResult;


//  OCMock
@property (nonatomic, strong) id ocMockPartialClass;

@property (nonatomic , strong) ResultProgress *resultProgress;

//  Cac tap hop con cua mot tap hop
@property (nonatomic , strong) NSMutableArray *arrayLoopAllAbility,*listChoiceAnswerStep5,*listChoiceAnswerHybridITScenarioCombinations,*listChoiceAnswerStep2;
@property (nonatomic, assign) int n,k,tag;


@end

@implementation CenturyLinkTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    //  ViewController
    _resultProgress = [ResultProgress new];
    
    //  init ocMock Partial
    _ocMockPartialClass = [OCMockObject partialMockForObject:_resultProgress];
    
    
    //  List All Answer
    //    _arrayAllAnswer = [NSArray arrayWithObjects:@"a",@"b",@"c",@"d", nil];
    
    //  step 2
//    BusinessDriverAddressedModel *businessDriverAddressedModel = [BusinessDriverAddressedModel new];
//    NSArray *listTemp = [businessDriverAddressedModel getAllRowsInTableBusinessDriverAddressed];
//    _arrayAllAnswerStep2 = [NSMutableArray new];
//    for (BusinessDriverAddressedField *businessDriverAddressedField in listTemp) {
//        [_arrayAllAnswerStep2 addObject:businessDriverAddressedField.valueBusinessDriverAddressed];
//    }
    _arrayAllAnswerStep2 = [NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
    
    //  step 3
    _arrayAllAnswerStep3 = [NSArray arrayWithObjects:@"a",@"b",@"c", nil];
    
    //  step 4
    _arrayAllAnswerStep4 = [NSArray arrayWithObjects:@"d",@"e",@"f", nil];
    
    //  step 5
    _arrayAllAnswerStep5 = [NSArray arrayWithObjects:@"g",@"h",@"i",@"j", nil];
    
    //  List Answer return result K
    _arrayAnswerReturnResult = [NSArray arrayWithObjects:@"a",@"c",@"d",@"f",@"g",@"j", nil];
    
    _arrayHybridITScenarioCombinationsResult = [NSArray arrayWithObjects:@"l",@"m",@"n",@"o", nil];
    
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    //  dealloc
    _resultProgress = nil;
    _arrayAllAnswerStep3 = nil;
    _arrayAllAnswerStep4 = nil;
    _arrayAllAnswerStep5 = nil;
    _arrayAnswerReturnResult = nil;
    
    _ocMockPartialClass = nil;
    
    [super tearDown];
}


- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}


/********************************************************************************/
#pragma mark - Liet kê tất cả các tập con của một tập hợp



/**
 * Cho vào mảng
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) print
{
    // in tổ hợp
    NSMutableArray *array = [NSMutableArray new];
    for(int i= 1; i <=_k;i++)
    {
        switch (_tag) {
            case 1:
                [array addObject:_arrayAllAnswerStep5[[_arrayLoopAllAbility[i] intValue] - 1]];
                break;
                
            case 2:
                [array addObject:_arrayHybridITScenarioCombinationsResult[[_arrayLoopAllAbility[i] intValue] - 1]];
                break;
                
            case 3:
                [array addObject:_arrayAllAnswerStep2[[_arrayLoopAllAbility[i] intValue] - 1]];
                break;
                
            default:
                break;
        }
        
    }
        
    //  add object
    switch (_tag) {
        case 1:
            [_listChoiceAnswerStep5 addObject:array];
            break;
            
        case 2:
            [_listChoiceAnswerHybridITScenarioCombinations addObject:array];
            break;
            
        case 3:
            [_listChoiceAnswerStep2 addObject:array];
            break;
            
        default:
            break;
    }
    
    
}



/**
 * Hàm quay lui
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)Try:(int)i
{
    
    for(int j =[_arrayLoopAllAbility[i-1] intValue] + 1; j <= _n-_k + i; j++) {
        _arrayLoopAllAbility[i] = [NSString stringWithFormat:@"%d",j];
        if(i==_k) {
            [self print];
        }
        else {
            [self Try:i+1];
        }
    }
}



/**
 * tat ca cac tap hop con cua tap hop N
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testLietKeTatCaTapConCuaTapN
{
    //  init
    _arrayLoopAllAbility = [NSMutableArray arrayWithCapacity:100];
    _listChoiceAnswerStep5 = [NSMutableArray arrayWithCapacity:100];
    _n = 4;
    [_arrayLoopAllAbility addObject:@"0"];
    _tag = 1;
    
    //  tong cac truong hop co the xay ra
    int sumAllAbility = 0;
    for (int i = 1; i <= _n; i++) {
        _k = i;
        [self Try:1];
        
        //  Tổ hợp chập b của a = a! / b! * (a-b)! với c = (a-b)
        int a = [self calculatorFactorial:_arrayAllAnswerStep5.count];
        int b = [self calculatorFactorial:i];
        int c = [self calculatorFactorial:_arrayAllAnswerStep5.count - i];
        sumAllAbility += a/(b * c);
    }
    
    //
    XCTAssertEqual(sumAllAbility, _listChoiceAnswerStep5.count , @"Error");
    
    
}



/**
 * Tính giai thừa
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(int)calculatorFactorial:(NSUInteger)n
{
    int factorial = 1;
    for (int i = 1; i <= n; i++) {
        factorial *= i;
    }
    
    return factorial;
}


/********************************************************************************/



/********************************************************************************/
#pragma mark - Q&A Outcome FLow(with Plan)


-(void) testNumber
{
    NSMutableArray *array = [NSMutableArray new];
    while (array.count < 3) {
        for (NSString *answerStep5 in _arrayAllAnswerStep5) {
            if (![array containsObject:answerStep5]) {
                [array addObject:answerStep5];
                break;
            }
            
        }
    }
    
    XCTAssertEqual(array.count, 3);
}









-(void) testGetAllGroupAnswer
{
    _listGroupAnswer = [NSMutableSet new];
    NSMutableArray *listAnswer = [NSMutableArray new];
    
    [self testLietKeTatCaTapConCuaTapN];
    
    
    NSMutableArray *listGroupAnswerTemp = [NSMutableArray new];
    for (NSString *answerStep3 in _arrayAllAnswerStep3) {
        for (NSString *answerStep4 in _arrayAllAnswerStep4) {
            for (NSArray *groupAnswerStep5 in _listChoiceAnswerStep5) {
                [listAnswer addObject:answerStep3];
                [listAnswer addObject:answerStep4];
                [listAnswer addObject:groupAnswerStep5];
                [listGroupAnswerTemp addObject:[listAnswer mutableCopy]];
                [listAnswer removeAllObjects];
                
            }
        }
    }
    
    
    
    XCTAssertEqual(listGroupAnswerTemp.count, _arrayAllAnswerStep3.count * _arrayAllAnswerStep4.count * _listChoiceAnswerStep5.count,@"Successfull");
    
    //  filter array with object not same
    [_listGroupAnswer addObjectsFromArray:listGroupAnswerTemp];
}


-(void) testValidateInputAnswer
{
    // test array answer
    NSArray *array = [NSArray new];
    
    //  ( câu 1 chọn b thì câu 2 bắt buột phải d)
    //    array = @[@"b",@"d",@[@"h"]]; // failed
    array = @[@"b",@"e",@[@"h"]]; // true
    XCTAssertTrue([_ocMockPartialClass validateListAnswerPass:array],@"Error");
    
    //  ( câu 1 chọn b thì câu 3 không đc chọn g)
    //  array = @[@"b",@"e",@[@"g"]]; // failed
    array = @[@"b",@"e",@[@"h"]]; // true
    XCTAssertTrue([_ocMockPartialClass validateListAnswerPass:array],@"Error");
    
    //  ( câu 1 chọn a thì câu 3 không đc chọn h)
    //  array = @[@"a",@"e",@[@"h"]]; // failed
    array = @[@"a",@"e",@[@"i"]]; // true
    XCTAssertTrue([_ocMockPartialClass validateListAnswerPass:array],@"Error");
    
    //  ( câu 1 chọn d thì câu 3 không đc chọn i)
    //  array = @[@"a",@"d",@[@"i"]]; // failed
    array = @[@"a",@"d",@[@"g"]]; // true
    XCTAssertTrue([_ocMockPartialClass validateListAnswerPass:array],@"Error");
    
    //  ( câu 1 chọn d thì câu 3 không đc chọn j)
    //  array = @[@"a",@"d",@[@"j"]]; // failed
    array = @[@"a",@"d",@[@"g"]]; // true
    XCTAssertTrue([_ocMockPartialClass validateListAnswerPass:array],@"Error");
    
    //    [self testGetAllGroupAnswer];
    
    //    for (NSArray *groupAnswer in _listGroupAnswer) {
    //        XCTAssertTrue([_ocMockPartialClass validateListAnswerPass:groupAnswer]);
    //    }
    
    
    
}



/**
 * test
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testResultReturn
{
    //  Plan
    [self testWithPlanOrNot:YES];
    
    //  No Plan
    [self testWithPlanOrNot:NO];
}




/**
 * Group Answer Step 2
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(NSString *)valueGroupAnswerStep2:(NSMutableArray *)groupAnswerStep2
{
    BusinessDriverAddressedModel *businessDriverAddressedModel = [BusinessDriverAddressedModel new];
    NSArray *listBusinessDriverAddressed = [businessDriverAddressedModel getAllRowsInTableBusinessDriverAddressed];
    
    NSString *stringBusinessDriverAddressedValue = @"";
    for (NSString *key in groupAnswerStep2) {
        for (BusinessDriverAddressedField *businessDriverAddressedField in listBusinessDriverAddressed) {
            if ([[businessDriverAddressedField.keyBusinessDriverAddressed lowercaseString] isEqualToString:[key lowercaseString]])
            {
                stringBusinessDriverAddressedValue = [stringBusinessDriverAddressedValue stringByAppendingString:businessDriverAddressedField.valueBusinessDriverAddressed];
            }
        }
    }
    
    return stringBusinessDriverAddressedValue;
}

/**
 * <#method description#>
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testWithPlanOrNot:(BOOL)isPlan
{
    // lấy tất cả trường hợp
    [self testGetAllGroupAnswer];
    
    NSString *stringHTML = @"";
    NSMutableArray *result = [NSMutableArray new];
    
    // get a URL reference (PREFERRED)
    NSURL *myURL = [[NSBundle mainBundle]URLForResource:@"TemplateHTML" withExtension:@"html"];
    
    // read the file from a URL (PREFERRED)
    stringHTML = [[NSString alloc]initWithContentsOfURL:myURL encoding:NSUTF8StringEncoding error:nil];
    
    
    //    NSMutableDictionary *dicSumup = [NSMutableDictionary new];
    NSString *stringReportTest = @"";
        for (NSArray *groupAnswer in _listGroupAnswer) {
            ResultField *resultFinal = [_ocMockPartialClass returnResultWhenUserSelectedAnswer:(NSMutableArray *)groupAnswer andArrayAnswerStep2:(NSMutableArray *)@[] andIsPlanOrNot:isPlan];
            
            if (resultFinal) {
                [result addObject:resultFinal];
                
                
                
                //  group answer step 2
                NSString *stringBusinessDriverAddressedValue = @"";//[self valueGroupAnswerStep2:groupAnswerStep2];
 
                //  sumup
                stringReportTest = [stringReportTest stringByAppendingString:[NSString stringWithFormat:@"<tr><td>%@%@</td><td>%@</td><td>%@</td><td>%@</td><td>%@</td></tr>",groupAnswer,stringBusinessDriverAddressedValue,resultFinal.choice,resultFinal.resultTitle,resultFinal.resultBusinessDriverAddressed,resultFinal.HybridITScenarioCombinationsValue]];
            }
            
        }
    
    // export report to html
    stringHTML = [stringHTML stringByReplacingOccurrencesOfString:kStringReportDescriptionNeedReplaceInHTML withString:stringReportTest];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = @"";
    //  Plan
    if (isPlan) {
        fileName = [NSString stringWithFormat:@"%@/ReportWithPlan.html",
                    documentsDirectory];
        NSLog(@" sum plan : %lu ",(unsigned long)[result count]);
    }
    //  No Plan
    else
    {
        fileName = [NSString stringWithFormat:@"%@/ReportWithNoPlan.html",
                    documentsDirectory];
        NSLog(@" sum no plan : %lu ",(unsigned long)[result count]);
    }
    
    
    NSLog(@" path : %@ ",fileName);
    
    //save content to the documents directory
    [stringHTML writeToFile:fileName
                 atomically:NO
                   encoding:NSStringEncodingConversionAllowLossy
                      error:nil];
    
//    XCTAssertEqual(result.count, _listGroupAnswer.count * _listChoiceAnswerStep2.count);
    
}




///**
// * Validate Result Hybrid IT Scenario Combinations
// *
// * @param <#param#>
// * @returns <#returns#>
// */
//-(void)testHybridITScenarioCombinationsResult
//{
//    NSArray *array = [_ocMockPartialClass returnResultHybridITScenarioCombinations:@[@"l",@"m",@"n",@"g"]];
//    
//    XCTAssertEqual(array.count, 0);
//    
//    
//    
//}

/********************************************************************************/


/********************************************************************************/
#pragma mark - Hybrid IT Scenario Combinations



/**
 * tat ca cac tap hop con cua tap hop Hybrid IT Scenario Combinations
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testLietKeTatCaTapConCuaTapHybridITScenarioCombinations
{
    //  init
    _arrayLoopAllAbility = [NSMutableArray arrayWithCapacity:100];
    _listChoiceAnswerHybridITScenarioCombinations = [NSMutableArray arrayWithCapacity:100];
    _n = 4;
    _tag = 2;
    [_arrayLoopAllAbility addObject:@"0"];
    
    //  tong cac truong hop co the xay ra
    int sumAllAbility = 0;
    for (int i = 1; i <= _n; i++) {
        _k = i;
        [self Try:1];
        
        //  Tổ hợp chập b của a = a! / b! * (a-b)! với c = (a-b)
        int a = [self calculatorFactorial:_arrayHybridITScenarioCombinationsResult.count];
        int b = [self calculatorFactorial:i];
        int c = [self calculatorFactorial:_arrayHybridITScenarioCombinationsResult.count - i];
        sumAllAbility += a/(b * c);
    }
    
    //
    XCTAssertEqual(sumAllAbility, _listChoiceAnswerHybridITScenarioCombinations.count , @"Error");
    
    
}



/**
 * Hybrid IT Scenario Combinations Specific condition
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testHybridITScenarioCombinationsResultSpecificCondition
{
    //  test specific condition
    _listChoiceAnswerHybridITScenarioCombinations = [NSMutableArray new];
    [_listChoiceAnswerHybridITScenarioCombinations addObject:@[@"l",@"m",@"o"]];
    
    _listChoiceAnswerHybridITScenarioCombinations = [NSMutableArray arrayWithArray:@[@[@"l",@"m",@"n",@"o",@"k"]
                                               ,@[@"m",@"n",@"o",@"k"]
                                               ,@[@"n",@"o",@"k"]
                                               ,@[@"l",@"m",@"n",@"k"]
                                               ,@[@"l",@"m",@"k"]
                                               ,@[@"m",@"n",@"k"]
                                               ,@[@"o",@"l",@"k"]
                                               ,@[@"n",@"l",@"k"]
                                               ,@[@"m",@"l",@"k"]
                                               ,@[@"m",@"o",@"k"]
                                               ,@[@"l",@"n",@"o",@"k"]
                                               ,@[@"l",@"k"]
                                               ,@[@"m",@"k"]
                                               ,@[@"n",@"k"]
                                               ,@[@"o",@"k"]]];
    
    //  Plan
    [self testHybridITScenarioCombinations:YES];
    
    //  No Plan
    [self testHybridITScenarioCombinations:NO];
}



/**
 * Hybrid IT Scenario Combinations all ability
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testHybridITScenarioCombinationsResultAllAbility
{
    // lấy tất cả trường hợp
    [self testLietKeTatCaTapConCuaTapHybridITScenarioCombinations];
    
    //  Plan
    [self testHybridITScenarioCombinations:YES];
    
    //  No Plan
    [self testHybridITScenarioCombinations:NO];
}


/**
 * <#method description#>
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testHybridITScenarioCombinations:(BOOL)isPlan
{

    NSString *stringHTML = @"";
    NSString *stringReportTest = @"";
    NSMutableArray *result = [NSMutableArray new];
    
    // get a URL reference (PREFERRED)
    NSURL *myURL = [[NSBundle mainBundle]URLForResource:@"TemplateHTML" withExtension:@"html"];
    
    // read the file from a URL (PREFERRED)
    stringHTML = [[NSString alloc]initWithContentsOfURL:myURL encoding:NSUTF8StringEncoding error:nil];
    
    
    
    //    NSMutableDictionary *dicSumup = [NSMutableDictionary new];
    for (NSArray *groupAnswer in _listChoiceAnswerHybridITScenarioCombinations) {
        HybridITScenarioCombinationsField *hybridITScenarioCombinationsField = [_ocMockPartialClass returnResultHybridITScenarioCombinations:groupAnswer];
        
        //  sumup
        
        if (hybridITScenarioCombinationsField == nil) {
            //[dicSumup setObject:@"No Result" forKey:groupAnswer];
        }
        else
        {
            [result addObject:hybridITScenarioCombinationsField];
            //[dicSumup setObject:arrayResult forKey:groupAnswer];
            
            //  choice option result
            
            NSString *arrayChoice = [NSString stringWithFormat:@"%@",hybridITScenarioCombinationsField.condition];
            NSString *result = [NSString stringWithFormat:@"%@",hybridITScenarioCombinationsField.value];
            stringReportTest = [stringReportTest stringByAppendingString:[NSString stringWithFormat:@"<tr><td>%@</td><td>%@</td></tr>",arrayChoice,result]];
            
            
        }
        
        
    }
    
    //
    
    // export report to html
    stringHTML = [stringHTML stringByReplacingOccurrencesOfString:kStringReportDescriptionNeedReplaceInHTML withString:stringReportTest];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = @"";
    //  Plan
    if (isPlan) {
        fileName = [NSString stringWithFormat:@"%@/HybridITScenarioCombinationsWithPlan.html",
                    documentsDirectory];
        NSLog(@" sum plan : %lu ",(unsigned long)[result count]);
    }
    //  No Plan
    else
    {
        fileName = [NSString stringWithFormat:@"%@/HybridITScenarioCombinationsWithNoPlan.html",
                    documentsDirectory];
        NSLog(@" sum no plan : %lu ",(unsigned long)[result count]);
    }
    
    NSLog(@" path : %@ ",fileName);
    
    //save content to the documents directory
    [stringHTML writeToFile:fileName
                 atomically:NO
                   encoding:NSStringEncodingConversionAllowLossy
                      error:nil];
    
    XCTAssertEqual(result.count, 15 , @"Error");
    
}



/********************************************************************************/


/********************************************************************************/
#pragma mark - Step 2


/**
 * tat ca cac tap hop con cua tap hop Hybrid IT Scenario Combinations
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void) testLietKeTatCaTapConCuaTapCauTraLoiStep2
{
    //  init
    _arrayLoopAllAbility = [NSMutableArray arrayWithCapacity:100];
    _listChoiceAnswerStep2 = [NSMutableArray arrayWithCapacity:100];
    _n = 7;
//    _k = 2;
    _tag = 3;
    [_arrayLoopAllAbility addObject:@"0"];
    
    //  tong cac truong hop co the xay ra
    int sumAllAbility = 0;
    for (int i = 1; i <= _n; i++) {
        if (i <= 2)//  chinh hop chap 2 cua 7
        {
            _k = i;
            [self Try:1];
            
            //  Tổ hợp chập b của a = a! / b! * (a-b)! với c = (a-b)
            int a = [self calculatorFactorial:_arrayAllAnswerStep2.count];
            int b = [self calculatorFactorial:i];
            int c = [self calculatorFactorial:_arrayAllAnswerStep2.count - i];
            sumAllAbility += a/(b * c);
        }
        
    }
    
    //
    XCTAssertEqual(sumAllAbility, _listChoiceAnswerStep2.count , @"Error");
    
    
}



/********************************************************************************/



@end
