//
//  SubmitFormTests.m
//  CenturyLink
//
//  Created by Thien Thanh on 11/18/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "QueueResultField.h"
#import <OCMock/OCMock.h>
#import "Model.h"
#import <UIKit/UIKit.h>
#import "SaveResultToQueue.h"
#import "ResultField.h"
#import "LoadAllRequest.h"
#import <objc/runtime.h>
#import "SubmitData.h"
#import "Common.h"
#import "StartViewController.h"



@interface SubmitFormTests : XCTestCase<LoadAllRequestDelegate>

@property (nonatomic, strong) LoadAllRequest *loadAllRequest;
@end

@implementation SubmitFormTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _loadAllRequest = [LoadAllRequest new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}


/********************************************************************************/
#pragma mark - Test Save Database


-(void) testSaveResultToQueue
{
    //  Given
    QueueResultField *queueResultField = [QueueResultField new];
    
    //  save data to queue
    //  Personal Info
    queueResultField.firstName = @"1";
    queueResultField.lastName  = @"2";
    queueResultField.email     = @"3";
    queueResultField.lastName  = @"4";
    
    //  result field
    queueResultField.otherConsiderations = @"5";
    queueResultField.resultBusinessDriverAddressed = @"6";
    queueResultField.isPlan = @"7";
    queueResultField.HybridITScenarioCombinationsValue = @"8";
    
    //  When
    QueueResultModel *queueResultModel = [QueueResultModel new];
//    QueueResultField *queueResultFieldFinal = [queueResultModel createRowInTableQueueResultReturnRowIdLastInsert:queueResultField];
    
    // Then
//    XCTAssertEqualObjects(queueResultField.firstName, queueResultFieldFinal.firstName , "@Successfull");
    

    
}



/********************************************************************************/


/********************************************************************************/
#pragma mark - Test Submit form to server


-(void)testCreateTestDataInDatabase
{
    int numberLoop = 15;
    
    for (int i = 1; i <= numberLoop; i++)
    {
        //  Given
        QueueResultField *queueResultField = [QueueResultField new];
        queueResultField.businessPhone = @"1234567890";
        queueResultField.businessPhone = [queueResultField.businessPhone stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.company = @"Kabi Company";
        queueResultField.company = queueResultField.company = [queueResultField.company stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        
        queueResultField.country = @"Viet Nam";
        queueResultField.country = [queueResultField.country stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        //  FIXME: sua mail o day
        queueResultField.email = @"chibong";
        queueResultField.email = [queueResultField.email stringByAppendingString:[NSString stringWithFormat:@"%i@yahoo.com",i]];
        if (i % 2 == 0) {
            queueResultField.email = @"chihuỳnh@carbon8.com";
        }
        else
            queueResultField.email = @"testmail0511@yahoo.com";
        
        queueResultField.firstName = @"Thanh";
        queueResultField.firstName = [queueResultField.firstName stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        
        queueResultField.lastName = @"Thien";
        queueResultField.lastName = [queueResultField.lastName stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        if (i % 2 == 0) {
            queueResultField.lastName = @",.,.,/";
        }
        else
            queueResultField.lastName = @"Kabi Company";
        
        queueResultField.jobTitle = @"Dev iOS";
        queueResultField.jobTitle = [queueResultField.jobTitle stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        
        queueResultField.zipCode = @"57000";
        queueResultField.zipCode = [queueResultField.zipCode stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.promoCode = @"promocode";
        queueResultField.promoCode = [queueResultField.promoCode stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.planStatus = @"Plan status";
        queueResultField.planStatus = [queueResultField.planStatus stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.businessDriver1 = @"Business driver 1";
        queueResultField.businessDriver1 = [queueResultField.businessDriver1 stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.businessDriver2 = @"Business driver 2";
        queueResultField.businessDriver2 = [queueResultField.businessDriver2 stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.management = @"Management – internal to 3rd party";
        queueResultField.management = [queueResultField.management stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.infrastructureLocation = @"Infrastructure location – on-premise to 3rd party";
        queueResultField.infrastructureLocation = [queueResultField.infrastructureLocation stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.computeModels = @"Test Compute";
        queueResultField.computeModels = [queueResultField.computeModels stringByAppendingString:[NSString stringWithFormat:@"%i",i]];
        
        queueResultField.outputPDF = [NSString stringWithFormat:@"%i",(i % 15 + 1)];
        
        queueResultField.valueCheckbox = @"1";
        queueResultField.salesRepCheckbox = @"0";
        
        //  When
        QueueResultModel *queueResultModel = [QueueResultModel new];
        BOOL isSuccess = [queueResultModel createRowInTableQueueResult:queueResultField];
        
        //  Then
        XCTAssertTrue(isSuccess);
    }
    
}



/**
 * test many time
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)testBulkSubmit
{
    //  Given
    [self testCreateTestDataInDatabase];
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory  inDomains:NSUserDomainMask] lastObject]);
    
    //  When
    StartViewController *startViewController = [StartViewController new];
    [startViewController action_submitAllDataInQueue:self];
    
    //  Then
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:50.0]];
}




/**
 * submit to eloqua
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)testSubmitFormToEloquaServer
{
    //  Given
    QueueResultField *queueResultField = [QueueResultField new];
    
    queueResultField.businessPhone = @"1234567890";
    queueResultField.company = @"thanh thien GGG";
    queueResultField.country = @"Viet Nam";
    queueResultField.email = @"kabigon1991@yahoo.com";
    queueResultField.firstName = @"Thanh";
    queueResultField.lastName = @"Thien";
    queueResultField.jobTitle = @"Dev iOS";
    queueResultField.zipCode = @"57000";
    queueResultField.promoCode = @"promocode";
    queueResultField.planStatus = @"Plan status";
    queueResultField.businessDriver1 = @"Business driver 1";
    queueResultField.businessDriver2 = @"Business driver 2";
    queueResultField.management = @"Management – internal to 3rd party";
    queueResultField.infrastructureLocation = @"Infrastructure location – on-premise to 3rd party";
    queueResultField.computeModels = @"Test Compute 11";
    queueResultField.outputPDF = @"1";
    queueResultField.valueCheckbox = @"1";
    queueResultField.salesRepCheckbox = @"0";
    
    //  delegate
    id mydelegatemock = [OCMockObject mockForProtocol:@protocol(LoadAllRequestDelegate)];
    _loadAllRequest.delegate = mydelegatemock;
    [[mydelegatemock expect] didSucessfullRequestPostData:[OCMArg any]];
    
    
    //  When
//    [SaveResultToQueue saveResultToQueue:personalInfoField andResultData:resultField andISNeedToSave:NO];
    NSDictionary *dicData = [SubmitData createObjectSubmit:queueResultField];
    [_loadAllRequest requestPostDataFromServer:kSubmitFormData andParameters:dicData andTag:2];

    
    //  Then
    // we need to wait for real result
//    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1.0]];
//    [self waitForExpectationsWithTimeout:0.5 handler:nil];
    [mydelegatemock verifyWithDelay:10.0];
    [(OCMockObject*)mydelegatemock verify];
    
    
    
    
}



/********************************************************************************/


/********************************************************************************/
#pragma mark - Test Input data submit




-(void)testInputSubmitData
{
    //  Given
    QueueResultField *queueResultField = [QueueResultField new];
    
    queueResultField.businessPhone = @"1234567890";
    queueResultField.company = @"kabigon";
    queueResultField.country = @"Viet Nam";
    queueResultField.email = @"kabigon@yahoo.com";
    queueResultField.firstName = @"Thanh";
    queueResultField.lastName = @"Thien";
    queueResultField.jobTitle = @"Dev iOS";
    queueResultField.zipCode = @"57000";
    queueResultField.promoCode = @"promocode";

    
    //  When
    NSDictionary *dicData = [SubmitData createObjectSubmit:queueResultField];
    
    //  Then
    XCTAssertNotEqualObjects(dicData, nil);
    
}


/********************************************************************************/

@end
