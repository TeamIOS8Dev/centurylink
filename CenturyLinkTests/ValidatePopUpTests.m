//
//  ValidatePopUpTests.m
//  CenturyLink
//
//  Created by Gau Uni on 11/19/14.
//  Copyright (c) 2014 CO2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ResultPopUpViewController.h"
#import <OCMock/OCMock.h>

@interface ValidatePopUpTests : XCTestCase
@property (nonatomic, strong) UITextField *textFieldFirstName;
@property (nonatomic, strong) ResultPopUpViewController *resultPopUp;
//  OCMock
@property (nonatomic, strong) id ocMockPartialClass;

@end

@implementation ValidatePopUpTests

- (void)setUp {
    
    _resultPopUp = [[ResultPopUpViewController alloc] init];
    _textFieldFirstName = [OCMockObject mockForClass:[UITextField class]];
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)testValidateForm
{
//    ResultPopUpViewController *resultPopUp = [[ResultPopUpViewController alloc] init];

}

//------------------------------------------------------------------------------------------
/**   */
-(void)testNilFirstName
{
    ResultPopUpViewController *resultPopUp = [[ResultPopUpViewController alloc] init];
    resultPopUp.textFieldFirstName.text = @"";
    BOOL isInvalid = [resultPopUp validateFirstName];
    XCTAssertFalse(isInvalid, @"First name is nil");
}

//------------------------------------------------------------------------------------------
/**   */
-(void)testValidFirstName
{
    id textFieldMock = [OCMockObject mockForClass:[UITextField class]];
    [[textFieldMock expect]text];
   ResultPopUpViewController *resultPopUp = [[ResultPopUpViewController alloc] init];
    resultPopUp.textFieldFirstName = textFieldMock;
    resultPopUp.textFieldFirstName.text = @"Nguyen";
   BOOL isValid = [resultPopUp validateFirstName];
    XCTAssertTrue(isValid, @"First name is nil");
}

/*
-(void)testValidateLastName
{
    ResultPopUpViewController *resultPopUp = [[ResultPopUpViewController alloc] init];
    
}

-(void)testValidateForm
{
    ResultPopUpViewController *resultPopUp = [[ResultPopUpViewController alloc] init];
    
}

-(void)testValidateForm
{
    ResultPopUpViewController *resultPopUp = [[ResultPopUpViewController alloc] init];
    
}

-(void)testValidateForm
{
    ResultPopUpViewController *resultPopUp = [[ResultPopUpViewController alloc] init];
    
}*/

@end
